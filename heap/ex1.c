#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"

/** Llegeix un fitxer amb una matriu d'adjacencia, calcula el grau de cadascu dels vèrtexs del graf i  l'imprimeix en pantalla. */
int main(int argc, char** argv)
{
	if(argc<3)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels>\n",argv[0]);
		exit(1);
	}

	//read matrix
	int **M;
	int n;
	M=read_matrix_2(argv[1],&n);
	char **labels;
	printf("size of the graph: %d\n",n);
	labels=read_labels(argv[2],n);

   int *grades=malloc(n*sizeof(*grades));

	for(int i=0;i<n;i++)
	{
		grades[i]=0;
		for(int j=0;j<n;j++)
		{
			grades[i]+=M[i][j];
		}

		printf("vèrtex: %d nom: %s grau: %d\n",i,labels[i],grades[i]);

	}

	return 0;
}
