#ifndef _GRAPH_DOT_H
#define _GRAPH_DOT_H

#include "graf_llista.h"

void matrix_write_dot(int *M,int n);
void matrix_write_dot_2(int *M,char** labels, int n);
void matrix_write_dot_3(int *M,char** labels, int n);
void graf_write_dot_2(graf *g, char** labels);

#endif //_GRAPH_DOT_H

