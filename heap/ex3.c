#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"
#include "graf_llista.h"

/** Llegeix un fitxer amb una matriu d'adjacencia, calcula el grau de cadascu dels vèrtexs del graf i  l'imprimeix en pantalla. */
int main(int argc, char** argv)
{
	if(argc<3)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels>\n",argv[0]);
		exit(1);
	}

	graf g;
	read_graph(argv[1],&g); //llegim un graf des de un fitxer en format de llista d'adjacencia
	char **labels;
	printf("nombre de vèrtex: %d nombre de branques: %d\n",g.nv,g.ne);
	labels=read_labels(argv[2],g.nv);

	int *grades=malloc(g.nv*sizeof(*grades));
	
	for(int i=0; i<g.nv;i++)
	{
		grades[i]=0;
		edge *e=g.v[i].first_out;
		while(e)
		{
			grades[i]++;
			e=e->next;
		}
		printf("vèrtex: %d nom: %s grau: %d\n",i,labels[i],grades[i]);
	}

	return 0;
}
