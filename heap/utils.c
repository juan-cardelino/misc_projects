#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "utils.h"

int** read_matrix_2(const char *filename, int *p)
{
	FILE *fid;
	int m=-1,n=-1;
	//int size=256;
	int size=0,v;
	//char* buff=malloc(size*sizeof(char));
	char* buff=NULL;
	char* c;
	int** matrix=NULL;

	fid=fopen(filename,"r");
	//read header
	getline(&buff,&size,fid);	//FIXME this needs to be changed since is C++ and not C
	c = strtok (buff," ");
	if (c !=NULL) m  = (int) atoi (c);
	c = strtok (NULL," ");
	if (c !=NULL) n  = (int) atoi (c);
	assert(m==n);
	//printf("size: %d %d\n",m,n);
	// alloc memory
	matrix=malloc(m*sizeof(int*));
	for(int i=0;i<m;i++)
		matrix[i]=malloc(n*sizeof(int));
	//read data
	//int i;
	for(int i=0; i<m;i++)
	{
		buff=NULL;
		free(buff);
		getline(&buff,&size,fid);

		for(int j=0; j<n;j++)
		{
			if(j==0)
				c = strtok (buff," ");
			else
				c = strtok (NULL," ");
			if (c !=NULL) v  = (int) atoi (c);
			matrix[i][j]=v;
			//printf(" %d",v);
		}
		//printf("\n");
	}
	free(buff);

	fclose(fid);
	*p=n;

	return matrix;
}

int* read_matrix(const char *filename, int *p)
{
	FILE *fid;
	int m=-1,n=-1;
	//int size=256;
	int size=0,v;
	//char* buff=malloc(size*sizeof(char));
	char* buff=NULL;
	char* c;
	int* matrix=NULL;

	fid=fopen(filename,"r");
	//read header
	getline(&buff,&size,fid);	//FIXME this needs to be changed since is C++ and not C
	c = strtok (buff," ");
	if (c !=NULL) m  = (int) atoi (c);
	c = strtok (NULL," ");
	if (c !=NULL) n  = (int) atoi (c);
	//printf("size: %d %d\n",m,n);
	// alloc memory
	matrix=malloc(m*n*sizeof(int));
	//read data
	//int i;
	for(int i=0; i<m;i++)
	{
		buff=NULL;
		free(buff);
		getline(&buff,&size,fid);

		for(int j=0; j<n;j++)
		{
			if(j==0)
				c = strtok (buff," ");
			else
				c = strtok (NULL," ");
			if (c !=NULL) v  = (int) atoi (c);
			matrix[i*n+j]=v;
			//printf(" %d",v);
		}
		//printf("\n");
	}
	free(buff);

	fclose(fid);
	*p=n;

	return matrix;
}

char** read_labels(const char *filename, int n)
{
	FILE *fid;
	int size=0;
	char* buff=NULL;
	char** nodes=malloc(n*sizeof(char*));

	fid=fopen(filename,"r");

	//read data
	//printf("%x\n",nodes);
	for(int i=0; i<n;i++)
	{
		buff=NULL;
		getline(&buff,&size,fid);
		nodes[i]=strtok(buff,"\n");
		//printf(" ptr: %x str: %s\n",nodes[i],nodes[i]);
	}

	fclose(fid);

	return nodes;
}

void write_path(int* path, int size, char** labels, const char *filename, int cost)
{
	FILE *fid;
	char *s;
	fid=fopen(filename,"w+");
	fprintf(fid,"%d\n",size);
	for(int i=size-1; i>=0;i--)
	{
		s=labels[path[i]];
		fprintf(fid,"%s\n",s);
	}
	fprintf(fid,"%d\n",cost);
	fclose(fid);
}

int* read_path(int *n, char** labels, int size_labels, const char *filename, int *cost)
{
	FILE *fid;
	char s[50];
	fid=fopen(filename,"r");

	fscanf(fid,"%d\n",n);
	int *path=malloc(*n*sizeof(int));
	//printf("n: %d\n",*n);
	for(int i=0;i<*n;i++)
	{
		fscanf(fid,"%s\n",s);
		path[i]=search_node_by_label(labels,s,size_labels);
	}
	fscanf(fid,"%d\n",cost);
	//printf("cost: %d\n",*cost);
	fclose(fid);

	return path;
}


int search_node_by_label(char** labels,char *l,int n)
{
	int found=0;
	int idx=-1;

	//printf("str: %s len: %d\n",l,strlen(l));
	for(int i=0;i<n && !found ;i++)
	{
		//printf("i: %d ptr: %x str: %s len: %d\n",i,labels[i],labels[i],strlen(l));

		if(!strcmp(labels[i],l))
		{
			found=1;
			idx=i;
		}

	}

	return idx;
}

