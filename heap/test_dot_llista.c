#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"
#include "grafs_dot.h"
#include "graf_llista.h"

/** Save the graph in dot format */
int main(int argc, char** argv)
{
	if(argc<3)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <graf> <labels>\n",argv[0]);
		exit(1);
	}

	int verbose=0; //for debug
	graf g;
	read_graph(argv[1],&g);
	char **labels;
	fprintf(stderr,"size of the graph: %d\n",g.nv);
	labels=read_labels(argv[2],g.nv);


	graf_write_dot_2(&g,labels);

	return 0;
}
