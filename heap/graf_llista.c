#include "graf_llista.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void init_graph(graf *g)
{
	g->dir=0;
	g->v=malloc(g->nv*sizeof(*g->v));
	g->e=malloc(g->ne*sizeof(*g->e));
	for (int i = 0; i < g->nv; i++)
	{
		vertex *v=g->v+i;
		v->first_out=NULL;
		edge *e=g->e+i;
		e->to=NULL;
		e->from=NULL;
		e->other=NULL;
		e->next=NULL;
		e->prev=NULL;
	}
}

void read_graph(const char* filename, graf *g)
{
	int verbose=0;

	FILE *f=fopen(filename,"r");
	int n,r = fscanf(f, "%d %d\n",&g->nv,&g->ne);
	if (r != 2)
		printf("could not read graf header from file %s\n", filename);
	
	init_graph(g);
	edge *e=g->e;

	for (int i = 0; i < g->nv; i++)
	{
		vertex *v=g->v+i;
		int ne;
		r = fscanf(f, "%d\n",&ne);
		if (r != 1)
			printf("could not read %dth value of file %s\n", i, filename);
		if(verbose)
		{
			printf("node: %d ne: %d\n",i,ne);
			printf("edges: ");
		}
		for (int k = 0; k < ne; k++)
		{
			int idx_v;
			r = fscanf(f, "%d",&idx_v);
			if(verbose)
				printf("%d ",idx_v);
			vertex *vo=g->v+idx_v;
			e->from=v;	
			e->to=vo;
			if(k==0)
			{
				v->first_out=e;
			}
			else
			{
				assert(v->first_out);
				edge* et=v->first_out;
				while(et->next)
				{
					et=et->next;
				}
				et->next=e;
			}
			edge *eo=vo->first_out;
			while(eo)
			{
				if(eo->to==v)
				{
					eo->other=e;
					e->other=eo;
				}
				eo=eo->next;
			}
			e++;	
		}
			r = fscanf(f, "\n");
			if(verbose)
				printf("\n");

	}
	fclose(f);

}

