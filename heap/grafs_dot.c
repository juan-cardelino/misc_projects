#include "grafs_dot.h"
#include "graf_llista.h"
#include "utils.h"

#include <stdio.h>

void matrix_write_dot(int *M,int n)
{
	printf("graph graf{\n");
	for(int i=0;i<n;i++)
	{
	for(int j=0;j<i;j++)
		{
			int *m=M+i*n+j;
			if(*m)
			{
				printf("%d -- %d\n",i,j);
			}
		}
	}
	printf("}\n");
}

void matrix_write_dot_2(int *M,char** labels, int n)
{
	printf("graph graf{\n");
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<i;j++)
		{
			int *m=M+i*n+j;
			if(*m)
			{
				//printf("%d -- %d [label=%s]\n",i,j,labels[i]);
				printf("%s -- %s\n",labels[i],labels[j]);
			}
		}
	}
	printf("}\n");
}

void matrix_write_dot_3(int *M,char** labels, int n)
{
	printf("graph graf{\n");
	printf("rankdir=LR;\n");
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<i;j++)
		{
			int *m=M+i*n+j;
			if(*m<INF)
			{
				//printf("%d -- %d [label=%s]\n",i,j,labels[i]);
				printf("%s -- %s [label=%d]\n",labels[i],labels[j],*m);
			}
		}
	}
	printf("}\n");
}

void graf_write_dot_2(graf *g, char** labels)
{
	printf("graph graf{\n");
	for(int i=0;i<g->nv;i++)
	{
		vertex *v=g->v+i;
		edge *e=v->first_out;
		while(e)
		{
			vertex *vo=e->to;
			int idx_vo=vo-g->v;
			if(g->dir)
				printf("%s -> %s\n",labels[i],labels[idx_vo]);
			else
				printf("%s -- %s\n",labels[i],labels[idx_vo]);
			e=e->next;
		}
	}
	printf("}\n");
}
