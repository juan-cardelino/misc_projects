#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"

/** Llegeix un fitxer amb una matriu d'adjacencia i executa l'algorisme de prim. */
int main(int argc, char** argv)
{
	if(argc<4)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels> <output>\n",argv[0]);
		exit(1);
	}

	//read matrix
	int **M;
	int n;
	M=read_matrix_2(argv[1],&n);
	char **labels;
	fprintf(stderr,"size of the graph: %d\n",n);
	labels=read_labels(argv[2],n);

	int m;
	int *T=malloc(n*sizeof(*T));
	int *B=malloc((n-1)*sizeof(*B));
	int jm,im,ind;
	int c=0;

	for(int i=0;i<n-1;i++)
	{
		T[i]=0;
		B[i]=-1;
	}

	m=99999;
	im=-1;
	jm=-1;
	printf("graph graf{\n");
	FILE* fid=fopen(argv[3],"w+");
	fprintf(fid,"%d\n",n-1);

	for(int i=0;i<n;i++)
		for(int j=0;j<i;j++)
		{
			if(  (M[i][j]<m) )
			{
				m=M[i][j];
				im=i;;
				jm=j;;
			}

		}
	if(im!=-1 && jm!=-1)
	{
		ind=im*n+jm;
		T[im]=1;
		T[jm]=1;
		//printf("inicialització, branca: (%d,%d) \n",im,jm);
		fprintf(fid,"(%s,%s)\n",labels[im],labels[jm]);
		printf("%s -- %s\n",labels[im],labels[jm]);
		B[0]=ind;
		c+=M[im][jm];
	}else
		printf("no hem trobat un mínim\n");

	for(int k=1;k<n-1;k++)
	{
		m=99999;
		im=-1;
		jm=-1;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<i;j++)
			{
				if( (T[i]!=T[j]) && (M[i][j]<m) )
				{
					m=M[i][j];
					im=i;
					jm=j;
				}
			}
		}
		if(im!=-1 && jm!=-1)
		{
			T[im]=1;
			T[jm]=1;
			//printf("pas: %d branca: (%d,%d) \n",k,im,jm);
			fprintf(fid,"(%s,%s)\n",labels[im],labels[jm]);
			printf("%s -- %s\n",labels[im],labels[jm]);
			ind=im*n+jm;
			B[k]=ind;
			c+=M[im][jm];
		}else
			printf("no hem trobat un mínim\n");

	}

		printf("}\n");
		fprintf(fid,"%d\n",c);
		fprintf(stderr,"cost total: %d\n",c);
		fclose(fid);
return 0;
}
