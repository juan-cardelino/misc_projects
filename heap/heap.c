#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "heap.h"

/* Priority queue, using a binary heap with static maximum size
This code is in the public domain, but I would be glad if you leave my name in:
Rene Wiermer, rwiermer@googlemail.com. Feedback is welcome.

The implemementation is based on Heapsort as described in
"Introduction to Algorithms" (Cormen, Leiserson, Rivest, 24. printing)

Modified By Gabriele Facciolo
*/

ITEM* find_minimum(ITEM* nodes, int n)
{
	ITEM* res=NULL;
	float min=INF;
	for(ITEM* it=nodes; it<nodes+n;it++)
	{
		if(it->FITNESS<min && !it->etc)
		{
			res=it;
			min=it->FITNESS;
		}
	}
	return res;
}

/* Is Empty?*/
int heap_isEmpty(HEAP *h) {
	return h->size==0;
}
/* Is FULL?*/
int heap_isFull(HEAP*h) {
	return h->size >= h->MAX_SIZE;
}
/* Initialize the heap and allocate its space*/
int heap_init(HEAP* h, long size) {
	if (!(h->array = (ITEM**) calloc((long) size, sizeof(ITEM*))) )
		return 0;

	memset(h->array, 0, size*sizeof(ITEM*));

	h->size=0;
	h->MAX_SIZE = size;
	return 1;
}
/* Delete the heap*/
void heap_delete(HEAP* h) {
	free(h->array);
}
/* Reestablish heap condition by moving up the element i until to a good place*/
void heap_moveUP(HEAP* h, int i ) {
	ITEM* tmp;
	int parent;

	tmp=h->array[i];
	/*find the correct place to insert*/
	parent=(i-1)/2;
	while ((i > 0) && (tmp->FITNESS <  h->array[parent]->FITNESS)) {
		h->array[i]=h->array[parent];
		h->array[i]->idx = i;
		i=parent;
		parent=(i-1)/2;
	}
	h->array[i]=tmp;
	h->array[i]->idx =i;
}
/* Reestablish heap condition by moving down the element i until to a good place*/
void heap_moveDOWN(HEAP* h, int i) {
	ITEM* tmp;
	int l,r,biggest;

	l=2*i+1; /*left child*/
	r=2*i+2; /*right child*/

	if ((l < h->size)&&(h->array[l]->FITNESS <  h->array[i]->FITNESS )) biggest=l;
	else biggest=i;

	if ((r < h->size)&&(h->array[r]->FITNESS < h->array[biggest]->FITNESS)) biggest=r;

	while (biggest != i) {
		tmp=h->array[biggest];
		h->array[biggest]=h->array[i];
		h->array[biggest]->idx = biggest;
		h->array[i]=tmp;
		h->array[i]->idx = i;

		/* prepare for the next loop*/
		i=biggest;
		l=2*i+1; /*left child*/
		r=2*i+2; /*right child*/

		if ((l < h->size) && (h->array[l]->FITNESS <  h->array[i]->FITNESS )) biggest=l;
		else biggest=i;

		if ((r < h->size) && (h->array[r]->FITNESS < h->array[biggest]->FITNESS )) biggest=r;
	}
}
/* Add FIELD p to the HEAP h this operation takes O(lg(n))
 * returns 1 if the operation was successfull or 0 if the heap is full*/
int heap_addItem(HEAP* h, ITEM* p) {
	int i;
	if (heap_isFull(h)) return 0;
	/* increase the number of elements*/
	h->size++;

	/* the new availeble position is at the end of the heap */
	i=h->size-1;

	h->array[i]=p;
	h->array[i]->idx=i;

	/* move the element to its position */
	heap_moveUP(h, i);
	return 1;
}

/* Extract Minimum
 * returns the pointer to ITEM or NULL if the heap is empty*/
ITEM* heap_extractMin(HEAP* h) {
	ITEM* max;
	if (heap_isEmpty(h)) return 0;

	/* extract the max */
	max = h->array[0];
	max->idx = -1;

	/* overwrite it with the last element */
	h->array[0] = h->array[h->size-1];
	h->array[0]->idx= 0;

	/* reduce the size of the heap */
	h->size = h->size-1;

	/* reestabilish propertyes */
	heap_moveDOWN(h,0);

	return max;
}

/* Query Minimum (do not remove it)
 * returns the pointer to ITEM or NULL if the heap is empty*/
ITEM* heap_queryMin(HEAP* h) {
   ITEM* max;
   if (heap_isEmpty(h)) return 0;
	/* the max */
   max = h->array[0];
	return max;
}

/* Delete element i*/
void heap_delItem(HEAP* h, ITEM *p) {
	int parent;
	int i = p->idx;

	if (i >= h->size ) {
		printf("HEAP (del): there is no element %d the heap is shorter (%d)\n",i,h->size);
	return;
	}

	if (i<0) {
		printf("HEAP (del): sorry the element is not here\n");
	return;
	}

	/* tag the previous element as outside the heap */
	h->array[i]->idx = -1;

	/* move the last element to this position to overwrite it */
	h->array[i] = h->array[h->size-1];
	h->array[i]->idx = i;
	h->size-- ;

	/* determine if the element goes up or down */
	parent = (i-1)/2;
	if ((i>0)&& (h->array[i]->FITNESS < h->array[parent]->FITNESS)){
		heap_moveUP(h,i);
	}
	else {
		heap_moveDOWN(h,i);
	}
}
/* Refresh element i*/
void heap_updateItem(HEAP* h, ITEM *p) {
	int parent;
	int i = p->idx;

	if (i >= h->size ) {
		printf("HEAP (update): there is no element %d the heap is shorter (%d)\n",i,h->size);
		return;
	}

	if (i<0) {
		printf("HEAP (update): sorry the element is not here\n");
		return;
	}

	/* determine if the element goes up or down */
	parent = (i-1)/2;
	if ((i>0) && (h->array[i]->FITNESS < h->array[parent]->FITNESS)){
		heap_moveUP(h,i);
	}
	else {
		heap_moveDOWN(h,i);
	}
}


