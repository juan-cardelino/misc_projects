#ifndef _UTILS_H
#define _UTILS_H


#ifndef INF
#define INF 2147483647
#endif


int search_node_by_label(char** labels,char *l,int n);
void write_path(int* path, int size, char** labels, const char *filename, int cost);
char** read_labels(const char *filename, int n);
int* read_matrix(const char *filename, int *p);
int** read_matrix_2(const char *filename, int *p);
int* read_path(int *n, char** labels, int size_labels, const char *filename, int *cost);

#endif //_UTILS_H
