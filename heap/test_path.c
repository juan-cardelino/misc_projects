#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "heap.h"
#include "utils.h"

int main(int argc, char** argv)
{
	if(argc<6)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels> <start> <end> <path>\n",argv[0]);
		exit(1);
	}

	int verbose=0; //for debug
	//read matrix
	int *M;
	int n;
	int size_path;
	M=read_matrix(argv[1],&n);
	char **labels;
	//printf("%d\n",n);
	labels=read_labels(argv[2],n);
	int *path=NULL;
	int cost;
	path=read_path( &size_path, labels, n, argv[5], &cost);
	char* start=argv[3];
	char* end=argv[4];

	printf("optimal path:\n");
	printf("n: %d\n",size_path);
	for(int i=0;i<size_path;i++)
	{
		if(verbose) printf("i: %d str: %s\n",path[i],labels[path[i]]);
	}

	//find initial node
	int idx_start=search_node_by_label(labels,start,n);
	if(idx_start>=0)
	{
		if(verbose) printf("ind: %d label: %s\n",idx_start,labels[idx_start]);
	}else
		fprintf(stderr,"incorrect initial node\n");

	//find final node
	int idx_end=search_node_by_label(labels,end,n);
	if(idx_end>=0)
	{
		if(verbose) printf("ind: %d label: %s\n",idx_end,labels[idx_end]);
	}else
		fprintf(stderr,"incorrect final node\n");

		int curr_node=idx_start;
		int correct=1;
		int actual_cost=0;
		for(int i=1;i<size_path && correct;i++)
		{
			int next_node=path[i];
			int weight=M[curr_node*n+next_node];
			if(weight>=INF)
				correct=0;
			else
				actual_cost+=weight;
			if(verbose) printf("node: %d next_node: %d weight: %d\n",curr_node,next_node,weight);
			curr_node=next_node;
		}
		if(fabs(actual_cost-cost)>1e-6)
			correct=0;

		printf("given cost: %d\n",cost);
		printf("actual cost: %d\n",actual_cost);
		if(correct)
			printf("test passed!\n");
	return 0;
}
