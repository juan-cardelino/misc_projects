#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"
#include "grafs_dot.h"

/** Compute the grade of each node on the graf */
int main(int argc, char** argv)
{
	if(argc<3)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels>\n",argv[0]);
		exit(1);
	}

	int verbose=0; //for debug
	//read matrix
	int *M;
	int n;
	M=read_matrix(argv[1],&n);
	char **labels;
	fprintf(stderr,"size of the graph: %d\n",n);
	labels=read_labels(argv[2],n);


	matrix_write_dot_3(M,labels,n);

	return 0;
}
