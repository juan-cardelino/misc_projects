#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "heap.h"

#include "utils.h"

/** Dijsktra algorithm using heap. */
int main(int argc, char** argv)
{
	if(argc<6)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels> <start> <end> <output>\n",argv[0]);
		exit(1);
	}

	int verbose=0; //for debug
	//read matrix
	int *M;
	int n;
	M=read_matrix(argv[1],&n);
	char **labels;
	//printf("%d\n",n);
	labels=read_labels(argv[2],n);
	char* start=argv[3];
	char* end=argv[4];

	HEAP heap;
	ITEM* nodes=malloc(n*sizeof(ITEM));

	//printf("ptr: %x\n",labels);
	for(int i=0;i<n;i++)
	{
		//printf("i: %d str: %s\n",i,labels[i]);
		nodes[i].FITNESS=INF;
		nodes[i].label=i;
		nodes[i].prev=-1;
		nodes[i].etc=0;
	}

	heap_init(&heap, n);

	//find initial node
	int idx_start=search_node_by_label(labels,start,n);
	if(idx_start>=0)
	{
		if(verbose) printf("ind: %d label: %s\n",idx_start,labels[idx_start]);
	}else
		fprintf(stderr,"incorrect initial node\n");
	// set its cost to zero
	ITEM *it=nodes+idx_start;
	it->FITNESS=0;

	clock_t t_start, t_end;
	t_start=clock();
	//now fill the heap
	for(int i=0;i<n;i++)
		heap_addItem(&heap,nodes+i);

	//test extraction
	/*
	while (!heap_isEmpty(&heap)) {
		ITEM* res = heap_extractMin(&heap);
		printf("%s\t%.2f\n",labels[res->label],res->FITNESS);
	}
	*/

	while (!heap_isEmpty(&heap))
	{
		ITEM* res = heap_extractMin(&heap);
		res->etc=1;
		if(verbose) printf("node: %s\t dist: %.2f\n",labels[res->label],res->FITNESS);
		for(int i=0;i<n;i++)
		{
			ITEM* neighbor=nodes+i;
			int weight=M[res->label*n+i];
			if(verbose) printf("node: %s\t dist: %.2f weight: %d\n",labels[neighbor->label],neighbor->FITNESS,weight);
			if(!neighbor->etc && weight<INF) //check if it was previously processed
			{
				float new_dist=res->FITNESS + weight;
				if(neighbor->FITNESS > new_dist)
				{
					neighbor->prev=res->label;
					neighbor->FITNESS=new_dist;
					heap_updateItem(&heap,neighbor);
				}
			}
		}
		if(verbose) printf("--------------------\n");
	}

	if(verbose) printf("final distances\n");
	for(int i=0;i<n;i++)
	{
		ITEM* it=nodes+i;
		if(verbose)  printf("node: %s\t dist: %.2f",labels[it->label],it->FITNESS);
		if(verbose)
		{
			if(it->prev>=0)
			{
				printf(" prev_idx: %d",it->prev);
				printf(" prev: %s",labels[it->prev]);
			}
			printf("\n");
		}
	}
	t_end=clock();

	printf( "ET: %f \n", ((double) (t_end - t_start)) * 1000/CLOCKS_PER_SEC );

	//find final nodes
	int idx_end=search_node_by_label(labels,end,n);
	if(idx_end>=0)
	{
 		if(verbose) printf("ind: %d label: %s\n",idx_end,labels[idx_end]);
	}
	else
		fprintf(stderr,"incorrect final node\n");

	//compute optimal path
	if(verbose) printf("--------------------\n");
	if(verbose) printf("optimal path: \n");
	int *path=malloc(n*sizeof(int));
	int *p=path;
	int size_path=1;
	it=nodes+idx_end;
	printf("%s ",labels[it->label]);
	*p++=it->label;
	int cost=it->FITNESS;
	while(it->prev>=0)
	{
		it=nodes+it->prev;
		*p++=it->label;
		size_path++;
		if(verbose) printf("%s ",labels[it->label]);
	}
	if(verbose) printf("\n");
	printf("%d\n",cost);
	write_path(path, size_path, labels, argv[5], cost);
	return 0;
}
