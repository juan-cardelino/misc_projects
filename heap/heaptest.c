#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "heap.h"


int number_of_nodes=20;

/*Test code. Tests if a path is correct and it has the given value */
int main() {
	int i;
	HEAP heap;
	ITEM p[number_of_nodes];
	ITEM *res;

/*	p[0].FITNESS=123;
	p[1].FITNESS=23;
	p[2].FITNESS=0;
	p[3].FITNESS=22;
	p[4].FITNESS=255;
	p[5].FITNESS=1;
	p[6].FITNESS=10;
	p[7].FITNESS=3;
	p[8].FITNESS=101;
	p[9].FITNESS =102;*/

	heap_init(&heap, number_of_nodes);

	printf("\nAdding nodes to the heap\nLabel\tCost\n");
	for (i=0;i<number_of_nodes;i++) {
		p[i].label = i;
		p[i].FITNESS = (int)(rand()/1000000);
		printf("%d\t%.2f\n",p[i].label,p[i].FITNESS);
		heap_addItem(&heap,&(p[i]));
	}

	printf("\n\nExtracting the nodes from the heap\nLabel\tCost\n");
	while (!heap_isEmpty(&heap)) {
		res = heap_extractMin(&heap);
		printf("%d\t%.2f\n",res->label,res->FITNESS);
	}
}


