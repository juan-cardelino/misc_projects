/** \file graf_llista.h
 * estructures de dades i funcions per a representar grafs com llistes encadenades.
*/
#ifndef _GRAF_LLISTA_H
#define _GRAF_LLISTA_H

typedef struct _edge edge;
typedef struct _vertex vertex;

/** branca d'un graf dirigit. */
struct _edge {
	/// vèrtex origen de la branca
	vertex *from;
	/// vèrtex destí de la branca
	vertex *to;
	/// branca següent	
	struct _edge *next;
	/// branca anterior
	struct _edge *prev;
	/// branca que va en la direcció contraria
	struct _edge *other;
};

/** vèrtex d'un graf dirigit. */
struct _vertex{
	///primera branca de la llista encadenada
	edge *first_out;	
};


/** struct per a representar un graf dirigit.
 * En el cas d'un graf no dirigit, es fa un graf dirigit amb dues branques per cada connexió.  */
typedef struct{
	/// indica si el graf es dirigit o no.
	int dir;
	///nombre de branques.
	int ne;
	///nombre de vèrtexs.
	int nv;
	///llista de vèrtexs.
	vertex *v;
	///llista de branques.	
	edge *e;		
}graf;

/** llegeix un graf des de un fitxer */
void read_graph(const char* filename, graf* g);
/** inicialitza un graf. a de tenir g.nv i g.ne amb valors correctes.*/
void init_graph(graf *g);

#endif // _GRAF_LLISTA_H

