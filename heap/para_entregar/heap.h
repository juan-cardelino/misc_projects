#ifndef BINARY_TREE_HEAP
#define BINARY_TREE_HEAP
/* Priority queue, using a binary heap with static maximum size
This code is in the public domain, but I would be glad if you leave my name in:
Rene Wiermer, rwiermer@googlemail.com. Feedback is welcome.

The implemementation is based on Heapsort as described in
"Introduction to Algorithms" (Cormen, Leiserson, Rivest, 24. printing)

Modified By Gabriele Facciolo
*/


/* HEAP ITEM STRUCTURE 
 * FITNESS correspond to the cost/fitness value used to sort the heap
 * ITEM can customized */
typedef struct
{
	/* ALL YOUR DATA HERE */
	int label;
	int prev;
	int etc;

	/*Private data */
	float FITNESS;  /* cost/fitness key*/
	int idx;	/* internal index : DON'T TOUCH*/

} ITEM;


/*****************************************************************
 * DON'T MODIFY BELOW THIS LINE 
 * **************************************************************/

/* HEAP INTERNAL STRUCTURE*/
typedef struct {
	ITEM **array;
	unsigned int size;
	long MAX_SIZE;
} HEAP;



int heap_isEmpty(HEAP *h);
int heap_isFull(HEAP*h);
int heap_init(HEAP* h, long size);
void heap_delete(HEAP* h);

int heap_addItem(HEAP* h, ITEM* p);
void heap_delItem(HEAP* h, ITEM *p);
void heap_updateItem(HEAP* h, ITEM *p);
ITEM* heap_extractMin(HEAP* h);
ITEM* heap_queryMin(HEAP* h);

#endif

