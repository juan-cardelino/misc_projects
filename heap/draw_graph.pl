#!/usr/bin/perl
    use GraphViz;

    #create your adjacency matrix, nodes do whatever else you need.
    my @admat = (
    [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
    );

    my @nodes=('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');

    #create a new graph
    my $graph = GraphViz->new();

    #add all your nodes to the graph
    foreach (@nodes) {
        $graph->add_node($_);
    }

    #create edges as defined by your adjacecncy matrix
    for($i=0; $i<@nodes; $i++){
        for($j=0; $j<@nodes; $j++){
            if($admat[$i][$j] eq 1){
                print "adding edge from $i to $j\n";
                $graph->add_edge($nodes[$i] => $nodes[$j]);}
        }
    }

    #render the graph to a png-file
    $graph->as_png("pretty.png");