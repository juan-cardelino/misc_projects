#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"
#include "graf_llista.h"

/** Llegeix un fitxer amb una matriu d'adjacencia, recorreix els veïns i els imprimeix en pantalla. */
int main(int argc, char** argv)
{
	if(argc<3)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels>\n",argv[0]);
		exit(1);
	}

	graf g;
	read_graph(argv[1],&g); //llegim un graf des de un fitxer en format de llista d'adjacencia
	char **labels;
	printf("nombre de vèrtex: %d nombre de branques: %d\n",g.nv,g.ne);
	labels=read_labels(argv[2],g.nv);

	//completar aqui

	return 0;
}
