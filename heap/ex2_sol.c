#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include "utils.h"

/** Llegeix un fitxer amb una matriu d'adjacencia, calcula la llista de veins de cadascu dels vertexs del graf i els imprimeix en pantalla. */
int main(int argc, char** argv)
{
	if(argc<3)
	{
		fprintf(stderr,"wrong arguments \n");
		fprintf(stderr,"usage: %s <matrix> <labels>\n",argv[0]);
		exit(1);
	}

	int **M;	//declarem la matri d'adjacències
	int n;
	M=read_matrix_2(argv[1],&n); //llegim la matriu
	char **labels;
	printf("mida del graf: %d\n",n);
	labels=read_labels(argv[2],n);

	for(int i=0;i<n;i++)
	{
		printf("vèrtex: %d nom: %s veïns:",i,labels[i]);
		for(int j=0;j<n;j++)
		{
			if(M[i][j]==1) printf(" %s",labels[j]);
		}
		printf("\n");
	}

	return 0;
}
