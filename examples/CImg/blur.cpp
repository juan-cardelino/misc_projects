
#include <iostream>
#include <fstream>
#include <string>
// Include CImg library file and use its main namespace
#define cimg_verbosity 0
#include "CImg.h"
#include <time.h>
using namespace cimg_library;
using namespace std;

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)




void bones_prueba_sharpen(const char* file_i, const char* file_o)
{
	XML_IN;

	//original image
	const CImg<float> im(file_i);
	im.display();

	const CImg<float> im_grey=im.get_norm()/sqrt(3.0);
	im_grey.display();

	//blurred image
	float sigma=5;
	const CImg<float> im_blur=im_grey.get_blur(sigma);
	im_blur.display();

	XML_OUT;
}



int main(int argc,char **argv)
{
	const char* file_i=  "2.jpg";
	const char* file_o= "examples/2_out.bmp";

	bones_prueba_sharpen(file_i, file_o);
	return 0;
}
