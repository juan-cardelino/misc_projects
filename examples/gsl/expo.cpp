#include <cstdio>
#include  "gsl/gsl_randist.h"
#include  "gsl/gsl_sf.h"


double ggd_pdf(double x, double a,double b){
  double p;
    p=b*gsl_sf_gamma(1+1/b)/gsl_sf_gamma(1/b)*gsl_ran_exppow_pdf(x,a,b);

  return p; 
}

int main(int argc,char **argv){

  double x=atof(argv[1]);
  double a=1;
  double b=0.5;

  double p,q,r;

  p=gsl_ran_exppow_pdf(x,a,b);
  r=ggd_pdf(x,a,b);
  printf("x: %f exppow(x): %f \n",x,p);
  printf("x: %f ggd_pdf(x): %f \n",x,r);

  double t=b*gsl_sf_gamma(1+1/b)/gsl_sf_gamma(1/b);
  printf("factor: %f \n",t);
  printf("now evaluate gamma\n");

  q=gsl_sf_gamma(x);  
  printf("x: %f G(x): %f \n",x,q);



  return 0;
}
