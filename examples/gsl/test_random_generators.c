#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <time.h>

int main (void){

  const gsl_rng_type * T;
  gsl_rng * r;
  int i, n = 10;

  gsl_rng_env_setup();
  time_t t=time(NULL);  
  gsl_rng_default_seed=t;

  T = gsl_rng_default;
  r = gsl_rng_alloc (T);


  printf("seed: %lu\n",t);
  /* printf("%u\n",t);
  printf("%d\n",t);
  */

  for (i = 0; i < n; i++){
    
    unsigned long int u = gsl_rng_uniform_int (r,100);
    printf ("%u \n", u);
  }
  gsl_rng_free (r);
  
  return 0;
}
