#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>


struct my_f_params
{
  double a;
};

double my_f(double x, void *params)
{
  struct my_f_params *p=(my_f_params *)params;
  double a=(double)(p->a);
  return exp(a*x)-1;
}

double my_df(double x, void *params)
{
  struct my_f_params *p=(my_f_params *)params;
  double a=(double)(p->a);
  return a*exp(a*x);
}

void my_fdf(double x, void *params,double *f, double *df)
{
  struct my_f_params *p=(my_f_params *)params;
  double a=(double)(p->a);
  double t=exp(a*x);
  *f= t-1;
  *df= a*t;
}

int main (void)
{
  int status;
  int iter = 0, max_iter = 100;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;
  double r = 0, r_expected = 0;
  double x_lo = -1, x_hi = 1.0;
  gsl_function F;
  struct my_f_params p = {1.0};

  F.function = &my_f;
  F.params = &p;

  T = gsl_root_fsolver_brent;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_lo, x_hi);

  printf ("using %s method\n",
	  gsl_root_fsolver_name (s));

  printf ("%5s [%9s, %9s] %9s %10s %9s\n",
	  "iter", "lower", "upper", "root",
	  "err", "err(est)");

  do
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      r = gsl_root_fsolver_root (s);
      x_lo = gsl_root_fsolver_x_lower (s);
      x_hi = gsl_root_fsolver_x_upper (s);
		status = gsl_root_test_interval (x_lo, x_hi,
				0, 0.001);

		if (status == GSL_SUCCESS)
			printf ("Converged:\n");

		printf ("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
				iter, x_lo, x_hi,
				r, r - r_expected,
				x_hi - x_lo);
	 }
  while (status == GSL_CONTINUE && iter < max_iter);
  return status;
}
