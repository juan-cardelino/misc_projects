#include <cstdio>
#include  "gsl/gsl_randist.h"
#include  "gsl/gsl_sf.h"
#include  "gsl/gsl_roots.h"

struct my_f_params{
  double a;
};

double my_f(double x, void *params){
    struct my_f_params *p=(my_f_params *)params;
    double a=(double)(p->a);
    return exp(a*x)-1;
  }

double my_df(double x, void *params){
    struct my_f_params *p=(my_f_params *)params;
  double a=(double)(p->a);
  return a*exp(a*x);
}

void my_fdf(double x, void *params,double *f, double *df)
{
  struct my_f_params *p=(my_f_params *)params;
  double a=(double)(p->a);
  double t=exp(a*x);
  *f= t-1;
  *df= a*t;
}

int main(int argc,char **argv){

  double x=0.5,a=1;
  double b=2;
  int status;
  int max_iter=100;
  double r,x0=0.5;
  double r_expected=0;


  //  const gsl_root_fsolver_type *T = gsl_root_fsolver_bisection;
  // gsl_root_fsolver *s = gsl_root_fsolver_alloc(T);
  const gsl_root_fdfsolver_type *T = gsl_root_fdfsolver_newton;
   gsl_root_fdfsolver *s = gsl_root_fdfsolver_alloc(T);

  gsl_function_fdf F;

  F.f=&my_f;
  F.df=&my_df;
  F.fdf=&my_fdf;

  struct my_f_params params={1};
  F.params=&params;

  int iter=0;

  gsl_root_fdfsolver_set (s, &F, x0);

  printf ("using %s method\n",
	  gsl_root_fdfsolver_name (s));

  printf ("%-5s %10s %10s %10s\n",
	  "iter", "root", "err", "err(est)");

  do
    {
      iter++;
      status = gsl_root_fdfsolver_iterate (s);
      x0=x;
      x = gsl_root_fdfsolver_root (s);
      status = gsl_root_test_delta (x, x0, 0, 1e-3);

      if (status == GSL_SUCCESS)
	printf ("Converged:\n");
      
      printf ("%5d %10.7f %+10.7f %10.7f\n",
                   iter, x, x - r_expected, x - x0);
    }
  while (status == GSL_CONTINUE && iter < max_iter);
  return status;


}
