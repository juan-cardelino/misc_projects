project(demo)

cmake_minimum_required(VERSION 2.6)

find_package(abc REQUIRED)

#this part of the project generates the library

include_directories(${ABC_INCLUDE_DIRS})
link_directories(${ABC_LINK_DIRS})

add_executable(demo demo.c)
target_link_libraries(demo ${ABC_LIBRARIES})


