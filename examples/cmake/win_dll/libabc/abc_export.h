
#ifndef ABC_EXPORT_H
#define ABC_EXPORT_H

#ifdef ABC_BUILT_AS_STATIC
#  define ABC_EXPORT
#  define ABC_NO_EXPORT
#else
#  ifndef ABC_EXPORT
#    ifdef abc_EXPORTS
        /* We are building this library */
#      define ABC_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define ABC_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef ABC_NO_EXPORT
#    define ABC_NO_EXPORT 
#  endif
#endif

#ifndef ABC_DEPRECATED
#  define ABC_DEPRECATED __declspec(deprecated)
#  define ABC_DEPRECATED_EXPORT ABC_EXPORT __declspec(deprecated)
#  define ABC_DEPRECATED_NO_EXPORT ABC_NO_EXPORT __declspec(deprecated)
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define ABC_NO_DEPRECATED
#endif

#endif
