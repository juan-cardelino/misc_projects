#ifndef LIBABC_H_
#define LIBABC_H_

#include "abc_export.h"
 
void ABC_EXPORT a_fun();
void b_fun();
void c_fun();

#endif //LIBABC_H_