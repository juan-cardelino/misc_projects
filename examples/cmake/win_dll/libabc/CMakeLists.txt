project(testabc)

cmake_minimum_required(VERSION 2.6)
include (GenerateExportHeader)

#this part of the project generates the library
add_library(abc SHARED libabc.h abc_export.h a.c b.c c.c)

ADD_DEFINITIONS(-DABC_EXPORTS)

GENERATE_EXPORT_HEADER( abc
             BASE_NAME abc
             EXPORT_MACRO_NAME ABC_EXPORT
             EXPORT_FILE_NAME abc_export.h
             STATIC_DEFINE ABC_BUILT_AS_STATIC
)

#setup Config.cmake
set(ABC_BASE_DIR "${PROJECT_SOURCE_DIR}")
set(ABC_BUILD_DIR "${PROJECT_BINARY_DIR}")
configure_file(abcConfig.cmake.in
  "${PROJECT_BINARY_DIR}/abcConfig.cmake" @ONLY)