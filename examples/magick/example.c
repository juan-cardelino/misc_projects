#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string.h>
#include <magick/api.h>



int main(int argc, char** argv)
{
	ExceptionInfo exception;
	Image *image;
	ImageInfo *image_info;
	InitializeMagick(".");
	GetExceptionInfo(&exception);
	image_info=CloneImageInfo((ImageInfo *) NULL);
	(void) strcpy(image_info->filename, argv[1]);
	image=ReadImage(image_info,&exception);

	if (image == (Image *) NULL)
		MagickError(exception.severity,exception.reason,exception.description);

	int COLS = image -> columns;				// numero de columnas
	int ROWS = image -> rows;					// numero de filas
	
	printf("filas: %d columnas: %d\n",COLS, ROWS);

	//output image
	ImageInfo *image_info_out=NULL;
	image_info_out=CloneImageInfo((ImageInfo *) NULL);
	image_info_out->depth=8;
	Image *image_out=NULL;
	image_out=AllocateImage(image_info_out);
	if (image == (Image *) NULL)
		MagickError(ResourceLimitError,"Unable to save image",
						"Memory allocation failed");
	image_info_out=CloneImageInfo((ImageInfo *) NULL);
	strcpy( image_info_out->filename, argv[2] );
	image_out -> rows = image->rows;
	image_out -> columns = image->columns;
	image_out->depth=8;

	PixelPacket *q;
	
	for (int i=0; i < ROWS; i++ )
		for (int j=0; j < COLS; j++)
	{
		int offset=COLS*i+j;
		int r=(int)rint(GetOnePixel( image, j, i ).red);
		int g=(int)rint(GetOnePixel( image, j, i ).green);
		int b=(int)rint(GetOnePixel( image, j, i ).blue);
		//printf(" %d",r);

	q=SetImagePixels(image_out,j,i,1,1);
		if (q == (PixelPacket *) NULL)
		{
			break;
		}
		q -> red =  ( (unsigned ) ( r ) );
		q -> green =  ( (unsigned ) ( g ) );
		q -> blue =  ( (unsigned ) ( b ) );
		q++;

	}
	SyncImagePixels(image_out);

	WriteImage(image_info_out,image_out);
	DestroyImage(image_out);
	DestroyImageInfo(image_info_out);
	
	DestroyExceptionInfo(&exception);
	DestroyImage(image);
	DestroyImageInfo(image_info);
	DestroyMagick();
}


