#!/bin/bash

#color="#D2ACAC"
color="#9A1E45"
#color="#ECB9D6"

#mask generation
convert mask_white.png mask_white.pgm
#convert -negate mask_white.pgm mask_white_neg.pgm
convert -size 500x400 xc:"$color" red.png
convert red.png -separate sep_%d.pgm
#convert sep_0.pgm sep_1.pgm sep_2.pgm mask_white_neg.pgm -channel RGBA -combine
convert sep_0.pgm sep_1.pgm sep_2.pgm mask_white.pgm -channel RGBA -combine mask_full_red.png




##################  threshold ##################
#extract lips (and)
composite -compose Dst_In mask_white.png crop.jpg comp_and.png
#threshold
convert -white-threshold 45000 comp_and.png comp_mask_threshold.png
#put alpha
convert -transparent black comp_mask_threshold.png comp_mask_threshold_alpha.png
#replace
composite -compose Over comp_mask_threshold_alpha.png crop.jpg comp_gloss.png



##################  multiply ##################
#blur mask (alpha channel only)
convert -gaussian-blur 0x2 -channel A mask_full_red.png mask_full_red_blur.png
#set alpha for mask
convert -channel Alpha -evaluate Multiply 0.87 mask_full_red_blur.png mask_full_red_blur_alpha.png
#multiply
composite -compose Multiply comp_gloss.png mask_full_red_blur_alpha.png comp_gloss_mult.png
#composite -compose Multiply crop.jpg mask_full_red_blur_alpha.png comp_mult.png
#lighten
convert -channel A -evaluate Multiply 0.12 mask_full_red_blur.png mask_full_red_blur_alpha_lighten.png
composite -compose Lighten  comp_gloss_mult.png mask_full_red_blur_alpha_lighten.png comp_gloss_mult_light.png
