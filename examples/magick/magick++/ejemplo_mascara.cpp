#include <Magick++.h>
#include <iostream>
using namespace std;
using namespace Magick;

int main(int argc, char **argv)
{
	try
	{
		InitializeMagick(NULL);
		//create red image
		Color color_red(MaxRGB, 0, 0, 1);
		Magick::Image red(Geometry(640, 480), color_red);
		red.type(TrueColorMatteType);
		//red.display();

		//create red image
		Color color_green(0, MaxRGB, 0, 1);
		Image green(Geometry(640, 480), color_green);
		//red.type(TrueColorMatteType);

		//intento copiar una imagen gris en el canal alpha
		//lo mejor sera hacer un for sobre el canal alpha y ya esta.
		Color color_white(MaxRGB, MaxRGB, MaxRGB, 1);
		Magick::Image white(Geometry(640, 480), color_white);
		white.type(GrayscaleType);
		//blanco.display();
		white.write("blanco.png");

		//another failed attempt to combine channels
		MagickCore::ExceptionInfo* e = MagickCore::AcquireExceptionInfo();
		Magick::Image combine[2];
		combine[0] = white;
		combine[1] = white;
		//const Image* c=combine;
		MagickCore::Image* combined = MagickCore::CombineImages(white.constImage(), MagickCore::RedChannel, e);
		MagickCore::Image* combined2 = MagickCore::CombineImages(white.constImage(), MagickCore::GreenChannel, e);

		Image im2(combined2);
		im2.write("hola.png");

		cout << "has matte channel: " << red.matte() << endl;
		Pixels view(red);

		size_t columns = 196;
		size_t rows = 162;
		Color green_alpha(0, MaxRGB, 0, 0);
		PixelPacket *pixels = view.get(38, 36, columns, rows);
		for (ssize_t row = 0; row < rows; ++row)
			for (ssize_t column = 0; column < columns; ++column)
				*pixels++ = green_alpha;

		// Save changes to image.
		view.sync();
		//rojo.display();
		red.write("rojo.png");

		
		red.composite(white, Geometry(0, 0, 0, 0), Magick::CopyGreenCompositeOp);

		int nb_channels = 4;
		int m = 320;
		int n = 240;

		int* pixs = (int*) malloc(m * n * nb_channels * sizeof (*pixs));

		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				int ind = nb_channels * (i * n + j);
				if (i < n / 2)
				{
					pixs[ind] = MaxRGB;
					pixs[ind + 1] = 0;
					pixs[ind + 2] = 0;
					pixs[ind + 3] = MaxRGB;
				} else
				{
					pixs[ind] = 0;
					pixs[ind + 1] = 0;
					pixs[ind + 2] = 0;
					pixs[ind + 3] = MaxRGB;
				}
			}
		Image image(m, n, "RGBA", IntegerPixel, pixs);
		image.write("im.png");

		//This test works. At fucking last!!!!
		Magick::Image* im = new Magick::Image(Geometry(m, n), color_red);
		im->type(TrueColorMatteType);

		// magickIm->depth(8);
		im->modifyImage();


		unsigned long p;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				// se guarda por columnas       
				p = MaxRGB;
				if (i < m / 2)
					im->pixelColor(i, j, Color(p, 0, 0, MaxRGB));
				else
					im->pixelColor(i, j, Color(p, 0, 0, 0));
			}

		im->syncPixels();
		im->write("a.png");
	} catch (Exception &error_)
	{
		cout << "Caught exception: " << error_.what() << endl;
		return 1;
	}
	return 0;
}
