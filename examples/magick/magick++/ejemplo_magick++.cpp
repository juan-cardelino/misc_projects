#include <Magick++.h>
#include <iostream>
using namespace std;
using namespace Magick;
int main(int argc,char **argv)
{
 try {
   InitializeMagick(NULL);
   // Create an image object and read an image
   Image image( argv[1] );

   // Crop the image to specified size
   // (Geometry implicitly initialized by char *)
   image.crop("100x100+100+100" );

   image.display();
 
  // Write the image to a file
   image.write( argv[2] );

 }
 catch( Exception &error_ )
   {
     cout << "Caught exception: " << error_.what() << endl;
     return 1;
   }
 return 0;
}
