#!/bin/bash


#and
composite -compose Multiply crop.jpg mask_white.png comp_and.jpg
#replace
composite -compose Over mask.png crop.jpg  comp_over.jpg
#lighten
composite -compose Lighten  crop.jpg mask.png comp_light.png
#multiply
composite -compose Multiply crop.jpg mask.png comp_mult.png
#threshold
convert -white-threshold 200 crop.jpg crop_threshold.jpg
