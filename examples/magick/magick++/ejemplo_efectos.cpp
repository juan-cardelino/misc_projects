#include <Magick++.h>
#include <iostream>
using namespace std;
using namespace Magick;

int main(int argc,char **argv)
{
 try {
   InitializeMagick(NULL);
   // Create an image object and read an image
	Magick::Image image( argv[1] );
	Magick::Image top(argv[2]);
	
	Image layer2(top);
	
	//general things
	Magick::Geometry placement(0,0,0,0);
	const MagickCore::ChannelType channel=MagickCore::AlphaChannel;
	
	//setup layer1
	top.gaussianBlurChannel(channel,0,1.5);
	top.quantumOperator(Magick::OpacityChannel, Magick::MultiplyEvaluateOperator, 0.5);
	//top.display();
	
	//setup layer2
	layer2.gaussianBlurChannel(channel,0,1.5);
	layer2.quantumOperator(Magick::OpacityChannel, Magick::MultiplyEvaluateOperator, 0.1);
	//layer2.display();
	
	//image.composite(top, placement, Magick::LightenCompositeOp );
	//image.composite(top, placement, Magick::OverlayCompositeOp );
	image.composite(top, placement, Magick::MultiplyCompositeOp );

	//apply layer 2
	image.composite(layer2, placement, Magick::LightenCompositeOp );
	
	//image.threshold(10000);	//FIXME, we need to know the value of the maxRGB value.
	//para ver: ThresholdCompositeOp
	
   image.display();

  // Write the image to a file
   image.write( argv[3] );

 }
 catch( Exception &error_ )
   {
     cout << "Caught exception: " << error_.what() << endl;
     return 1;
   }
 return 0;
}
