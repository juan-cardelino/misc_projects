#include <string> 
#include <iostream> 
#include <list> 
#include <Magick++.h>

using namespace std; 
using namespace Magick;

int main(int /*argc*/,char **/*argv*/) 
{ 
  try { 

    InitializeMagick(NULL);

    // Create base image (white image of 300 by 200 pixels) 
    Image image( Geometry(300,200), Color("white") );

    // Construct drawing list 
    std::list<Magick::Drawable> drawList;

    // Add some drawing options to drawing list 
    drawList.push_back(DrawableStrokeColor("red")); // Outline color 
    drawList.push_back(DrawableStrokeWidth(5)); // Stroke width 
    drawList.push_back(DrawableFillColor("green")); // Fill color

    // Add a Circle to drawing list 
    drawList.push_back(DrawableCircle(100,100, 50,100));

    // Add a Rectangle to drawing list 
    drawList.push_back(DrawableRectangle(200,100, 270,170));

    // Draw everything using completed drawing list 
    image.draw(drawList);

    // Display the result 
    image.display( ); 
	 
	 
	  Image image2( Geometry(300,200), Color("white") );
	   // Construct drawing list 
    std::list<Magick::Drawable> drawList2;
	 std::list<Magick::Coordinate> coords;
	 coords.push_back(Magick::Coordinate(10,10));
	 coords.push_back(Magick::Coordinate(30,10));
	 coords.push_back(Magick::Coordinate(30,30));
	 coords.push_back(Magick::Coordinate(10,30));
	 drawList2.push_back(DrawablePolygon(coords));
	     
	 image2.draw(drawList2);

    // Display the result 
    image2.display( ); 
	 
  } 
  catch( exception &error_ ) 
    { 
      cout << "Caught exception: " << error_.what() << endl; 
      return 1; 
    }

  return 0; 
}
