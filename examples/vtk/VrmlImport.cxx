#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkVRMLImporter.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include "vtkTransform.h"
#include "vtkAxesActor.h"

int main ( int argc, char *argv[])
{
	if(argc != 2)
	{
		std::cout << "Required arguments: Filename" << std::endl;
		return EXIT_FAILURE;
	}

	std::string filename = argv[1];
	std::cout << "Reading " << filename << std::endl;

	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->AddRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWindowInteractor->SetRenderWindow(renderWindow);

	// VRML Import
	vtkSmartPointer<vtkVRMLImporter> importer = vtkSmartPointer<vtkVRMLImporter>::New();
	importer->SetFileName ( filename.c_str() );
	importer->Read();
	importer->SetRenderWindow(renderWindow);
	importer->Update();

	// ----------------------------------------------------------
	vtkObject* obj1 = importer->GetVRMLDEFObject("obj1");
	vtkActor* obj1Actor = static_cast <vtkActor*> (obj1);
	double colorAbajo[3] = {1,0,0};
	obj1Actor->GetProperty()->SetColor(colorAbajo);
	vtkTransform *tobj1 = vtkTransform::New();
	tobj1->SetMatrix(obj1Actor->GetMatrix());
	std::cout << "Box Abajo" << *tobj1 << std::endl;

	/*
	vtkObject* boxarriba = importer->GetVRMLDEFObject("boxarriba");
	vtkActor* boxArribaActor = static_cast <vtkActor*> (boxarriba);
	double colorArriba[3] = {0,1,0};
	boxArribaActor->GetProperty()->SetColor(colorArriba);
	vtkTransform *tBoxArriba = vtkTransform::New();
	tBoxArriba->SetMatrix(boxArribaActor->GetMatrix());
	std::cout << "Box Arriba" << *tBoxArriba << std::endl;
	*/

	// axes
	vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();
	//axes->AxisLabelsOff();
	double l[3]={30,30,30};
	axes->SetTotalLength(l);
	renderer->AddActor(axes);


	renderWindow->Render();
	renderWindowInteractor->Start();

	return EXIT_SUCCESS;
}