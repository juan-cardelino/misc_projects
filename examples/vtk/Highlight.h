#include <vtkTransform.h>
#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkProperty.h>
#include <vtkPlanes.h>
#include <vtkObjectFactory.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkPointSource.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkAreaPicker.h>
#include <vtkExtractGeometry.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkInteractorObserver.h> 
#include <vtkCommand.h>
#include <vtkProp3DCollection.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

		double verde[3]={0,1,0};
		double amarillo[3]={0,1,1};
		double rojo[3]={1,0,0};
double azul[3]={0,0,1};

void add_actor(vtkSmartPointer<vtkRenderer> renderer, int option)
{
  vtkSmartPointer<vtkPointSource> pointSource = 
      vtkSmartPointer<vtkPointSource>::New();
  pointSource->SetNumberOfPoints(20);
  pointSource->Update();
 
	vtkTransform *trans= vtkTransform::New();


  //create a mapper and actor
  vtkSmartPointer<vtkPolyDataMapper> mapper = 
      vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(pointSource->GetOutputPort());
 
  vtkSmartPointer<vtkActor> actor = 
      vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);



switch(option)
{
case 1:
trans->Translate(5,0,0);
actor->GetProperty()->SetColor(rojo);
break;
case 2:
trans->Translate(0,0,0);
actor->GetProperty()->SetColor(verde);
break;
case 3:
trans->Translate(-5,0,0);
actor->GetProperty()->SetColor(azul);
break;

}

actor->SetUserMatrix(trans->GetMatrix());
// add the actors to the scene
  renderer->AddActor(actor);
}

void save_screenshot(vtkSmartPointer<vtkRenderWindow> renwin)
{
//save image
		vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter =
				vtkSmartPointer<vtkWindowToImageFilter>::New();
		windowToImageFilter->SetInput(renwin);
		windowToImageFilter->Update();
	printf("saving screenshots\n");
		vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
		writer->SetFileName("screenshot.png");
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();
}

void fill_picklist(vtkSmartPointer<vtkRenderer> renderer)
{
	vtkActorCollection *actors = renderer->GetActors();
		actors->InitTraversal();
		vtkSmartPointer<vtkActor> actor=actors->GetNextActor();
		int i=0;
		int visible_actors=0;
		while((actor != NULL) )
		{
			if(actor->GetVisibility())
				visible_actors++;

			actor=actors->GetNextActor();
		}

}
