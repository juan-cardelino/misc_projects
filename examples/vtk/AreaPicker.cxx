#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleTrackball.h>
#include <vtkAreaPicker.h>
#include <vtkCallbackCommand.h>
 
void PickCallbackFunction ( vtkObject* caller, long unsigned int eventId, void* clientData, void* callData );
 
int main(int, char *[])
{
  //create a set of points
  vtkSmartPointer<vtkPoints> points = 
      vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> vertices = 
      vtkSmartPointer<vtkCellArray>::New();
  vtkIdType pid[1];
  pid[0] = points->InsertNextPoint ( 1.0, 0.0, 0.0 );
  vertices->InsertNextCell ( 1,pid );
  pid[0] = points->InsertNextPoint ( 0.0, 0.0, 0.0 );
  vertices->InsertNextCell ( 1,pid );
  pid[0] = points->InsertNextPoint ( 0.0, 1.0, 0.0 );
  vertices->InsertNextCell ( 1,pid );
 
  //create a polydata
  vtkSmartPointer<vtkPolyData> polydata = 
      vtkSmartPointer<vtkPolyData>::New();
  polydata->SetPoints ( points );
  polydata->SetVerts ( vertices );
 
  //create a mapper
  vtkSmartPointer<vtkPolyDataMapper> mapper = 
      vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputData(polydata);
 
  // create an actor
  vtkSmartPointer<vtkActor> actor = 
      vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
 
  // a renderer and render window
  vtkSmartPointer<vtkRenderer> renderer = 
      vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
      vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
 
  vtkSmartPointer<vtkAreaPicker> areaPicker = 
      vtkSmartPointer<vtkAreaPicker>::New();
 
  // an interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
      vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetPicker(areaPicker);
  // add the actors to the scene
  renderer->AddActor(actor);
  renderer->SetBackground(0,0,0.2); // Background color dark blue
 
  // render an image (lights and cameras are created automatically)
  renderWindow->Render();
 
  // For vtkInteractorStyleRubberBandPick - use 'r' and left-mouse to draw a selection box used to pick
  vtkSmartPointer<vtkInteractorStyleRubberBandPick> style = vtkSmartPointer<vtkInteractorStyleRubberBandPick>::New();
 
  // For vtkInteractorStyleTrackballCamera - use 'p' to pick at the current mouse position
  //vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = 
  //   vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview
 
  renderWindowInteractor->SetInteractorStyle( style );
 
  vtkSmartPointer<vtkCallbackCommand> pickCallback = 
      vtkSmartPointer<vtkCallbackCommand>::New();
  pickCallback->SetCallback ( PickCallbackFunction );
 
  areaPicker->AddObserver ( vtkCommand::EndPickEvent, pickCallback );
 
  // begin mouse interaction
  renderWindowInteractor->Start();
 
  return EXIT_SUCCESS;
}
 
 
void PickCallbackFunction ( vtkObject* vtkNotUsed(caller), long unsigned int vtkNotUsed(eventId), void* vtkNotUsed(clientData), void* vtkNotUsed(callData) )
{
  std::cout << "Pick." << std::endl;
}
