#include <vtkTransform.h>
#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkProperty.h>
#include <vtkPlanes.h>
#include <vtkObjectFactory.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkPointSource.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkAreaPicker.h>
#include <vtkExtractGeometry.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkInteractorObserver.h> 
#include <vtkCommand.h>
#include <vtkProp3DCollection.h>
#include "Highlight.h"

class vtkMyCallback : public vtkCommand
{
public:
void SetPoints(vtkSmartPointer<vtkPolyData> points) {this->Points = points;}
	vtkMyCallback() { 
picker = NULL; 
Points = NULL;
     this->SelectedMapper = vtkSmartPointer<vtkDataSetMapper>::New();
      this->SelectedActor = vtkSmartPointer<vtkActor>::New();
      this->SelectedActor->SetMapper(SelectedMapper);
};
	static vtkMyCallback *New() { return new vtkMyCallback; }
	void PrintSelf(ostream&, vtkIndent) { }
	void PrintTrailer(ostream&, vtkIndent) { }
	void PrintHeader(ostream&, vtkIndent) { }
	void CollectRevisions(ostream&) {}

	void SetPicker(vtkAreaPicker* a)
		{
			picker = a;
		};
void SetRenderWindow(vtkRenderWindow* r)
		{
			renWin = r;
		};

	virtual void Execute(vtkObject *caller, unsigned long, void*)
	{
		cout << "<Execute>"<<endl;
bool extract_geometry=false;
if(Points!=NULL)
{
vtkPlanes* frustum = picker->GetFrustum();
 	cout<<"frustum"<< *frustum<<endl;
      vtkSmartPointer<vtkExtractGeometry> extractGeometry = 
          vtkSmartPointer<vtkExtractGeometry>::New();
      extractGeometry->SetImplicitFunction(frustum);
	  extractGeometry->SetInputData(this->Points);
      extractGeometry->Update();
 
      vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter = 
          vtkSmartPointer<vtkVertexGlyphFilter>::New();
      glyphFilter->SetInputConnection(extractGeometry->GetOutputPort());
      glyphFilter->Update();
 
      vtkPolyData* selected = glyphFilter->GetOutput();
      cout << "Selected " << selected->GetNumberOfPoints() << " points." << endl;
      cout << "Selected " << selected->GetNumberOfCells() << " cells." << endl;
	  this->SelectedMapper->SetInputData(selected);
 
      this->SelectedActor->GetProperty()->SetColor(1.0, 0.0, 0.0); //(R,G,B)
      this->SelectedActor->GetProperty()->SetPointSize(3);
 
      renWin->GetRenderers()->GetFirstRenderer()->AddActor(SelectedActor);
      renWin->Render();
      //this->HighlightProp(NULL);
}
	

vtkPropCollection* props_list_pick=picker->GetPickList();
props_list_pick->InitTraversal();
vtkSmartPointer<vtkProp> prop_pick=props_list_pick->GetNextProp();
		
	int pickable_actors=0;
while((prop_pick != NULL) )
		{
			pickable_actors++;
			prop_pick=props_list_pick->GetNextProp();
		}

cout<<"Pickable actors: "<<pickable_actors<<endl;

	vtkProp3DCollection *props=picker->GetProp3Ds();
props->InitTraversal();
		vtkSmartPointer<vtkProp3D> prop=props->GetNextProp3D();

		int visible_actors=0;
		while((prop != NULL) )
		{
			visible_actors++;
			prop=props->GetNextProp3D();
		}

		cout<<"visible actors: "<<visible_actors<<endl;

		
if(visible_actors>0)
{
vtkSmartPointer<vtkActor> a=picker->GetActor();
//cout<<"actor: "<<*a<<endl;
a->GetProperty()->SetColor(amarillo);
renWin->Render();
}

cout << "</Execute>"<<endl;

	}
private:
	vtkAreaPicker* picker;
	    vtkSmartPointer<vtkPolyData> Points;
    vtkSmartPointer<vtkActor> SelectedActor;
    vtkSmartPointer<vtkDataSetMapper> SelectedMapper;
vtkSmartPointer<vtkRenderWindow> renWin;
};



 
int main(int, char *[])
{
 

 
  // a renderer and render window
  vtkSmartPointer<vtkRenderer> renderer = 
      vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
      vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
renderWindow->SetSize(300,300);

  vtkSmartPointer<vtkAreaPicker> areaPicker = 
      vtkSmartPointer<vtkAreaPicker>::New();
  // an interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
      vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetPicker(areaPicker);
  renderWindowInteractor->SetRenderWindow(renderWindow);
 


  
  //renderer->SetBackground(1,1,1); // Background color white
 
	 add_actor(renderer,1);
	 add_actor(renderer,2);
	 add_actor(renderer,3);
  renderWindow->Render();
 
  areaPicker->AreaPick(0,0,300,300,renderer);

 
vtkMyCallback *callback = vtkMyCallback::New();
	callback->SetPicker(areaPicker);
//only used to extract geometry
//callback->SetPoints(pointSource->GetOutput());
	renderWindowInteractor->AddObserver(vtkCommand::UserEvent, callback);
callback->SetRenderWindow(renderWindow);


save_screenshot(renderWindow);

  // begin mouse interaction
  renderWindowInteractor->Start();
 



  return EXIT_SUCCESS;
}
