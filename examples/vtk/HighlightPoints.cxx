#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkProperty.h>
#include <vtkPlanes.h>
#include <vtkObjectFactory.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkPointSource.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkAreaPicker.h>
#include <vtkExtractGeometry.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkProp3DCollection.h> 
#include "Highlight.h"

// Define interaction style
class InteractorStyle : public vtkInteractorStyleRubberBandPick
{
  public:
    static InteractorStyle* New();
    int InteractorStyle::vtkTypeRevisionMacro(InteractorStyle,vtkInteractorStyleRubberBandPick);
 
    InteractorStyle()
    {
 
      this->SelectedMapper = vtkSmartPointer<vtkDataSetMapper>::New();
      this->SelectedActor = vtkSmartPointer<vtkActor>::New();
      this->SelectedActor->SetMapper(SelectedMapper);
 
    }
 
    virtual void OnLeftButtonUp() 
    {
      // forward events
      vtkInteractorStyleRubberBandPick::OnLeftButtonUp();
vtkSmartPointer<vtkAreaPicker> picker=static_cast<vtkAreaPicker*>(this->GetInteractor()->GetPicker());
if(Points!=NULL)
{
 
      vtkPlanes* frustum = picker->GetFrustum();
 	//cout<<"frustum"<< *frustum<<endl;
      vtkSmartPointer<vtkExtractGeometry> extractGeometry = 
          vtkSmartPointer<vtkExtractGeometry>::New();
      extractGeometry->SetImplicitFunction(frustum);
      extractGeometry->SetInputData(this->Points);
      extractGeometry->Update();
 
      vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter = 
          vtkSmartPointer<vtkVertexGlyphFilter>::New();
      glyphFilter->SetInputConnection(extractGeometry->GetOutputPort());
      glyphFilter->Update();
 
      vtkPolyData* selected = glyphFilter->GetOutput();
      cout << "Selected " << selected->GetNumberOfPoints() << " points." << endl;
      cout << "Selected " << selected->GetNumberOfCells() << " cells." << endl;
      this->SelectedMapper->SetInputData(selected);
 
      this->SelectedActor->GetProperty()->SetColor(1.0, 0.0, 0.0); //(R,G,B)
      this->SelectedActor->GetProperty()->SetPointSize(3);
 
      this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(SelectedActor);
      this->GetInteractor()->GetRenderWindow()->Render();
      this->HighlightProp(NULL);
}

	//test the selection of whole actors
vtkProp3DCollection *props=picker->GetProp3Ds();
props->InitTraversal();
		vtkSmartPointer<vtkProp3D> prop=props->GetNextProp3D();
		int visible_actors=0;
		while((prop != NULL) )
		{
			visible_actors++;
			prop=props->GetNextProp3D();
		}

		cout<<"visible actors: "<<visible_actors<<endl;

if(visible_actors>0)
{

vtkSmartPointer<vtkActor> a=picker->GetActor();

a->GetProperty()->SetColor(amarillo);
}
    }
 
    void SetPoints(vtkSmartPointer<vtkPolyData> points) {this->Points = points;}
  private:
    vtkSmartPointer<vtkPolyData> Points;
    vtkSmartPointer<vtkActor> SelectedActor;
    vtkSmartPointer<vtkDataSetMapper> SelectedMapper;
 
};
//vtkCxxRevisionMacro(InteractorStyle, "$Revision: 1.1 $");
vtkStandardNewMacro(InteractorStyle);
 
 
int main(int, char *[])
{
 
 
  // a renderer and render window
  vtkSmartPointer<vtkRenderer> renderer = 
      vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
      vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
 
  vtkSmartPointer<vtkAreaPicker> areaPicker = 
      vtkSmartPointer<vtkAreaPicker>::New();
  // an interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
      vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetPicker(areaPicker);
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  // add the actors to the scene
	 add_actor(renderer,1);
	 add_actor(renderer,2);
	 add_actor(renderer,3);
 
 
  renderWindow->Render();
 
  vtkSmartPointer<InteractorStyle> style = 
      vtkSmartPointer<InteractorStyle>::New();
//need to improve this  
//style->SetPoints(pointSource->GetOutput());
  renderWindowInteractor->SetInteractorStyle( style );
 
  // begin mouse interaction
  renderWindowInteractor->Start();
 
  return EXIT_SUCCESS;
}
