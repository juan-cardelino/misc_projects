#include <vtk3DSImporter.h> 
#include <vtkLight.h> 
#include <vtkMapper.h> 
#include <vtkRenderWindow.h> 
#include <vtkRenderWindowInteractor.h> 
#include <vtkRenderer.h> 
#include <vtkSmartPointer.h> 
#include <vtkAxesActor.h>
#include <vtkImporter.h>

int main(int argc, char *argv[]) 
{ 
	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New(); 
	vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow); 
	vtkSmartPointer<vtk3DSImporter> importer = vtkSmartPointer<vtk3DSImporter>::New(); 
	importer->SetFileName("teaport.3ds"); 
	importer->DebugOn(); 
	importer->Read(); 
	vtkSmartPointer<vtkRenderer> renderer = importer->GetRenderer();
	renderWindow->AddRenderer(renderer); 

	// axes
	vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();
	//axes->AxisLabelsOff();
	double l[3]={30,30,30};
	axes->SetTotalLength(l);
	renderer->AddActor(axes);

	//importer->ImportActors(renderer);

	vtkSmartPointer<vtkLight> light = vtkSmartPointer<vtkLight>::New(); 
	light->SetColor(1, 1, 1); 
	light->SetDiffuseColor(1.0, 1.0, 1.0); 
	light->SetIntensity(1); 
	light->SetAmbientColor(2.0, 2.0, 2.0); 
	light->SetLightTypeToHeadlight(); 
	importer->GetRenderer()->AddLight(light); 

	interactor->Initialize(); 
	interactor->Start(); 
} 