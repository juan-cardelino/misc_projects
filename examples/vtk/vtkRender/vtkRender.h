#include <cstdio>
#ifdef WIN32
#include <Windows.h>
#endif

// VTK
#include "vtkMatrix4x4.h"
#include "vtkPerspectiveTransform.h"
#include "vtkTransform.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkRenderWindowInteractor.h" 
#include "vtkSTLReader.h"
#include "vtkPolyDataNormals.h"
#include "vtkPolyDataMapper.h"
#include "vtkAssembly.h"
#include "vtkImageShiftScale.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkSmartPointer.h" 
#include "vtkCommand.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkDoubleArray.h"
#include "vtkTextProperty.h"
#include "vtkWindowToImageFilter.h"
#include "vtkLookupTable.h"
#include "vtkTextActor.h"
#include "vtkAxesActor.h"
#include "vtkScalarBarActor.h"
#include <vtkMath.h>
#include <vtkSphereSource.h>
#include <vtkInteractorObserver.h>

//custom headers of the app
#include "renderWindow.h" 

#include <vector>
#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

class VipsDrillGuide
{
public:

/*	void loadImage4Registration(
		std::string imageFileName,
		igtbTypes::FloatingPointGreyLevImage2DType::Pointer &rawImage
		);
*/

void Rendering(
	std::string model3Dstlfile_STLmodel,
	double viewAngle,
	double eye[3],
	double center[3],
	double up[3],
	double size[2]
);

VipsDrillGuide();

double size[2];

public:
	RenderWindowZero* rw0;
};