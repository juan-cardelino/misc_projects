#include "vtkRender.h"

#include <ctime>
//#include "mclmcr.h"
#include <cstdio>
#ifdef WIN32
#include <Windows.h>
#endif

// VTK
#include "vtkTransform.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkSTLReader.h"
#include "vtkSmartPointer.h" 
#include "vtkCamera.h"
#include "vtkAssembly.h"
#include "vtkAxesActor.h"
#include <vtkMath.h>
#include "vtkRenderWindowInteractor.h" 
#include "vtkImageExport.h"
#include "vtkWindowToImageFilter.h"

// OpenCV
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include <stdio.h>

#ifndef C_ARM_FOCAL_LENGTH
#define C_ARM_FOCAL_LENGTH 1000.0
#endif

//double size[2];

VipsDrillGuide::VipsDrillGuide()
{
	this->size[0] = 1024;
	this->size[1] = 1024;
}

void VipsDrillGuide::Rendering(
	std::string model3Dstlfile_STLmodel, 
	double viewAngle, 
	double eye[3], 
	double center[3], 
	double up[3],
	double size[2]
) 
{
	cout<<"create rendering dataset 0"<<endl;
	this->rw0 = new RenderWindowZero(model3Dstlfile_STLmodel, viewAngle, eye, center, up, size,false,true);

	// A renderer and render window
	vtkSmartPointer<vtkRenderer> renderer2 = 
		vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renderWindow2 = 
		vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow2->AddRenderer ( renderer2 );
	rw0->renderer=renderer2;
	rw0->renderWindow=renderWindow2;
	renderer2->AddActor(rw0->objectAssembly);
	// Background color
	renderer2->SetBackground(0.5,0.5,0.5);

	// An interactor
	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
		vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWindowInteractor->SetRenderWindow ( this->rw0->renderWindow );

	// Transform
	double rx, ry, rz;
	rx = 10.0;
	ry = -45.0;
	rz = 10.0;
	vtkSmartPointer<vtkTransform> T = 
		vtkSmartPointer<vtkTransform>::New();
	T->Identity();
	T->Translate(0,0,600);
	T->RotateZ(rx);
	T->RotateY(ry);
	T->RotateX(rz);
	rw0->polyDataTransform->Concatenate(T);
	std::cout<< "Transform: " << *this->rw0->polyDataTransform << std::endl;

	// Configure camera
	cout<<"configure camera"<<endl;
	vtkSmartPointer<vtkCamera> camera2 = renderer2->GetActiveCamera();
	camera2->SetParallelProjection(false);
	camera2->SetPosition(eye[0],eye[1],eye[2]);
	cout<<"camera position: "<<eye[0]<<", "<<eye[1]<<", "<<eye[2]<<endl;
	camera2->SetFocalPoint(center[0],center[1],center[2]); 
	camera2->SetViewUp(up[0],up[1],up[2]);
	camera2->SetViewAngle(viewAngle);
	renderWindow2->SetSize(this->size[0], this->size[1]);

	//cout<<"enable axes"<<endl;
	//rw0->EnableAxes();

	// Render and interact
	this->rw0->renderWindow->Render();
	renderWindowInteractor->Start();

	// Render ---------------------------------------------------------------------------------------------------
	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImageFilter->SetInput(renderWindow2);
	windowToImageFilter->SetInputBufferTypeToRGB();
	windowToImageFilter->Update();

	vtkSmartPointer<vtkImageExport> exporter = vtkImageExport::New();
	exporter->SetInputData(windowToImageFilter->GetOutput());
	exporter->ImageLowerLeftOff();

	windowToImageFilter->Modified();

	// get render window info from exporter
	int memsize = exporter->GetDataMemorySize();
	int* dimensions = exporter->GetDataDimensions();
	int csize = exporter->GetDataNumberOfScalarComponents();

	IplImage* openCV_img;
	openCV_img = cvCreateImage(cvSize(renderWindow2->GetSize()[0], renderWindow2->GetSize()[1]), IPL_DEPTH_8U,  3);

	// export the image data to OpenCV image data
	exporter->Export(openCV_img->imageData);
	openCV_img->widthStep = dimensions[0] * csize;

	// swap R and B channel
	cvCvtColor(openCV_img, openCV_img, CV_BGR2RGB);	
	
	// save image & release image
	char imageFilename[64];
	sprintf(imageFilename, "../../out/renderTz600Rz%0.0fy%0.0fx%0.0f.png",rz,ry,rx);
	cvSaveImage(imageFilename,openCV_img);
	cvReleaseImage(&openCV_img);

	// save transform matrix
	char matrixFilename[64];
	sprintf(matrixFilename, "../../out/renderTz600Rz%0.0fy%0.0fx%0.0f.xml",rz,ry,rx);
	ofstream trans_file;
	trans_file.open (matrixFilename);
	trans_file<<"<Matrix4x4>"<<endl;
	trans_file << *rw0->polyDataTransform->GetMatrix() << endl;
	trans_file<<"</Matrix4x4>"<<endl;
	trans_file.close();

}

int main(int argc , char* argv[] ) 
{
	if ( argc != 2 ) {
		std::cout << "Arguments STLmodel" << std::endl;
		return -1;
	}
	
	VipsDrillGuide* vips = new VipsDrillGuide();
	
	double eye[3]={0.0,0.0,0.0};
	double up[3]={0.0,-1.0,0.0};
	double center[3]={0.0,0.0,C_ARM_FOCAL_LENGTH};
	
	std::string STLmodel(argv[1]);
	
	vips->Rendering(STLmodel,13.04,eye,center,up,vips->size);
	
	return 0;
}

