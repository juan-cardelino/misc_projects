#ifndef RENDER_WINDOWS_H_
#define RENDER_WINDOWS_H_

#include "vtkMatrix4x4.h"
#include "vtkPerspectiveTransform.h"
#include "vtkTransform.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkRenderWindowInteractor.h" 
#include "vtkSTLReader.h"
#include "vtkPolyDataNormals.h"
#include "vtkPolyDataMapper.h"
#include "vtkAssembly.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkSmartPointer.h" 
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkDoubleArray.h"
#include "vtkTextProperty.h"
#include "vtkWindowToImageFilter.h"
#include "vtkLookupTable.h"
#include "vtkTextActor.h"
#include "vtkAxesActor.h"

class RenderWindowZero
{
public:
	vtkSmartPointer<vtkAxesActor> axes;
	vtkRenderer *renderer;
	vtkRenderWindow *renderWindow;
	vtkRenderWindowInteractor *renderWindowInteractor;
	vtkSmartPointer<vtkAssembly> objectAssembly;
	vtkSmartPointer<vtkTransform> polyDataTransform;
	vtkSmartPointer<vtkTextActor> textActorTET;

	vtkActor* m_actor2[1];
	vtkDoubleArray* m_scalars2[1];
	vtkPolyData* m_dataset2[1];

	RenderWindowZero(
		std::string model3Dstlfile_STLmodel,
		double viewAngle,
		double eye[3],
		double center[3],
		double up[3],
		double size[2],
		bool enableScalars,
		bool add_bb
		);

	void AddBb(
		double radius,
		double center[3],
		double color[3]
	);

	void EnableAxes();

};

#endif //RENDER_WINDOWS_H_