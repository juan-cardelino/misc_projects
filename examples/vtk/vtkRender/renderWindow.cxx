#include "renderWindow.h" 

// VTK
#include "vtkMatrix4x4.h"
#include "vtkPerspectiveTransform.h"
#include "vtkTransform.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkRenderWindowInteractor.h" 
#include "vtkSTLReader.h"
#include "vtkPolyDataNormals.h"
#include "vtkPolyDataMapper.h"
#include "vtkAssembly.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkSmartPointer.h" 
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkDoubleArray.h"
#include "vtkTextProperty.h"
#include "vtkWindowToImageFilter.h"
#include "vtkLookupTable.h"
#include "vtkActor.h"
#include "vtkTextActor.h"
#include "vtkAxesActor.h"
#include "vtkLODActor.h"
#include <vtkSphereSource.h>

void LoadSTLModel(
	std::string model3Dstlfile,
	vtkActor* actor2add, 
	double color_r, 
	double color_g, 
	double color_b, 
	double opacity,
	vtkSmartPointer<vtkPolyData> &dataset,
	vtkDoubleArray* scalars,
	bool enableScalars = false
	) {
		vtkSTLReader* vtkReader = vtkSTLReader::New();
		vtkReader->SetFileName(model3Dstlfile.c_str());

		vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
		normals->SetInputConnection(vtkReader->GetOutputPort());
		normals->SetFeatureAngle(60.0);
		normals->Update();

		// Create a lookup table to share between the mapper and the scalar bar
		vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
		hueLut->SetHueRange(0.25,1.0);
		hueLut->SetNumberOfTableValues(512);
		hueLut->Build();
		vtkSmartPointer<vtkPolyDataMapper> solidMapper =  vtkSmartPointer<vtkPolyDataMapper>::New();
		solidMapper->SetInputConnection(normals->GetOutputPort());

		if ( enableScalars ) {
			if ( dataset.GetPointer() == NULL ) dataset = vtkPolyData::New();
			dataset = normals->GetOutput();
			int numPts = dataset->GetPoints()->GetNumberOfPoints();
			scalars->SetNumberOfValues( numPts );
			dataset->GetPointData()->SetScalars(scalars);

			solidMapper->ScalarVisibilityOn();
			solidMapper->SetScalarModeToUsePointData();
			solidMapper->SetColorModeToMapScalars();
			solidMapper->SetLookupTable( hueLut );

			actor2add->GetProperty()->SetRepresentationToWireframe();
		}


		// Actor
		actor2add->SetMapper(solidMapper);

		if ( ! enableScalars ) {
			actor2add->GetProperty()->SetAmbient(1.0);
			actor2add->GetProperty()->SetAmbientColor(color_r*0.5,color_g*0.5,color_b*0.5);
			actor2add->GetProperty()->SetDiffuseColor(color_r*0.25,color_g*0.25,color_b*0.25);
			actor2add->GetProperty()->SetSpecularColor(color_r*0.25,color_g*0.25,color_b*0.25);
			actor2add->GetProperty()->SetOpacity( opacity );
			actor2add->GetProperty()->SetBackfaceCulling(true);
		}
		if ( opacity < 1.0 ) actor2add->GetProperty()->SetRepresentationToWireframe();
}

RenderWindowZero::RenderWindowZero
	(
	std::string model3Dstlfile_STLmodel,
	double viewAngle,
	double eye[3],
	double center[3],
	double up[3],
	double size[2],
	bool enableScalars,
	bool add_bb
	)
{
	this->axes =vtkSmartPointer<vtkAxesActor>::New();
	this->axes->AxisLabelsOff();
	double l[3]={30,30,30};
	this->axes->SetTotalLength(l);

	vtkSmartPointer<vtkPolyData> dataSet_STLmodel;
	vtkSmartPointer<vtkDoubleArray> scalars_STLmodel = vtkSmartPointer<vtkDoubleArray>::New();
	vtkSmartPointer<vtkActor> inActor_STLmodel = vtkActor::New();
	LoadSTLModel(model3Dstlfile_STLmodel,inActor_STLmodel,0.08,0.08,0.08,1,dataSet_STLmodel,scalars_STLmodel,enableScalars);

	objectAssembly = vtkAssembly::New();
	if(add_bb){
		double bbColor[3] = {0.2,0.3,0.7};
		double bbPose[3]  = {-1.293, -39.338, -3.369};
		RenderWindowZero::AddBb(3,bbPose,bbColor);
	}
	this->objectAssembly->AddPart(inActor_STLmodel);
	polyDataTransform = vtkTransform::New();
	this->objectAssembly->SetUserTransform(this->polyDataTransform);

	this->m_scalars2[0] = scalars_STLmodel;
	this->m_dataset2[0] = dataSet_STLmodel;
	this->m_actor2[0]   = inActor_STLmodel;
}

void RenderWindowZero::AddBb
	(
	double radius,
	double center[3],
	double color[3]
)
{
	//create source sphere
	vtkSphereSource* bb = vtkSphereSource::New();
	bb->SetRadius(3);
	bb->SetThetaResolution(16);
	bb->Update();

	//transform
	vtkTransform *trans= vtkTransform::New();
	trans->Translate(center);

	//mapper
	vtkPolyDataMapper *map_bb = vtkPolyDataMapper::New();
	map_bb->SetInputData(bb->GetOutput());
	map_bb->Update();

	//actor
	vtkSmartPointer<vtkActor> inActor_BB = vtkSmartPointer<vtkActor>::New();
	inActor_BB->GetProperty()->SetAmbient(0.8);
	inActor_BB->GetProperty()->SetColor(color);
	inActor_BB->SetMapper(map_bb);
	inActor_BB->SetUserMatrix(trans->GetMatrix());
	this->objectAssembly->AddPart(inActor_BB);
}

void RenderWindowZero::EnableAxes()
{
	this->renderer->AddActor(this->axes);
}