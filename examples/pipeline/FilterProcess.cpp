/*
 * File:   FilterProcess.cpp
 * Author: juan
 *
 * Created on August 4, 2013, 7:35 PM
 */

#include "FilterProcess.h"
#include "amt_debug.h"

FilterProcess::FilterProcess()
{
}


FilterProcess::~FilterProcess()
{
}

void FilterProcess::process()
{
	XML_IN;
	printf("process id: %d\n",m_id);
	m_output=0;
	for(std::vector<Filter*>::iterator it=inputs.begin();it!=inputs.end();it++)
	{
		Filter* f=*it;
		m_output+=f->getOutput();
	}

	//m_state=1;
	XML_OUT;
}