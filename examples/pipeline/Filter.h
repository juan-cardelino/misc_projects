#ifndef FILTER_H
#define FILTER_H

#include <vector>

class Filter
{
protected:
	int m_state;
	int m_nb_inputs;
	std::vector<Filter*> inputs;
	float m_output;
	int m_id;
	static int m_last_id;
	
public:
	virtual int update();
	float getOutput();
	void setInput(Filter* f);
	virtual void process();
	Filter();
	~Filter();

};

#endif /* FILTER_H */