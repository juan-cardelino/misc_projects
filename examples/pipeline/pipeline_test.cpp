#include "Filter.h"
#include "FilterSource.h"
#include "FilterProcess.h"

#include "amt_debug.h"

#include <cstdio>

int main(int argc, char** argv)
{
	FilterSource* s1=new FilterSource();
	FilterSource* s2=new FilterSource();

	s1->setData(2.0);
	s2->setData(3.0);

	FilterProcess* p1=new FilterProcess();
	FilterProcess* p2=new FilterProcess();
	FilterProcess* p3=new FilterProcess();

	p1->setInput(s1);
	
	printf("p1: %g\n",p1->getOutput());
	p1->update();
	printf("p1: %g\n",p1->getOutput());
	p1->update();
	
	
	p2->setInput(s2);

	p3->setInput(p1);
	p3->setInput(p2);

	printf("p3: %g\n",p3->getOutput());
	p3->update();

	printf("p3: %g\n",p3->getOutput());

	p3->update();
	printf("p3: %g\n",p3->getOutput());
	
	s1->setData(4.0);
	p3->update();
	printf("p3: %g\n",p3->getOutput());
	
	return 0;
}