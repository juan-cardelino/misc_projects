/*
 * File:   FilterSource.cpp
 * Author: juan
 *
 * Created on August 4, 2013, 7:29 PM
 */

#include "FilterSource.h"
#include "amt_debug.h"

FilterSource::FilterSource()
{
}


FilterSource::~FilterSource()
{
}

void FilterSource::setData(float f)
{
	m_output=f;
	m_state=1;
}

void FilterSource::process()
{
	XML_IN;
	printf("base id: %d\n",m_id);
	m_state=0;
	XML_OUT;
}


int FilterSource::update()
{
	XML_IN;
	int output=0;
	printf("source id: %d\n",m_id);
	if(m_state)
	{
		output=1;
		process();
	}
	
	XML_OUT;
	return output;
}