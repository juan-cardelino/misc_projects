/*
 * File:   FilterSource.h
 * Author: juan
 *
 * Created on August 4, 2013, 7:29 PM
 */

#ifndef FILTERSOURCE_H
#define	FILTERSOURCE_H

#include "Filter.h"

class FilterSource: public Filter
{
public:
	FilterSource();
	~FilterSource();
	
	void setData(float f);
	void process();
	int update();
private:

};

#endif	/* FILTERSOURCE_H */

