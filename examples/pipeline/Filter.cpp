#include "Filter.h"
#include "amt_debug.h"

int Filter::m_last_id=0;

 Filter::Filter()
 {
	 m_state=0;
	 m_id=m_last_id++;
 }

Filter::~Filter()
 {}


int Filter::update()
{
	XML_IN;
	printf("base id: %d\n",m_id);
	
	int input_state=0;

	for(std::vector<Filter*>::iterator it=inputs.begin();it!=inputs.end();it++)
	{
		Filter* f=*it;
		input_state|=f->update();
	}

	if(input_state)
	{
		process();
		m_state=0;
	}

	XML_OUT;
	return input_state;
}

void Filter::setInput(Filter* f)
{
	inputs.push_back(f);
}

void Filter::process()
{}

float Filter::getOutput()
{

	return m_output;
}