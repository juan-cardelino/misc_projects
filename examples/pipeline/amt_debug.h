#ifndef AMT_DEBUG_H_
#define AMT_DEBUG_H_

#define VERBOSE
#define USE_EX

#include <stdio.h>

extern FILE* amt_logfile;

#ifdef VERBOSE
#define xml_out(...) fprintf ( amt_logfile , __VA_ARGS__ )
#else
#define xml_out(...)
#endif

#define XML_IN xml_out("<%s>\n",  __FUNCTION__)
#define XML_IN_T(str) xml_out("<%s title=\"%s\">\n", __FUNCTION__ , str)
#define XML_OUT xml_out("</%s>\n", __FUNCTION__)

#define XML_IN_S printf("<%s>\n",  __FUNCTION__)
#define XML_OUT_S printf("</%s>\n", __FUNCTION__)

int openLog(char* filename, int option);
void closeLog();

#endif //AMT_DEBUG_H_
