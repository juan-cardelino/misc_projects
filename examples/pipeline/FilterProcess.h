/*
 * File:   FilterProcess.h
 * Author: juan
 *
 * Created on August 4, 2013, 7:35 PM
 */

#ifndef FILTERPROCESS_H
#define	FILTERPROCESS_H

#include "Filter.h"

class FilterProcess: public Filter
{
public:
	FilterProcess();
	~FilterProcess();

	void process();
private:

};

#endif	/* FILTERPROCESS_H */

