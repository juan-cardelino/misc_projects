#include "amt_debug.h"

FILE* amt_logfile=stdout;
int amt_log_open=0;

int openLog(char* path, int option)
{
	XML_IN;
	int output=-1;
	char filename[2048];
	sprintf(filename,"%s/amt_imaging_%d.xml",path,option);
	printf("filename: %s\n",filename);
	XML_OUT;
	FILE* fid=fopen(filename,"w+");
	if(fid)
	{
		printf("success\n");
		amt_log_open=1;
		amt_logfile=fid;
		output=0;
	}

	return output;
}

void closeLog()
{
	if(amt_log_open)
	{
		printf("closing log\n");
		fclose(amt_logfile);
		amt_logfile=stdout;
	}
}