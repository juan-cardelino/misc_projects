/**
 * Code for thinning a binary image using Guo-Hall algorithm.
 */
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core_c.h>
#include <stdio.h>


/**
 * This is an example on how to call the thinning function above.
 */
int main(int argc, char** argv)
{
    IplImage* src = cvCreateImage(cvSize(3,3), IPL_DEPTH_8U,3);
	 //cvSet(src, cvScalar(130, 80, 32,255),NULL);
	 cvSetZero(src);
	 cvSet2D(src, 0,0, cvScalar(237, 195, 149,255));
	 cvSet2D(src, 1,0, cvScalar(222, 175, 123,255));
	 cvSet2D(src, 2,0, cvScalar(208, 154, 96,255));
	 cvSet2D(src, 0,1, cvScalar(193, 134, 70,255));
	 cvSet2D(src, 1,1, cvScalar(178, 114, 44,255));
	 cvSet2D(src, 2,1, cvScalar(130, 80, 32,255));
	 IplImage* img_lab = cvCreateImage(cvSize(3,3), IPL_DEPTH_8U,3);
	 //IplImage* img_lab = cvCreateImage(cvSize(3,3), IPL_DEPTH_32F,3);
    cvCvtColor(src, img_lab, CV_RGB2Lab);
printf("original\n");
for (int x = 0; x < img_lab->width; x++)
		for (int y = 0; y < img_lab->height; y++)
		{
			CvScalar c = cvGet2D(src, y, x);
			printf("%g %g %g\n",c.val[0],c.val[1],c.val[2]);
			//printf("%g %g %g\n",c.val[0]*100.0/255.0,c.val[1]-128,c.val[2]-128);
		}

printf("converted\n");
for (int x = 0; x < img_lab->width; x++)
		for (int y = 0; y < img_lab->height; y++)
		{
			CvScalar c = cvGet2D(img_lab, y, x);
			printf("%g %g %g\n",c.val[0],c.val[1],c.val[2]);
			//printf("%g %g %g\n",c.val[0]*100.0/255.0,c.val[1]-128,c.val[2]-128);
		}
	 
	 return 0;
}
