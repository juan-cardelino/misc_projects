#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    Mat before = imread("../examples/iman2.png");
	Mat conv;
	cvtColor(before,conv,CV_RGB2GRAY);
	
	//before.convertTo(conv, CV_8UC1);
    Mat dist;
    //dist.convertTo(dist, CV_32FC1);
    distanceTransform(conv, dist, CV_DIST_L2, CV_DIST_MASK_PRECISE);

    imshow("before", conv);
    imshow("-n", dist);
	 Mat im_norm;
    normalize(dist, im_norm, 0.0, 1.0, NORM_MINMAX);
    imshow("+n", im_norm);
    waitKey();
    return 0;
}
