#include <stdio.h>

#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/legacy/compat.hpp"
#include "opencv2/highgui/highgui.hpp"

IplImage *img0, *img1;
 
void mouseHandler(int event, int x, int y, int flags, void *param)
{
    switch(event) {
        /* left button down */
        case CV_EVENT_LBUTTONDOWN:
            fprintf(stdout, "Left button down (%d, %d).\n", x, y);
            break;
       
        /* right button down */
        case CV_EVENT_RBUTTONDOWN:
            fprintf(stdout, "Right button down (%d, %d).\n", x, y);
            break;
       
        /* mouse move */
        case CV_EVENT_MOUSEMOVE:
			if(img1!=NULL){
				cvReleaseImage(&img1);
			}
            /* draw a rectangle */
            img1 = cvCloneImage(img0);
            cvRectangle(img1,
                        cvPoint(x - 15, y - 15),
                        cvPoint(x + 15, y + 15),
                        cvScalar(0, 0, 255, 0), 2, 8, 0);
            cvShowImage("image", img1);
            break;
    }
}
 
int main(int argc, char** argv)
{
    /* load an image */
    img0 = cvLoadImage("../examples/p2.jpg", CV_LOAD_IMAGE_COLOR);
    img1 = NULL;
    /* create new window and register mouse handler */
    cvNamedWindow("image", CV_WINDOW_AUTOSIZE);
    cvSetMouseCallback( "image", mouseHandler, NULL );
   
    /* display the image */
    cvShowImage("image", img0);
    cvWaitKey(0);
   
    cvDestroyWindow("image");
    cvReleaseImage(&img0);
    cvReleaseImage(&img1);
   
    return 0;
}
 
