#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
// #include "/usr/include/opencv2/core/core_c.h"
// #include "/usr/include/opencv2/core/core_c.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/legacy/compat.hpp"
#include "opencv2/highgui/highgui.hpp"

/*
  int main( int argc, char** argv )
  {
  CvMat the_matrix_data;
  the_matrix_data = read( argv[1], 1 );

  if( argc != 2 || !the_matrix_data.data )
  {
  printf( "No image data \n" );
  return -1;
  }

  CvMat A = cvMat( 5, 5, CV_32F, the_matrix_data);
  cvSave( “my_matrix.xml”, &A );
  // cvSave( “my_matrix.xml”, &A, "my_matrix", "comment" );

  return 0;

  }
*/



int main()
{
    float a[] = { 1,  2,  3,  4,
		   5,  6,  7,  8, 
		   9, 10, 11, 12 };

    CvMat Ma = cvMat(3, 4, CV_32FC1, a);

    int width, height;
    CvSize r = cvSize( width, height);

    printf("\n\nSe supone q cree la matriz.\ny la matriz tiene largo %d y ancho %d\n",width, height);

    cvSave("my_matrix_2.xml", &Ma);


    return 0;
}


/*
  int main()
  {
  CvFileStorage* fs=cvOpenFileStorage("cfg.xml", 0,CV_STORAGE_WRITE);
  cvWriteInt( fs, "frame_count", 10 );
  cvWriteStartWriteStruct( fs, "frame_size", CV_NODE_SEQ);
  cvWriteInt( fs, 0, 320 );
  cvWriteint( fs, 0, 200 );
  cvEndWriteStruct(fs);
  cvWrite( fs, "color_cvt_matrix", cmatrix);
  cvReleaseFileStorage( &fs);
  }
*/
