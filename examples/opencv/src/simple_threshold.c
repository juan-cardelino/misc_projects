#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"

int main ( int argc, char **argv )
{

	IplImage *in = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	IplImage *out = cvCreateImage(cvGetSize(in), in->depth, in->nChannels);
	cvThreshold(in, out, 128, 255, CV_THRESH_BINARY);

	cvSaveImage(argv[2],out, NULL);
	
	cvReleaseImage(&in);
	cvReleaseImage(&out);
  return 0;
}
