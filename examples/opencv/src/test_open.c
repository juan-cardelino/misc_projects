#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"

int main ( int argc, char **argv )
{
  cvNamedWindow( "My Window", 1 );
  IplImage *img = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
  CvFont font;
  double hScale = 1.0;
  double vScale = 1.0;
  int lineWidth = 1;
  //cvInitFont( &font, CV_FONT_HERSHEY_SIMPLEX | CV_FONT_ITALIC, hScale, vScale, 0, lineWidth );
  //cvPutText( img, "Hello World!", cvPoint( 200, 400 ), &font, cvScalar( 255, 255, 0,1 ) );
  cvShowImage( "My Window", img );
  cvWaitKey(0);
  return 0;
}
