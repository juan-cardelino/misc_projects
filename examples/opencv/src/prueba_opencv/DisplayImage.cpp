#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/legacy/compat.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;

int main( int argc, char** argv )
{
    IplImage *image;
    char file_name[] = "lena.jpg";
  
    if( argc != 2 )
    {
    	printf( "\nTo excute program run: DisplayImage <ruta_imagen>\nUsing lena.jpg as dfault\n" );
    }

    //    Mat image;
    //    image = imread( argv[1], 1 );
    
    image = cvLoadImage( argc == 2 ? argv[1] : file_name, 3 );

    int w = image->width;
    int h = image->height;
    int d = image->depth;
    int c = image->nChannels;

    printf("width:\t%d\nheight:\t%d\ndepth:\t%d\nchan:\t%d\n",w,h,d,c);

    // if( !image.data )
    // {
    // 	printf( "No image data \n" );
    // 	return -1;
    // }

    //    namedWindow( "Display Image", CV_WINDOW_AUTOSIZE );
    //    imshow( "Display Image", image );

    cvShowImage("probando titulo",image);
    
    waitKey(0);

    return 0;
}
