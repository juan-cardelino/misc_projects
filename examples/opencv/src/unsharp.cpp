#include "cv.h" // include standard OpenCV headers, same as before
#include "opencv2/highgui/highgui.hpp"
#include <cstdio>

using namespace cv; // all the new API is put into "cv" namespace. Export its content
using namespace std;
// enable/disable use of mixed API in the code below.
#define DEMO_MIXED_API_USE 1

int main( int argc, char** argv )
{
    const char* imagename = argc > 1 ? argv[1] : "lena.jpg";

    Mat img = imread(imagename); // the newer cvLoadImage alternative, MATLAB-style function
    if(img.empty())
    {
        fprintf(stderr, "Can not load image %s\n", imagename);
        return -1;
    }
    
    if( !img.data ) // check if the image has been loaded properly
        return -1;
    
    Mat img_gray;
    cvtColor(img, img_gray, CV_RGB2GRAY); // convert image to YUV color space. The output image will be created automatically

    namedWindow("original image", CV_WINDOW_AUTOSIZE);
       imshow("original image", img);

       namedWindow("image grey-level", CV_WINDOW_AUTOSIZE);
       imshow("image grey-level", img_gray);



    


    const double th_low=20;
    const double th_high=160;
    Mat mask_high,mask_low;
    threshold(img_gray,mask_high,th_high,1, CV_THRESH_BINARY);
    threshold(img_gray,mask_low,th_low,1, CV_THRESH_BINARY_INV);
    namedWindow("mask image high", CV_WINDOW_AUTOSIZE);
    imshow("mask image high ", mask_high*255);
    namedWindow("mask image low", CV_WINDOW_AUTOSIZE);
    imshow("mask image low", mask_low*255);


    Mat img_cut,mask_high_inv,mask_low_inv;
    //add(-1*mask_high, 1, mask_high_inv);
    //add(-1*mask_low, 1, mask_low_inv);
    threshold(img_gray,mask_high_inv,th_high,1, CV_THRESH_BINARY_INV);
    threshold(img_gray,mask_low_inv,th_low,1, CV_THRESH_BINARY);
    //img_cut=img_gray.mul(mask_high_inv).mul(mask_low_inv);
    img_cut=img_gray.mul(mask_high_inv).mul(mask_low_inv)+th_high*mask_high+th_low*mask_low;
    namedWindow("mask image high inv", CV_WINDOW_AUTOSIZE);
    imshow("mask image high inv", mask_high_inv*255);
    namedWindow("mask image low inv", CV_WINDOW_AUTOSIZE);
    imshow("mask image low inv", mask_low_inv*255);
    namedWindow("image cut", CV_WINDOW_AUTOSIZE);
    imshow("image cut", img_cut);

    Mat im_norm;
    normalize(img_cut,im_norm,255,0,CV_MINMAX);
    namedWindow("image norm", CV_WINDOW_AUTOSIZE);
    imshow("image norm", im_norm);


    Mat img_blur; // another Mat constructor; allocates a matrix of the specified size and type
    GaussianBlur(im_norm, img_blur, Size(11, 11), 2, 2); // blur the noise a bit, kernel size is 3x3 and both sigma's are set to 0.5
    // this is counterpart for cvNamedWindow
  namedWindow("blurred image", CV_WINDOW_AUTOSIZE);
  imshow("blurred image", img_blur);

      Mat im_sharp;
      addWeighted(im_norm, 3.5, img_blur, -2.5, 0, im_sharp);
      namedWindow("image sharpen", CV_WINDOW_AUTOSIZE);
           imshow("image sharpen", im_sharp);
      waitKey();



    return 0;
    // all the memory will automatically be released by Vector<>, Mat and Ptr<> destructors.
}

