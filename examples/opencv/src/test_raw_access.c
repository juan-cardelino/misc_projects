#define CV_NO_BACKWARD_COMPATIBILITY

#ifdef _CH_
#pragma package <opencv>
#endif

#include "opencv2/core/core_c.h"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/highgui/highgui_c.h"
#include <stdio.h>

char wndname[] = "Distance transform";
char tbarname[] = "Threshold";
int mask_size = CV_DIST_MASK_5;
int build_voronoi = 0;
int edge_thresh = 100;
int dist_type = CV_DIST_L1;

// The output and temporary images
IplImage* dst = 0;
IplImage* dst2 = 0;
IplImage* src = 0;

// threshold trackbar callback

void copy1(IplImage* src, IplImage* dst)
{

    uint8_t* pixelPtr = (uint8_t*)src->imageData;
    uint8_t* dstPtr = (uint8_t*)dst->imageData;

    int cn = src->nChannels;
    int step=src->widthStep;
    for(int i = 0; i < src->height; i++)
    {
        uint8_t* rowPtr=(uint8_t *)(src->imageData + i*step);
        uint8_t* dstRowPtr=(uint8_t *)(dst->imageData + i*step);
        for(int j = 0; j < src->width; j++)
        {
         CvScalar p;
        p.val[0] = rowPtr[j*cn + 0]; // B
        p.val[1] = rowPtr[j*cn + 1]; // G
        p.val[2] = rowPtr[j*cn + 2]; // R
        dstRowPtr[j*cn + 0]=p.val[0]; // B
        dstRowPtr[j*cn + 1]=p.val[1]; // G
        dstRowPtr[j*cn + 2]=p.val[2]; // R
    }
}
}

void copy2(IplImage* src, IplImage* dst)
{

    int cn = src->nChannels;
    int step=src->widthStep;
    for(int i = 0; i < src->height; i++)
    {
        for(int j = 0; j < src->width; j++)
        {
            CvScalar p = cvGet2D(src, i, j);
            cvSet2D(dst,i,j,p);
        }
    }
}

int main( int argc, char** argv )
{
    char* filename = argc == 2 ? argv[1] : (char*)"stuff.jpg";

    if( (src = cvLoadImage( filename, CV_LOAD_IMAGE_COLOR )) == 0 )
        return -1;


    dst = cvCreateImage( cvGetSize(src), IPL_DEPTH_8U, 3 );
    dst2 = cvCreateImage( cvGetSize(src), IPL_DEPTH_8U, 3 );

    copy1(src,dst);
    copy2(src,dst2);

    cvSaveImage ("out1.png",dst,NULL);
    cvSaveImage ("out2.png",dst2,NULL);

    return 0;
}
