#include <opencv2/imgproc/imgproc.hpp>
#include "opencv/highgui.h"

using namespace cv;

int main( int argc, char** argv )
{
    Mat src;
    // the first command line parameter must be file name of binary
    // (black-n-white) image
    if( argc != 2 || !(src=imread(argv[1], 0)).data)
        return -1;

    Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);

    //src = src > 1;
    namedWindow( "Source", 1 );
    imshow( "Source", src );

	 //grey = cvCreateImage(cvGetSize(dst), IPL_DEPTH_8U, 1);
	 Mat grey;
	 //cvtColor(src, grey, CV_BGR2GRAY);
    //namedWindow( "grey", 2 );
    //imshow( "grey", grey );
	 
	 cv::threshold(src,grey, 100, 255, CV_THRESH_TOZERO_INV | CV_THRESH_OTSU  );


    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    findContours( grey, contours, hierarchy,
        CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

    // iterate through all the top-level contours,
    // draw each connected component with its own random color
    int idx = 0;
    for( ; idx >= 0; idx = hierarchy[idx][0] )
    {
        Scalar color( rand()&255, rand()&255, rand()&255 );
        drawContours( dst, contours, idx, color, CV_FILLED, 8, hierarchy );
    }

    namedWindow( "Components", 1 );
    imshow( "Components", dst );
    waitKey(0);
}
