/**
 * Display video from webcam
 *
 * Author  Nash
 * License GPL
 * Website http://nashruddin.com
 */
 
#include <stdio.h>

#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/legacy/compat.hpp"
#include "opencv2/highgui/highgui.hpp"

int main( int argc, char **argv )
{
    CvCapture *capture = 0;
    IplImage  *frame = 0;
    int       key = 0;

IplImage *frames[10];
int      i = 0;

    /* initialize camera */
    capture = cvCaptureFromCAM( 0 );

    /* always check */
    if ( !capture ) {
        fprintf( stderr, "Cannot open initialize webcam!\n" );
        return 1;
    }

    /* create a window for the video */
    cvNamedWindow( "result", CV_WINDOW_AUTOSIZE );

    while( key != 'q' ) {
		/* get a frame */
        frame = cvQueryFrame( capture );

        /* always check */
        if( !frame ){
		  fprintf(stderr,"no frame\n");
		  break;
        }
        /* display current frame */
        cvShowImage( "result", frame );
        
  /* copy first 10 frames to buffer */
    if( i < 10 ) {
        frames[i] = cvCloneImage( frame );
    }
i++;
    printf("frame: %d\n",i);
        /* exit if user press 'q' */
        key = cvWaitKey( 10  );
    }


/* save images in buffer to disk */
/*
char *filename = (char*)"";
for( i = 0 ; i < 10 ; i++ ) {
    sprintf( filename, "%d.jpg", (i+1) );
    printf("saving: %s\n",filename);
    cvSaveImage( filename, frames[i] );
}
*/
 cvSaveImage( "prueba.jpg", frame, NULL );
        cvShowImage( "result", frame );
            while( key != 'q' ) {
        key = cvWaitKey( 1 );
        }
    /* free memory */
    cvDestroyWindow( "result" );
    cvReleaseCapture( &capture );

    return 0;
}
