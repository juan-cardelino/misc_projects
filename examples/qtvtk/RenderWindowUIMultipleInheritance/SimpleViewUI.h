#ifndef SimpleView_H
#define SimpleView_H
 
#include "ui_SimpleViewUI.h"
 
#include <QMainWindow>

#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include "vtkSmartPointer.h"

class SimpleView : public QMainWindow, private Ui::SimpleView
{
  Q_OBJECT
public:
 
  SimpleView();
 
public slots:
 
  virtual void slotExit();
  virtual void slotScrewA();
  virtual void slotScrewB();

private:
  vtkSmartPointer<vtkActor> sphereActor;
  vtkSmartPointer<vtkRenderer> renderer; 
};
 
#endif
