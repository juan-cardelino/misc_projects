#include "SimpleViewUI.h"
 
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include "vtkSmartPointer.h"
#include "vtkProperty.h"
#include "vtkTransform.h"


using namespace std;

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)

void create_cylinder_4(vtkSmartPointer<vtkAssembly> objectAssembly, double a[3], double t[3], double color[3], double height, double radius)
{
	XML_IN;

	//create cylinder
		vtkCylinderSource* cilinder = vtkCylinderSource::New();
		cilinder->SetHeight(height);
		cilinder->SetRadius(radius);
		cilinder->SetResolution(64);
		cilinder->Update();

		//mappers
		vtkPolyDataMapper *map_cil = vtkPolyDataMapper::New();
		map_cil->SetInputData(cilinder->GetOutput());
		map_cil->Update();
		//actors
		vtkActor *aCilinder = vtkActor::New();
		aCilinder->GetProperty()->SetAmbient(0.1);
		aCilinder->GetProperty()->SetColor(color);
		aCilinder->SetMapper(map_cil);
		//aCilinder->GetProperty()->SetRepresentationToWireframe();

		//add actors to renderer
		//	Renderer->AddActor(aCilinder);
		objectAssembly->AddPart(aCilinder  );

		//transforms to position the actors
		vtkTransform *trans= vtkTransform::New();
		trans->RotateX(a[0]);
		trans->RotateY(a[1]);
		trans->RotateZ(a[2]);
		trans->Translate(t);
		aCilinder->SetUserMatrix(trans->GetMatrix());


	XML_OUT;
}

void create_cylinder_3(vtkSmartPointer<vtkAssembly> objectAssembly)
{
	XML_IN;
	//define some colors
	double verde[3]={0,0.3,0};
	double amarillo[3]={0,0.1961,0.1961};
	double rojo[3]={1,0,0};
	double negro[3]={0.1,0.1,0.1};
	double blanco[3]={1,1,1};

	double height = 100;
	double radius = 5;

	double a1[3]={90,0,0};
	double t1[3]={-10,10,0};
	create_cylinder_4(objectAssembly, a1,t1, negro,height, radius);

	double a2[3]={90,0,0};
	double t2[3]={10,10,0};
	create_cylinder_4(objectAssembly,a2,t2, negro,height, radius);


	double a3[3]={90,0,90};
	double t3[3]={-50,10,0};
	create_cylinder_4(objectAssembly,a3,t3, negro,height, radius);

	XML_OUT;
}



void configure_camera_3(vtkSmartPointer<vtkCamera> camera, float pose[])
{
	//camera->SetViewAngle(12.8766);
	camera->SetViewAngle(13.04);
	camera->SetFocalPoint(0,0,1); //set up initial view orientation
	double l=1000;
	camera->SetPosition(0,0,0);
	camera->SetViewUp(0,-1,0);
	
	vtkTransform *rot= vtkTransform::New();
	rot->RotateX(-pose[0]);
	rot->RotateY(-pose[1]);
	rot->RotateZ(-pose[2]);

	//move camera
	vtkTransform *trans= vtkTransform::New();
	trans->Translate(-pose[3],-pose[4],-pose[5]);
	rot->Concatenate(trans);

	camera->ApplyTransform(rot);

	camera->SetDistance(l);

}

double fov=13.04;

void transform_actor(vtkSmartPointer<vtkCamera> camera, vtkSmartPointer<vtkAssembly> actor, float pose[])
{
	camera->SetViewAngle(fov);
	camera->SetFocalPoint(0,0,1); //set up initial view orientation
	double l=1000;
	double d=700;
	camera->SetPosition(0,0,0);
	camera->SetViewUp(0,-1,0);
	camera->SetDistance(l);

	vtkTransform *rot2= vtkTransform::New();
	rot2->RotateX(-pose[0]);
	rot2->RotateY(-pose[1]);
	rot2->RotateZ(-pose[2]);

	vtkTransform *rot= vtkTransform::New();
	rot->RotateX(pose[0]);
	rot->RotateY(pose[1]);
	rot->RotateZ(pose[2]);
	vtkMatrix4x4 *m1=vtkMatrix4x4::New();
	rot2->GetInverse(m1);

	vtkTransform *trans= vtkTransform::New();
	trans->Translate(pose[3],pose[4],pose[5]);
	//trans->Concatenate(rot);
	trans->Concatenate(m1);

	vtkMatrix4x4 *m=actor->GetUserMatrix();
	actor->SetUserMatrix(trans->GetMatrix());

}

int _main( int argc, char** argv )
{
	int w_size=512;

	cout<<"creating render window..."<<endl;
	vtkSmartPointer<vtkRenderer> ren = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renWin= vtkSmartPointer<vtkRenderWindow>::New();
	renWin->AddRenderer(ren);
	renWin->SetSize(w_size, w_size);
	cout<<"multisample: "<<renWin->GetMultiSamples()<<endl;
	double bg_color[3]={1,1,1};

	double verde[3]={0,0.3,0};
	double amarillo[3]={0,0.1961,0.1961};
	double rojo[3]={1,0,0};
	double negro[3]={0.1,0.1,0.1};
	double blanco[3]={1,1,1};

	double height = 100;
	double radius = 5;

	double a[3]={90,0,0};
	double t[3]={-10,10,0};

	vtkSmartPointer<vtkAssembly> objectAssembly = vtkAssembly::New();


 create_cylinder_3(objectAssembly);
ren->AddActor(objectAssembly);
 // an interactor
	vtkSmartPointer<vtkRenderWindowInteractor> RenderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	RenderWindowInteractor->SetRenderWindow(renWin);

	//configurar cámara
	vtkSmartPointer<vtkCamera> camera = vtkSmartPointer<vtkCamera>::New();
		float pose1[6]={0.00, 0.0,  0.00, 0.00,  0.00, 600.00};
		float pose2[6]={30.00, 00.0,  0.00, 0.00,  0.00, 600.00};
	configure_camera_3(camera,pose2);
	//transform_actor(camera,objectAssembly,pose1);
	ren->SetActiveCamera(camera);
	//double bg_color[3]={0,0,0};
	//double bg_color[3]={0,0,0};
	ren->SetBackground(bg_color);




/*
	renderLarge =vtkSmartPointer<vtkRenderLargeImage>::New();
			  renderLarge->SetInput(ren);
			  renderLarge->SetMagnification(2.0);
*/

	// render an image (lights and cameras are created automatically)
	renWin->Render();

	// begin mouse interaction



	RenderWindowInteractor->Start();
}



// Constructor
SimpleView::SimpleView()
{
  this->setupUi(this);
 
  // Sphere
  vtkSmartPointer<vtkSphereSource> sphereSource =
    vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->Update();
  vtkSmartPointer<vtkPolyDataMapper> sphereMapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  sphereMapper->SetInputConnection(sphereSource->GetOutputPort());
  sphereActor =
    vtkSmartPointer<vtkActor>::New();
  sphereActor->SetMapper(sphereMapper);
 
  // VTK Renderer
  renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor(sphereActor);
 
  // VTK/Qt wedded
  this->qvtkWidget->GetRenderWindow()->AddRenderer(renderer);
 
  // Set up action signals and slots
  connect(this->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
 
};
 
void SimpleView::slotExit()
{
  qApp->exit();
}

void SimpleView::slotScrewA()
{
  cout<<"hola"<<endl;
  sphereActor->GetProperty()->SetColor(1,0,0);
  this->qvtkWidget->GetRenderWindow()->GetInteractor()->Render();
}

void SimpleView::slotScrewB()
{
  cout<<"hola B"<<endl;
  vtkSmartPointer<vtkTransform> trans= vtkSmartPointer<vtkTransform>::New();
  double t[3]={1,0,0};
  trans->Translate(t);
  sphereActor->SetUserTransform(trans);
  this->qvtkWidget->GetRenderWindow()->GetInteractor()->Render();
  //renderer->Render();
}
