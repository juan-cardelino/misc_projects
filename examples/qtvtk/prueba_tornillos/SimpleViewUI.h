#ifndef SimpleView_H
#define SimpleView_H
 
#include "ui_SimpleViewUI.h"
 
#include <QMainWindow>
#include <QtGui>

#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include "vtkSmartPointer.h"
#include "vtkAssembly.h"

class SimpleView : public QMainWindow, private Ui::SimpleView
{
  Q_OBJECT
public:
 
  SimpleView();
  //SimpleView(vtkSmartPointer<vtkRenderer> renderer);

public slots:
 
  virtual void slotExit();
  virtual void slotScrewA();
  virtual void slotScrewB();
  virtual void slotScrewC();
  virtual void slotScrewD();
  virtual void slotC_l();
  virtual void slotC_r();
  virtual void slotC_up();
  virtual void slotC_dw();

  //plate controls
  virtual void slot_plate_Q();
  virtual void slot_plate_W();
  virtual void slot_plate_A();
  virtual void slot_plate_S();
  virtual void slot_plate_Z();
  virtual void slot_plate_X();

  //plate controls
  virtual void slot_plate_U();
  virtual void slot_plate_D();
  virtual void slot_plate_L();
  virtual void slot_plate_R();
  virtual void slot_plate_I();
  virtual void slot_plate_O();

  //virtual void slotFileOpenAP(vtkSmartPointer<vtkRenderer> ren);
  virtual void slotFileOpenAP();
  virtual void slotFileOpenLAT();

  void resetColors(vtkSmartPointer<vtkAssembly> objectAssembly);
  void rotateScrew(vtkSmartPointer<vtkActor> screwActor, double angle);
  void rotateScrew2(vtkSmartPointer<vtkActor> screwActor, double angle);
  vtkSmartPointer<vtkActor>  create_cylinder_4(vtkSmartPointer<vtkAssembly> objectAssembly, double a[3], double t[3], double color[3], double height, double radius);
  void create_cylinder_3(vtkSmartPointer<vtkAssembly> objectAssembly);
  void configure_camera_3(vtkSmartPointer<vtkCamera> camera, float pose[]);
  void transform_actor(vtkSmartPointer<vtkCamera> camera, vtkSmartPointer<vtkAssembly> actor, float pose[]);
  vtkSmartPointer<vtkActor> stl_importer(vtkSmartPointer<vtkAssembly> objectAssembly, std::string filename);
  void setBackground(QVTKWidget* wid);
  void loadImage(std::string background_filename, QVTKWidget* wid);
  void rotatePlate(vtkSmartPointer<vtkAssembly> plate, double angle, double* axis);
  void translatePlate(vtkSmartPointer<vtkAssembly> plate, double* t);

private:
  vtkSmartPointer<vtkAssembly> objectAssembly;
  vtkSmartPointer<vtkRenderer> renderer_AP; 
  vtkSmartPointer<vtkRenderer> renderer_LAT;
  vtkSmartPointer<vtkActor> c1;
  vtkSmartPointer<vtkActor> c2;
  vtkSmartPointer<vtkActor> c3;
  vtkSmartPointer<vtkActor> plate;
  double speed;
};


class PlateControl : public QWidget
{
  Q_OBJECT

public:
	PlateControl(){};
 
public slots:
	virtual void slotPrueba(){cout<<"pepe"<<endl;};
};

#endif


