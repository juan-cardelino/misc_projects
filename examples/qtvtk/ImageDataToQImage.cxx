#include <vtkImageData.h>
#include <vtkSmartPointer.h>

#include <QColor>
#include <QImage>

#include <QApplication>
#include "SimpleViewUI.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  SimpleView mySimpleView;
  mySimpleView.show();

  return app.exec();

}
