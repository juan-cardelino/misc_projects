#ifndef Form_H
#define Form_H

#include "ui_Form.h"

#include <QMainWindow>

#include <vtkSmartPointer.h>
class vtkEventQtSlotConnect;

class Form : public QMainWindow, private Ui::Form
{
  Q_OBJECT
public:

  Form();

public slots:

  void slot_clicked(vtkObject*, unsigned long, void*, void*);

private:

  vtkSmartPointer<vtkEventQtSlotConnect> Connections;
};

#endif
