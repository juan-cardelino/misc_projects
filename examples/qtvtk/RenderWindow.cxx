#include <QApplication>
 
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkImageViewer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleImage.h>
#include <vtkRenderer.h>
#include <vtkJPEGReader.h>
 
#include <QVTKWidget.h>
#include <QPushButton> 
#include <QtGui>

class Prueba:public QObject
{
private slots:
void test()
{
cout<<"hello"<<endl;
}
};

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  QWidget mainWidget;
  QVTKWidget widget;
  widget.resize(256,256);
  mainWidget.resize(256,256);
 
QGridLayout *layout = new QGridLayout();
 	
 	mainWidget.setLayout(layout);
 

 	layout->addWidget(new QPushButton("Button  1"),0,0);

  //setup sphere
  vtkSmartPointer<vtkSphereSource> sphereSource = 
      vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->Update();
  vtkSmartPointer<vtkPolyDataMapper> sphereMapper = 
      vtkSmartPointer<vtkPolyDataMapper>::New();
  sphereMapper->SetInputConnection(sphereSource->GetOutputPort());
  vtkSmartPointer<vtkActor> sphereActor = 
      vtkSmartPointer<vtkActor>::New();
  sphereActor->SetMapper(sphereMapper);
 
  //setup window
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
      vtkSmartPointer<vtkRenderWindow>::New();
 
  //setup renderer
  vtkSmartPointer<vtkRenderer> renderer = 
      vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer);
 
  renderer->AddActor(sphereActor);
  renderer->ResetCamera();
 
  widget.SetRenderWindow(renderWindow);
  widget.show();
  mainWidget.show();
  //window->setCentralWidget(widget);
  //window->show();
     QPushButton hello("Hello world!");
     hello.resize(100, 30);

     hello.show(); 
  app.exec();
  Prueba *p=new Prueba();
  QObject::connect(&hello, SIGNAL(clicked()), p, SLOT(test())); 
  return EXIT_SUCCESS;
}
