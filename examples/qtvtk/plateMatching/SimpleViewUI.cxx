#include "SimpleViewUI.h"

#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include "vtkSmartPointer.h"
#include "vtkProperty.h"
#include "vtkTransform.h"

#include "vtkStructuredPointsReader.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkPolyData.h"
#include "vtkActor.h"
#include "vtkStructuredPoints.h"
#include "vtkMarchingContourFilter.h"
#include "vtkWindowToImageFilter.h"
#include "vtkPNGWriter.h"
#include "vtkJPEGWriter.h"
#include "vtkSmartPointer.h"
#include "vtkMatrix4x4.h"
#include "vtkImageMapper.h"

#include <vtkSTLReader.h>
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include <vtkWindowToImageFilter.h>
#include "vtkImporter.h"
#include "vtkVRMLImporter.h"
#include "vtkDataSet.h"
#include "vtkActorCollection.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkPolyDataNormals.h"
#include "vtkActor.h"
#include "vtkLODActor.h"
#include "vtkCamera.h"
#include <vtkCommand.h>
#include <vtkPNGWriter.h>
#include <vtkSphereSource.h>
#include <vtkCubeSource.h>
#include "vtkSmartPointer.h"
#include "vtkMatrix4x4.h"
#include <vtkInteractorObserver.h>
#include <vtkTransform.h>
#include <vtkCell.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include "vtkProperty.h"
#include <vtkFeatureEdges.h>
#include <iostream>
#include <fstream>
#include "vtkRenderer.h"
#include <vtkXMLPolyDataReader.h>
#include <vtkAxesActor.h>
#include <cassert>
#include "vtkJPEGReader.h"
#include "vtkPNGReader.h"
#include "vtkImageActor.h"
#include <vtkRenderLargeImage.h>
#include "vtkAssembly.h"
#include "vtkCylinderSource.h"
#include <vtkRendererCollection.h>
#include <iostream>

using namespace std;

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)

double velocities[3] = {0.1,1,10};
static double velocity = 0.1;
double angular_velocities[3] = {1,5,20};
static double angular_velocity = 1;

void SimpleView::loadImage(std::string background_filename, QVTKWidget* wid)
{
	vtkSmartPointer<vtkRenderer> backgroundRenderer = wid->GetRenderWindow()->GetRenderers()->GetFirstRenderer();
	vtkSmartPointer<vtkImageData> imageData;
	//vtkSmartPointer<vtkPNGReader> reader =vtkSmartPointer<vtkPNGReader>::New();
	vtkSmartPointer<vtkJPEGReader> reader =vtkSmartPointer<vtkJPEGReader>::New();
	if( !reader->CanReadFile( background_filename.c_str() ) )
	{
		std::cerr << "Error reading file " << background_filename << std::endl;
		return;
	}
	reader->SetFileName ( background_filename.c_str() );
	reader->Update();
	imageData = reader->GetOutput();

	// Create an image actor to display the image
	vtkSmartPointer<vtkImageActor> imageActor = vtkSmartPointer<vtkImageActor>::New();
	imageActor->SetInputData(imageData);

	// Set up the background camera to fill the renderer with the image
	double origin[3];
	double spacing[3];
	int extent[6];
	imageData->GetOrigin( origin );
	imageData->GetSpacing( spacing );
	imageData->GetExtent( extent );

	vtkCamera* camera = backgroundRenderer->GetActiveCamera();
	camera->ParallelProjectionOn();

	double xc = origin[0] + 0.5*(extent[0] + extent[1])*spacing[0];
	double yc = origin[1] + 0.5*(extent[2] + extent[3])*spacing[1];
	//double xd = (extent[1] - extent[0] + 1)*spacing[0];
	double yd = (extent[3] - extent[2] + 1)*spacing[1];
	double d = camera->GetDistance();
	camera->SetParallelScale(0.5*yd);
	camera->SetFocalPoint(xc,yc,0.0);
	camera->SetPosition(xc,yc,d);

	// Add actors to the renderers
	backgroundRenderer->AddActor(imageActor);
	wid->GetRenderWindow()->Render();
}


void SimpleView::setBackground(QVTKWidget* wid)
{
	// Create a renderer to display the image in the background
	vtkSmartPointer<vtkRenderer> backgroundRenderer =
		vtkSmartPointer<vtkRenderer>::New();

	// Set up the render window and renderers such that there is
	// a background layer and a foreground layer
	backgroundRenderer->SetLayer(0);
	backgroundRenderer->InteractiveOff();

	wid->GetRenderWindow()->SetNumberOfLayers(2);
	wid->GetRenderWindow()->AddRenderer(backgroundRenderer);
}

//void SimpleView::slotFileOpenAP(vtkSmartPointer<vtkRenderer> ren)
void SimpleView::slotFileOpenAP()
{ 
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
	std::cout<<"Selected file: "<< fileName.toUtf8().constData()<<std::endl;
	loadImage(fileName.toUtf8().constData(), this->AP_widget);
}


void SimpleView::slotChangeVelocity()
{

	static int count = 0;
	count++;

	if(count==3)
		count = 0;

	velocity=velocities[count];
	angular_velocity=angular_velocities[count];

	std::cout << "velocity: " << velocity << std::endl;

	//QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
	//std::cout<<"Selected file: "<< fileName.toUtf8().constData()<<std::endl;
	//loadImage(fileName.toUtf8().constData(), this->LAT_widget);
}

void SimpleView::slotChangeOpacity()
{
	changeOpacity(this->objectAssembly);
	this->renderer_AP->GetRenderWindow()->GetInteractor()->Render();
}

void SimpleView::changeOpacity(vtkSmartPointer<vtkAssembly> objectAssembly)
{
	static int op = 0;
	int op_max = 10;

	op=++op%op_max;
	float op_f=op/(float)op_max;

	double color[3]={0,1,0};
	vtkSmartPointer<vtkPropCollection> collection = vtkSmartPointer<vtkPropCollection>::New();
	objectAssembly->GetActors(collection);
	collection->InitTraversal();
	cout<<"number of actors: "<<collection->GetNumberOfItems()<<endl;
	cout<<"opacity: "<<op_f<<endl;
	for(vtkIdType i = 0; i < collection->GetNumberOfItems(); i++)
	{
		cout<<"i: "<<i<<endl;
		vtkSmartPointer<vtkActor> curr_actor=vtkActor::SafeDownCast(collection->GetNextProp());
		curr_actor->GetProperty()->SetColor(color);
		curr_actor->GetProperty()->SetOpacity(op_f);
	}
}

void SimpleView::slotModelLoad()
{
	QString fileName2 = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
	
	plate=stl_importer(objectAssembly,fileName2.toUtf8().constData());
	
	double bounds_data[6], center_data[3];
	plate->GetBounds(bounds_data);
	std::cout << "Bounding Box de la placa: " << bounds_data[0] << ", " << bounds_data[1] << ", " << bounds_data[2] << ", " << bounds_data[3] << ", " << bounds_data[4] << ", " << bounds_data[5] << ", " << bounds_data[6] << ", " << std::endl;
	center_data[0]=-(bounds_data[0]+bounds_data[1])/2;
	center_data[1]=-(bounds_data[2]+bounds_data[3])/2;
	center_data[2]=-(bounds_data[4]+bounds_data[5])/2;
	std::cout << "Centro de la placa: " << center_data[0] << ", " << center_data[1] << ", " << center_data[2] << endl;
	
	//vtkSmartPointer<vtkTransform> t =vtkTransform::SafeDownCast(plate->GetUserTransform());
	//t->Translate(10,10,10);
	vtkSmartPointer<vtkTransform> t =vtkTransform::New();
	t->Translate(center_data);
	plate->SetUserMatrix(t->GetMatrix());

	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
}


/*
vtkSmartPointer<vtkActor>  SimpleView::create_cylinder_4(vtkSmartPointer<vtkAssembly> objectAssembly, double a[3], double t[3], double color[3], double height, double radius)
{
	XML_IN;

	//create cylinder
	vtkCylinderSource* cilinder = vtkCylinderSource::New();
	cilinder->SetHeight(height);
	cilinder->SetRadius(radius);
	cilinder->SetResolution(64);
	cilinder->Update();

	//mappers
	vtkPolyDataMapper *map_cil = vtkPolyDataMapper::New();
	map_cil->SetInputData(cilinder->GetOutput());
	map_cil->Update();
	//actors
	vtkSmartPointer<vtkActor> aCilinder = vtkActor::New();
	aCilinder->GetProperty()->SetAmbient(0.1);
	aCilinder->GetProperty()->SetColor(color);
	aCilinder->SetMapper(map_cil);
	//aCilinder->GetProperty()->SetRepresentationToWireframe();

	//add actors to renderer
	//	Renderer->AddActor(aCilinder);
	objectAssembly->AddPart(aCilinder  );

	//transforms to position the actors
	vtkTransform *trans= vtkTransform::New();
	trans->RotateX(a[0]);
	trans->RotateY(a[1]);
	trans->RotateZ(a[2]);
	trans->Translate(t);
	aCilinder->SetUserMatrix(trans->GetMatrix());

	return aCilinder;
	XML_OUT;
}


void SimpleView::create_cylinder_3(vtkSmartPointer<vtkAssembly> objectAssembly)
{
	XML_IN;
	//define some colors
	double verde[3]={0,0.3,0};
	double amarillo[3]={0,0.1961,0.1961};
	double rojo[3]={1,0,0};
	double negro[3]={0.1,0.1,0.1};
	double blanco[3]={1,1,1};

	double height = 18;
	double radius = 2;

	double a1[3]={90,0,0};
	double t1[3]={-10,10,0};
	c1=create_cylinder_4(objectAssembly, a1,t1, negro,height, radius);

	double a2[3]={90,0,0};
	double t2[3]={10,10,0};
	c2=create_cylinder_4(objectAssembly,a2,t2, negro,height, radius);


	double a3[3]={90,0,90};
	double t3[3]={-50,10,0};
	c3=create_cylinder_4(objectAssembly,a3,t3, negro,height, radius);

	XML_OUT;
}

*/

void SimpleView::configure_camera_3(vtkSmartPointer<vtkCamera> camera, float pose[])
{
	//camera->SetViewAngle(12.8766);
	camera->SetViewAngle(13.04);
	camera->SetFocalPoint(0,0,1); //set up initial view orientation
	double l=1000;
	camera->SetPosition(0,0,0);
	camera->SetViewUp(0,-1,0);

	vtkTransform *rot= vtkTransform::New();
	rot->RotateX(-pose[0]);
	rot->RotateY(-pose[1]);
	rot->RotateZ(-pose[2]);

	//move camera
	vtkTransform *trans= vtkTransform::New();
	trans->Translate(-pose[3],-pose[4],-pose[5]);
	rot->Concatenate(trans);

	camera->ApplyTransform(rot);

	camera->SetDistance(l);

}

double fov=13.04;

void SimpleView::transform_actor(vtkSmartPointer<vtkCamera> camera, vtkSmartPointer<vtkAssembly> actor, float pose[])
{
	camera->SetViewAngle(fov);
	camera->SetFocalPoint(0,0,1); //set up initial view orientation
	double l=1000;
	double d=700;
	camera->SetPosition(0,0,0);
	camera->SetViewUp(0,-1,0);
	camera->SetDistance(l);

	vtkTransform *rot2= vtkTransform::New();
	rot2->RotateX(-pose[0]);
	rot2->RotateY(-pose[1]);
	rot2->RotateZ(-pose[2]);

	vtkTransform *rot= vtkTransform::New();
	rot->RotateX(pose[0]);
	rot->RotateY(pose[1]);
	rot->RotateZ(pose[2]);
	vtkMatrix4x4 *m1=vtkMatrix4x4::New();
	rot2->GetInverse(m1);

	vtkTransform *trans= vtkTransform::New();
	trans->Translate(pose[3],pose[4],pose[5]);
	//trans->Concatenate(rot);
	trans->Concatenate(m1);

	actor->SetUserTransform(trans);

}



// Constructor
SimpleView::SimpleView()
{
	this->setupUi(this);
	double bg_color[3]={1,1,1};
	double verde[3]={0,0.3,0};
	double amarillo[3]={0,0.1961,0.1961};
	double rojo[3]={1,0,0};
	double negro[3]={0.1,0.1,0.1};
	double blanco[3]={1,1,1};

	double height = 100;
	double radius = 5;

	double a[3]={90,0,0};
	double t[3]={-10,10,0};

	objectAssembly = vtkAssembly::New();

	//create_cylinder_3(objectAssembly);

	// VTK Renderer
	renderer_AP = vtkSmartPointer<vtkRenderer>::New();
	renderer_AP->AddActor(objectAssembly);
	renderer_AP->SetLayer(1);

	/*
	renderer_LAT = vtkSmartPointer<vtkRenderer>::New();
	renderer_LAT->AddActor(objectAssembly);
	renderer_LAT->SetLayer(1);
	*/

	//configurar cámara
	vtkSmartPointer<vtkCamera> camera = vtkSmartPointer<vtkCamera>::New();
	float pose1[6]={0.00, 0.0,  0.00, 0.00,  0.00, 600.00};
	float pose2[6]={00.00, 00.0,  0.00, 0.00,  0.00, 600.00};
	float pose3[6]={00.00, 90.0,  0.00, 0.00,  0.00, 600.00};
	configure_camera_3(camera,pose2);

	vtkSmartPointer<vtkCamera> camera_LAT = vtkSmartPointer<vtkCamera>::New();
	configure_camera_3(camera_LAT,pose3);

	//transform_actor(camera,objectAssembly,pose1);
	renderer_AP->SetActiveCamera(camera);
//	renderer_LAT->SetActiveCamera(camera_LAT);
	//double bg_color[3]={0,0,0};
	//double bg_color[3]={0,0,0};
	//renderer_AP->SetBackground(bg_color);

	// VTK/Qt wedded
	this->setBackground(this->AP_widget);
//	this->setBackground(this->LAT_widget);
	this->AP_widget->GetRenderWindow()->AddRenderer(renderer_AP);
//	this->LAT_widget->GetRenderWindow()->AddRenderer(renderer_LAT);

	// Set up action signals and slots
	connect(this->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));

	this->speed=5;
};

void SimpleView::slotExit()
{
	qApp->exit();
}

/*
void SimpleView::slotScrewA()
{
	static int count=0;
	cout<<"hola"<<endl;
	// Extract each actor from the assembly and change its opacity
	vtkSmartPointer<vtkPropCollection> collection =
		vtkSmartPointer<vtkPropCollection>::New();
	objectAssembly->GetActors(collection);
	collection->InitTraversal();
	for(vtkIdType i = 0; i < collection->GetNumberOfItems(); i++)
	{
		vtkActor::SafeDownCast(collection->GetNextProp())->GetProperty()->SetOpacity(0.5);
	}

	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
	std::string title="count: "+count++;
	this->label->setText(QString::fromStdString(title));
}

void SimpleView::slotScrewB()
{
	cout<<"hola B"<<endl;
	vtkSmartPointer<vtkTransform> trans= vtkSmartPointer<vtkTransform>::New();
	double t[3]={1,0,0};
	trans->Translate(t);
	objectAssembly->SetUserTransform(trans);
	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
	//renderer->Render();
}
*/
void SimpleView::resetColors(vtkSmartPointer<vtkAssembly> objectAssembly)
{
	double negro[3]={0.1,0.1,0.1};
	vtkSmartPointer<vtkPropCollection> collection =
		vtkSmartPointer<vtkPropCollection>::New();
	objectAssembly->GetActors(collection);
	collection->InitTraversal();
	for(vtkIdType i = 0; i < collection->GetNumberOfItems(); i++)
	{
		vtkActor::SafeDownCast(collection->GetNextProp())->GetProperty()->SetColor(negro);
	}
}

void SimpleView::rotateScrew(vtkSmartPointer<vtkActor> screwActor, double angle)
{
	double rojo[3]={1,0,0};
	this->resetColors(objectAssembly);
	screwActor->GetProperty()->SetColor(rojo);
	vtkSmartPointer<vtkTransform> trans= vtkSmartPointer<vtkTransform>::New();
	double axis[3]={1,0,0};
	//double angle=1;
	trans->RotateWXYZ(angle,axis);
	vtkMatrix4x4 *m=screwActor->GetMatrix();
	vtkSmartPointer<vtkTransform> curr_trans=vtkSmartPointer<vtkTransform>::New();
	curr_trans->SetMatrix(m);
	curr_trans->Concatenate(trans);
	screwActor->SetUserTransform(curr_trans);

	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
//	this->LAT_widget->GetRenderWindow()->GetInteractor()->Render();
}

void SimpleView::rotateScrew2(vtkSmartPointer<vtkActor> screwActor,double angle)
{
	double rojo[3]={1,0,0};
	this->resetColors(objectAssembly);
	screwActor->GetProperty()->SetColor(rojo);
	vtkSmartPointer<vtkTransform> trans= vtkSmartPointer<vtkTransform>::New();
	double axis[3]={0,0,1};

	trans->RotateWXYZ(angle,axis);
	vtkMatrix4x4 *m=screwActor->GetMatrix();
	vtkSmartPointer<vtkTransform> curr_trans=vtkSmartPointer<vtkTransform>::New();
	curr_trans->SetMatrix(m);
	curr_trans->Concatenate(trans);
	screwActor->SetUserTransform(curr_trans);

	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
//	this->LAT_widget->GetRenderWindow()->GetInteractor()->Render();

}

/*
void SimpleView::slotScrewC()
{
	cout<<"hola C"<<endl;
	rotateScrew(c1,1);
	//renderer->Render();
}


void SimpleView::slotC_l()
{
	cout<<"C left"<<endl;
	rotateScrew2(c1,1);
	//renderer->Render();
}
void SimpleView::slotC_r()
{
	cout<<"C right"<<endl;
	rotateScrew2(c1,-1);
	//renderer->Render();
}
void SimpleView::slotC_up()
{
	cout<<"C up"<<endl;
	rotateScrew(c1,1);
	//renderer->Render();
}
void SimpleView::slotC_dw()
{
	cout<<"C down"<<endl;
	rotateScrew(c1,-1);
	//renderer->Render();
}

void SimpleView::slotScrewD()
{
	cout<<"hola D"<<endl;
	//rotateScrew(c2,1);
	//renderer->Render();
	vtkCamera* camera = renderer_AP->GetActiveCamera();
	camera->Roll(1);
	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();

}

*/

void SimpleView::rotatePlate(vtkSmartPointer<vtkAssembly> plate, double angle, double* axis)
{
	vtkSmartPointer<vtkTransform> trans= vtkSmartPointer<vtkTransform>::New();

	trans->RotateWXYZ(angle,axis);
	vtkMatrix4x4 *m=plate->GetMatrix();
	vtkSmartPointer<vtkTransform> curr_trans=vtkSmartPointer<vtkTransform>::New();
	curr_trans->SetMatrix(m);
	curr_trans->Concatenate(trans);
	plate->SetUserTransform(curr_trans);

	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
//	this->LAT_widget->GetRenderWindow()->GetInteractor()->Render();
}


void SimpleView::translatePlate(vtkSmartPointer<vtkAssembly> plate, double* t)
{
	for(int i=0;i<3;i++)
		t[i]*=this->speed;

	vtkSmartPointer<vtkTransform> trans= vtkSmartPointer<vtkTransform>::New();
	trans->Translate(t);
	vtkMatrix4x4 *m=plate->GetMatrix();
	vtkSmartPointer<vtkTransform> curr_trans=vtkSmartPointer<vtkTransform>::New();
	curr_trans->SetMatrix(m);
	curr_trans->Concatenate(trans);
	plate->SetUserTransform(curr_trans);

	this->AP_widget->GetRenderWindow()->GetInteractor()->Render();
//	this->LAT_widget->GetRenderWindow()->GetInteractor()->Render();
}

void SimpleView::slot_plate_Q()
{
	double axis[3]={1,0,0};
	rotatePlate(objectAssembly,angular_velocity,axis);
}

void SimpleView::slot_plate_W()
{
	double axis[3]={1,0,0};
	rotatePlate(objectAssembly,-angular_velocity,axis);
}

void SimpleView::slot_plate_A()
{
	double axis[3]={0,1,0};
	rotatePlate(objectAssembly,angular_velocity,axis);
}

void SimpleView::slot_plate_S()
{
	double axis[3]={0,1,0};
	rotatePlate(objectAssembly,-angular_velocity,axis);
}

void SimpleView::slot_plate_Z()
{
	double axis[3]={0,0,1};
	rotatePlate(objectAssembly,angular_velocity,axis);
}

void SimpleView::slot_plate_X()
{
	double axis[3]={0,0,1};
	rotatePlate(objectAssembly,-angular_velocity,axis);
}



void SimpleView::slot_plate_U()
{
	double axis[3]={0,-velocity,0};
	translatePlate(objectAssembly,axis);
}

void SimpleView::slot_plate_D()
{
	double axis[3]={0,velocity,0};
	translatePlate(objectAssembly,axis);
}

void SimpleView::slot_plate_L()
{
	double axis[3]={-velocity,0,0};
	translatePlate(objectAssembly,axis);
}

void SimpleView::slot_plate_R()
{
	double axis[3]={velocity,0,0};
	translatePlate(objectAssembly,axis);
}

void SimpleView::slot_plate_I()
{
	double axis[3]={0,0,velocity};
	translatePlate(objectAssembly,axis);
}

void SimpleView::slot_plate_O()
{
	double axis[3]={0,0,-velocity};
	translatePlate(objectAssembly,axis);
}


vtkSmartPointer<vtkActor> SimpleView::stl_importer(vtkSmartPointer<vtkAssembly> objectAssembly, std::string filename)
{
	this->objectAssembly->RemovePart(this->plate);
	vtkMatrix4x4* m = this->objectAssembly->GetMatrix();
	m->Identity();
	this->objectAssembly->SetUserMatrix(m);

	vtkSTLReader* reader = vtkSTLReader::New();
	reader->SetFileName(filename.c_str());

	// Create Normal Vectors to enhance smoothness & illumination
	vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
	normals->SetInputConnection (reader->GetOutputPort());
	normals->SetFeatureAngle(60.0);

	// Mapper
	vtkSmartPointer<vtkPolyDataMapper> SolidMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	SolidMapper->SetInputConnection(normals->GetOutputPort());
	SolidMapper->ScalarVisibilityOff();

	// Actor
	vtkSmartPointer<vtkLODActor> SolidActor = vtkSmartPointer<vtkLODActor>::New();
	SolidActor->SetMapper(SolidMapper);
	SolidActor->GetProperty()->SetAmbient(1);

	double negro[3]={0.1,0.1,0.1};

	SolidActor->GetProperty()->SetColor(negro);

	objectAssembly->AddPart(SolidActor);

	cout<<"translating plate..."<<endl;
	vtkTransform *trans= vtkTransform::New();
	vtkTransform *trans_1= vtkTransform::New();
	//the S1 hole on the model is already at (0,0,0)
	//so we don't need to bring it there anymore
	//now take it to the desired position
	//which is the top of the highest slice
	//trans->Translate(0,0,17);	//for model 9
	//double height=12.5;	//for model 12
	//for model 11
	/*
	double t[3]={0,0,18.5};
	double a[3]={-90,0,90};
	trans->Scale(0.5,0.5,0.5);
	*/
	//move the model so the S1 hole is at (0,0,0)
	//for model 11 with synthes plate
	//double t_center[3]={-0.4,2.2,8.6};	//planned translation
	double t_center[3]={-0.4,1.5,8.6};		//correction for the real model (montevideo version)
	//double t_center[3]={-38,-11,11};		//for philos plate
	trans_1->Translate(t_center);
	//now take it to the desired position
	//which is the top of the highest slice in version 11
	//double t[3]={0,0,17}; //model 11
	//and the surface of the box in version 13
	double t[3]={0,0,0};

	bool use_test_box=false;
	bool is_ap=true;

	double a[3]={0,0,0};
	if(use_test_box) //apply translation only when using balls
	{
		if(is_ap){
			t[2]=23; //model 13
			a[2]=-90; //shared for all models
		}
		else
		{
			//model 13 (erlangen)
			t[0]=38;
			a[0]=-90;
			a[2]=-90;
			//correction for model 13-2 (first erlangen model)
			/*
			a[1]=-0.75;
			t[2]+=0.1;
			t[1]+=0.1;
			*/
			//correction for model 13-3 (second erlangen model)
			t[0]-=1;
			t[2]+=0.4;
			t[1]+=0.2;
		}
	}
	trans_1->Translate(t);
	trans->RotateX(a[0]);
	trans->RotateY(a[1]);
	trans->RotateZ(a[2]);
	trans->Concatenate(trans_1);
	SolidActor->SetUserMatrix(trans->GetMatrix());

	return SolidActor;
}

