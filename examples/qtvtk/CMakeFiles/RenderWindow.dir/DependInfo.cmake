# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Users/juan/juanc/develop/misc_projects/examples/qtvtk/RenderWindow.cxx" "/Users/juan/juanc/develop/misc_projects/examples/qtvtk/CMakeFiles/RenderWindow.dir/RenderWindow.cxx.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "vtkRenderingCore_AUTOINIT=3(vtkInteractionStyle,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)"
  "vtkRenderingVolume_AUTOINIT=1(vtkRenderingVolumeOpenGL)"
  "QT_NO_DEBUG"
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/vtk-6.0"
  "/usr/local/Cellar/qt/4.8.3/include"
  "/usr/local/Cellar/qt/4.8.3/lib/QtCore.framework"
  "/usr/local/Cellar/qt/4.8.3/include/QtGui"
  "/usr/local/Cellar/qt/4.8.3/lib/QtCore.framework/Headers"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
