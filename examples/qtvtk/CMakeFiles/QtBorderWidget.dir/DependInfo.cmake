# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Users/juan/juanc/develop/misc_projects/examples/qtvtk/QtBorderWidget.cxx" "/Users/juan/juanc/develop/misc_projects/examples/qtvtk/CMakeFiles/QtBorderWidget.dir/QtBorderWidget.cxx.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "vtkRenderingCore_AUTOINIT=3(vtkInteractionStyle,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)"
  "vtkRenderingVolume_AUTOINIT=1(vtkRenderingVolumeOpenGL)"
  "QT_NO_DEBUG"
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
