cmake_minimum_required(VERSION 2.6)
 
PROJECT(RenderWindow)
 
FIND_PACKAGE(VTK)
INCLUDE(${VTK_USE_FILE})
 
FIND_PACKAGE(Qt4 REQUIRED)
INCLUDE(${QT_USE_FILE})


SET( EXAMPLE_LIST
#RenderWindow
RenderWindowUI
#QtBorderWidget
)

SET(OBJ SimpleView.cxx SimpleViewUI.cxx)

FOREACH( var ${EXAMPLE_LIST} )
  ADD_EXECUTABLE( ${var} ${var}.cxx ${OBJ} )
TARGET_LINK_LIBRARIES(${var} ${VTK_LIBRARIES} ${QT_LIBRARIES})
ENDFOREACH( var )

