/********************************************************************************
** Form generated from reading UI file 'SimpleView.ui'
**
** Created: Sun Oct 21 03:28:52 2012
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIMPLEVIEW_H
#define UI_SIMPLEVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QWidget>
#include "QVTKWidget.h"

QT_BEGIN_NAMESPACE

class Ui_SimpleView
{
public:
    QAction *actionOpenFile;
    QAction *actionExit;
    QAction *actionPrint;
    QAction *actionHelp;
    QAction *actionSave;
    QWidget *centralwidget;
    QVTKWidget *qvtkWidget;

    void setupUi(QMainWindow *SimpleView)
    {
        if (SimpleView->objectName().isEmpty())
            SimpleView->setObjectName(QString::fromUtf8("SimpleView"));
        SimpleView->resize(541, 583);
        actionOpenFile = new QAction(SimpleView);
        actionOpenFile->setObjectName(QString::fromUtf8("actionOpenFile"));
        actionOpenFile->setEnabled(true);
        actionExit = new QAction(SimpleView);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionPrint = new QAction(SimpleView);
        actionPrint->setObjectName(QString::fromUtf8("actionPrint"));
        actionHelp = new QAction(SimpleView);
        actionHelp->setObjectName(QString::fromUtf8("actionHelp"));
        actionSave = new QAction(SimpleView);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        centralwidget = new QWidget(SimpleView);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        qvtkWidget = new QVTKWidget(centralwidget);
        qvtkWidget->setObjectName(QString::fromUtf8("qvtkWidget"));
        qvtkWidget->setGeometry(QRect(10, 20, 511, 541));
        SimpleView->setCentralWidget(centralwidget);

        retranslateUi(SimpleView);

        QMetaObject::connectSlotsByName(SimpleView);
    } // setupUi

    void retranslateUi(QMainWindow *SimpleView)
    {
        SimpleView->setWindowTitle(QApplication::translate("SimpleView", "SimpleView", 0, QApplication::UnicodeUTF8));
        actionOpenFile->setText(QApplication::translate("SimpleView", "Open File...", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("SimpleView", "Exit", 0, QApplication::UnicodeUTF8));
        actionPrint->setText(QApplication::translate("SimpleView", "Print", 0, QApplication::UnicodeUTF8));
        actionHelp->setText(QApplication::translate("SimpleView", "Help", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("SimpleView", "Save", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SimpleView: public Ui_SimpleView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIMPLEVIEW_H
