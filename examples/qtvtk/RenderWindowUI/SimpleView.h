#ifndef SimpleView_H
#define SimpleView_H

#include <QMainWindow>
#include "ui_SimpleView.h"

class SimpleView : public QMainWindow, public Ui_SimpleView
{
  Q_OBJECT
public:

  // Constructor/Destructor
  SimpleView(); 
  ~SimpleView() {};

public slots:

  virtual void slotExit();

protected:
   void ModifiedHandler();
   
protected slots:

};

#endif // SimpleView_H
