/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/

#include "util/util.h"

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

//  Software Guide : BeginCommandLineArgs
//    INPUTS: {Circle.png}
//    OUTPUTS: {CircleOutput.mha}
//    49 49 10
//  Software Guide : EndCommandLineArgs

// The use of the \doxygen{ScalarChanAndVeseSparseLevelSetImageFilter} is
// illustrated in the following example. The implementation of this filter in
// ITK is based on the paper by Chan And Vese.  This
// implementation extends the functionality of the
// level-set filters in ITK by using region-based variational techniques. These methods
// do not rely on the presence of edges in the images.
//
// ScalarChanAndVeseSparseLevelSetImageFilter expects two inputs.  The first is
// an initial level set in the form of an \doxygen{Image}. The second input
// is a feature image. For this algorithm, the feature image is the original
// raw or preprocessed image. Several parameters are required by the algorithm
// for regularization and weights of different energy terms. The user is encouraged to
// change different parameter settings to optimize the code example on their images.
//
// Let's start by including the headers of the main filters involved in the
// preprocessing.
//
// Software Guide : EndLatex
#include "itkLevelSetSegmentationCommon.h"

#include "itkScalarChanAndVeseSparseLevelSetImageFilter.h"
#include "itkScalarChanAndVeseLevelSetFunctionData.h"
#include "itkConstrainedRegionBasedLevelSetFunctionSharedData.h"
#include "itkFastMarchingImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkAtanRegularizedHeavisideStepFunction.h"
#include <vector>

using namespace std;

class GACDemo: public util::Configurable
{
private:
	int step;
	int verbose;

public:
	int nb_phases;
	int max_iter;
	double max_rms;
	std::string inputImage;
	std::string outputImage;
	std::string initialLevelSet;
	std::string seedFile;
	double propagationScaling;
	double curvatureScaling;
	double initialDistance;
	double sigma;
	double alpha;
	double beta;
	double control_points_weight;
	bool use_initial_level_set;
	bool use_tracking;
	bool use_fixed_level_set;
	bool use_offset;
	bool use_seeds;
	int ind_start;
	int ind_end;
	int magnet_type;

	std::string inputImageFilename;
	std::string outputImageFilename;
	std::string seedFilename;
	std::string initialLevelSetFilename;
	std::string userDistanceFilename;

	GACDemo();
	void configure(const util::Configuration& conf) throw (util::ConfException);
	util::Configuration getConfiguration() const;

	void run();
	//void tracking();
	//void segment();

};




GACDemo::GACDemo()
{
	nb_phases=2;
	max_iter=4000;
	use_initial_level_set=false;
	use_tracking=false;
	step=-1;
	use_fixed_level_set=false;
	use_offset=false;
	use_seeds=false;
	max_rms=0.02;
	ind_start=1;
	ind_end=2;
	control_points_weight=0;
	magnet_type=0;
	curvatureScaling=1;
	propagationScaling=1;
	verbose=0;
}

void GACDemo::configure(const util::Configuration& conf)
throw (util::ConfException)
{
	max_iter = conf.getIntValue("max_iter");
	inputImage = conf.getValue("inputImage");
	outputImage = conf.getValue("outputImage");
	initialLevelSet = conf.getValue("initialLevelSet");
	userDistanceFilename = conf.getValue("userDistanceFilename");
	propagationScaling = conf.getFloatValue("propagationScaling");
	curvatureScaling = conf.getFloatValue("curvatureScaling");
	seedFile = conf.getValue("seedFile");
	initialDistance=conf.getFloatValue("initialDistance");;
	sigma=conf.getFloatValue("sigma");
	alpha=conf.getFloatValue("alpha");
	beta=conf.getFloatValue("beta");
	use_initial_level_set = conf.getBoolValue("use_initial_level_set");
	use_tracking = conf.getBoolValue("use_tracking");
	use_fixed_level_set = conf.getBoolValue("use_fixed_level_set");
	max_rms = conf.getFloatValue("max_rms");
	use_offset = conf.getBoolValue("use_offset");
	use_seeds = conf.getBoolValue("use_seeds");
	ind_start = conf.getIntValue("ind_start");
	ind_end = conf.getIntValue("ind_end");
	control_points_weight=conf.getFloatValue("control_points_weight");
	magnet_type=conf.getIntValue("magnet_type");
	nb_phases=conf.getIntValue("nb_phases");
	verbose=conf.getIntValue("verbose");
}

util::Configuration GACDemo::getConfiguration() const
{
	util::Configuration miConf("base","Configuracion base");

	miConf.addOption("nb_phases","","2");
	miConf.addOption("max_iter","","500");
	miConf.addOption("use_initial_level_set","","false");
	miConf.addOption("inputImage","","in.png");
	miConf.addOption("outputImage","","out.png");
	miConf.addOption("initialLevelSet","","phi.mha");
	miConf.addOption("userDistanceFilename","","dist.mha");
	miConf.addOption("propagationScaling","","1");
	miConf.addOption("curvatureScaling","","1");
	miConf.addOption("seedFile","seed points (text file)","seeds.xml");
	miConf.addOption("initialDistance","","5");
	miConf.addOption("sigma","","1");
	miConf.addOption("alpha","","1");
	miConf.addOption("beta","","1");
	miConf.addOption("use_initial_level_set","","false");
	miConf.addOption("use_tracking","","false");
	miConf.addOption("use_fixed_level_set","","false");
	miConf.addOption("max_rms","","0.02");
	miConf.addOption("use_offset","","false");
	miConf.addOption("use_seeds","","false");
	miConf.addOption("ind_start","","1");
	miConf.addOption("ind_end","","2");
	miConf.addOption("control_points_weight","","0.0");
	miConf.addOption("magnet_type","","0");
	miConf.addOption("verbose","","0");
	return miConf;
}

int main(int argc, const char *argv[] )
{


	util::Logger::setLogLevel(util::Logger::INFO);
	//	cout << 	setprecision(5);
	util::infolog.logln("Starting algorithm");
	GACDemo demo;


	try {
		//
		// get core configuration parameters
		//
		util::Configuration cfg = demo.getConfiguration();
		//
		// create a configuration manager to read and write the configuration
		//
		util::ConfManager conf_manager(cfg);
		//
		// read config. from command line
		//
		cfg = conf_manager.parseConf(argc,argv);
		//
		// configure the algorithm
		//
		demo.configure(cfg);

		//and finally run the algorithm
		demo.run();

	} catch (base::Exception ex) {
		std::cout << "EPA! " << ex.getMessage() << std::endl;
	}

	return 0;
}

void GACDemo::run()
{


	//unsigned int nb_iteration = 300;
	//double rms = 0.;
	double epsilon = 1.;
	double curvature_weight = 0.;
	double area_weight = 0.;
	double reinitialization_weight = 0.;
	double volume_weight = 0.;
	double volume = 0.;
	double l1 = 1.;
	double l2 = 1.;

	//
	//  We now define the image type using a particular pixel type and
	//  dimension. In this case the \code{float} type is used for the pixels
	//  due to the requirements of the smoothing filter.
	//
	const unsigned int Dimension = 2;
	typedef float ScalarPixelType;
	typedef unsigned char OutputPixelType;
typedef itk::Image< ScalarPixelType, Dimension > InternalImageType;
typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

	typedef itk::ScalarChanAndVeseLevelSetFunctionData< InternalImageType,
			InternalImageType > DataHelperType;

	typedef itk::ConstrainedRegionBasedLevelSetFunctionSharedData<
			InternalImageType, InternalImageType, DataHelperType > SharedDataHelperType;

	typedef itk::ScalarChanAndVeseLevelSetFunction< InternalImageType,
			InternalImageType, SharedDataHelperType > LevelSetFunctionType;


	//  We declare now the type of the numerically discretized Step and Delta functions that
	//  will be used in the level-set computations for foreground and background regions
	//
	typedef itk::AtanRegularizedHeavisideStepFunction< ScalarPixelType,
			ScalarPixelType >  DomainFunctionType;

	DomainFunctionType::Pointer domainFunction = DomainFunctionType::New();
	domainFunction->SetEpsilon( epsilon );

	// We instantiate reader and writer types in the following lines.
	//
	typedef itk::ImageFileReader< InternalImageType > ReaderType;
	typedef itk::ImageFileWriter< InternalImageType > WriterType;

	ReaderType::Pointer reader = ReaderType::New();

	reader->SetFileName( inputImage );
	reader->Update();



	InternalImageType::Pointer featureImage = reader->GetOutput();

	//  We declare now the type of the FastMarchingImageFilter that
	//  will be used to generate the initial level set in the form of a distance
	//  map.
	//
	typedef  itk::FastMarchingImageFilter< InternalImageType, InternalImageType >
	FastMarchingFilterType;



	//  We declare now the type of the ScalarChanAndVeseSparseLevelSetImageFilter that
	//  will be used to generate a segmentation.
	//

	typedef itk::ScalarChanAndVeseSparseLevelSetImageFilter< InternalImageType,
			InternalImageType, InternalImageType, LevelSetFunctionType,
			SharedDataHelperType > MultiLevelSetType;

	MultiLevelSetType::Pointer levelSetFilter = MultiLevelSetType::New();

	typedef CommandIterationUpdate<MultiLevelSetType> CommandType;
	CommandType::Pointer observer = CommandType::New();
	observer->setVerbose(verbose);
	levelSetFilter->AddObserver( itk::IterationEvent(), observer );


	//  We set the function count to 1 since a single level-set is being evolved.
	//
	levelSetFilter->SetFunctionCount( nb_phases );

	//  Set the feature image and initial level-set image as output of the
	//  fast marching image filter.
	//

	itk::NumericSeriesFileNames::Pointer seedFilenames=itk::NumericSeriesFileNames::New();
	seedFilenames->SetSeriesFormat(seedFile);
	seedFilenames->SetStartIndex(0);
	seedFilenames->SetEndIndex(nb_phases-1);
	vector<string> seedFnames=seedFilenames->GetFileNames();
	levelSetFilter->SetFeatureImage( featureImage );

	vector<FastMarchingFilterType::Pointer> fm_vector;

	for ( unsigned int i = 0; i < nb_phases; i++ )
	{
		FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();
		fm_vector.push_back(fastMarching);
		NodeContainer::Pointer seeds=read_seeds(seedFnames[i], -initialDistance);
		cout<<"seeed: "<<i<<" nb: "<< seeds->size()<<endl;
		fastMarching->SetTrialPoints(  seeds  );
		fastMarching->SetSpeedConstant( 1.0 );
		fastMarching->SetOutputSize(featureImage->GetBufferedRegion().GetSize() );
		fastMarching->Update();
		levelSetFilter->SetLevelSet( i, fastMarching->GetOutput() );
	}


	itk::NumericSeriesFileNames::Pointer filenamesDist=itk::NumericSeriesFileNames::New();
	filenamesDist->SetSeriesFormat("dist_%02d.mha");
	filenamesDist->SetStartIndex(0);
	filenamesDist->SetEndIndex(nb_phases-1);
		vector<string> fnamesDist=filenamesDist->GetFileNames();

		for ( unsigned int i = 0; i < nb_phases; i++ )
		{
			FastMarchingFilterType::Pointer  fastMarching=fm_vector[i];
			std::cout<<"writing distance:"<<fnamesDist[i]<<std::endl;
			WriterType::Pointer writer = WriterType::New();
			writer->SetFileName( fnamesDist[i] );
			writer->SetInput( fastMarching->GetOutput() );
			//writer->SetInput( levelSetFilter->GetLevelSet(i) );
			try
			{
				writer->Update();
			}
			catch( itk::ExceptionObject & excep )
			{
				std::cerr << "Exception caught !" << std::endl;
				std::cerr << excep << std::endl;
				return ;
			}
		}



	//  Once activiated the level set evolution will stop if the convergence
	//  criteria or if the maximum number of iterations is reached.  The
	//  convergence criteria is defined in terms of the root mean squared (RMS)
	//  change in the level set function. The evolution is said to have
	//  converged if the RMS change is below a user specified threshold.  In a
	//  real application is desirable to couple the evolution of the zero set
	//  to a visualization module allowing t/he user to follow the evolution of
	//  the zero set. With this feedback, the user may decide when to stop the
	//  algorithm before the zero set leaks through the regions of low gradient
	//  in the contour of the anatomical structure to be segmented.
	//
	levelSetFilter->SetNumberOfIterations( max_iter );
	levelSetFilter->SetMaximumRMSError( max_rms );

	//  Often, in real applications, images have different pixel resolutions. In such
	//  cases, it is best to use the native spacings to compute derivatives etc rather
	//  than sampling the images.
	//
	levelSetFilter->SetUseImageSpacing( 1 );

	//  For large images, we may want to compute the level-set over the initial supplied
	//  level-set image. This saves a lot of memory.
	//
	levelSetFilter->SetInPlace( false );

	//  For the level set with phase 0, set different parameters and weights. This may
	//  to be set in a loop for the case of multiple level-sets evolving simultaneously.
	//

	  for ( unsigned int i = 0; i < nb_phases; i++ )
	  {
	levelSetFilter->GetDifferenceFunction(i)->SetDomainFunction( domainFunction );
	levelSetFilter->GetDifferenceFunction(i)->SetCurvatureWeight( curvature_weight );
	levelSetFilter->GetDifferenceFunction(i)->SetAreaWeight( area_weight );
	levelSetFilter->GetDifferenceFunction(i)->SetReinitializationSmoothingWeight( reinitialization_weight );
	levelSetFilter->GetDifferenceFunction(i)->SetVolumeMatchingWeight( volume_weight );
	levelSetFilter->GetDifferenceFunction(i)->SetVolume( volume );
	levelSetFilter->GetDifferenceFunction(i)->SetLambda1( l1 );
	levelSetFilter->GetDifferenceFunction(i)->SetLambda2( l2 );
	levelSetFilter->GetDifferenceFunction(i)->SetOverlapPenaltyWeight(4000);

	  }
	levelSetFilter->Update();



	itk::NumericSeriesFileNames::Pointer filenamesOut=itk::NumericSeriesFileNames::New();
	filenamesOut->SetSeriesFormat(outputImage);
	filenamesOut->SetStartIndex(0);
	filenamesOut->SetEndIndex(nb_phases-1);
	vector<string> fnamesOut=filenamesOut->GetFileNames();

	for ( unsigned int i = 0; i < nb_phases; i++ )
	{
		std::cout<<"writing ls:"<<fnamesOut[i]<<std::endl;
		WriterType::Pointer writer = WriterType::New();
		writer->SetFileName( fnamesOut[i] );
		//writer->SetInput( levelSetFilter->GetOutput(i) );
		writer->SetInput( levelSetFilter->GetLevelSet(i) );
		try
		{
			writer->Update();
		}
		catch( itk::ExceptionObject & excep )
		{
			std::cerr << "Exception caught !" << std::endl;
			std::cerr << excep << std::endl;
			return ;
		}
	}

	WriterType::Pointer writer = WriterType::New();
			writer->SetFileName( "out.mha" );
			writer->SetInput( levelSetFilter->GetOutput() );
			try
			{
				writer->Update();
			}catch( itk::ExceptionObject & excep )
			{
				std::cerr << "Exception caught !" << std::endl;
				std::cerr << excep << std::endl;
				return ;
			}


	return ;
}
