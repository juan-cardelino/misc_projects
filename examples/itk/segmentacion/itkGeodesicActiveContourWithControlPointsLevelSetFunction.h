/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
#ifndef __itkGeodesicActiveContourWithControlPointsLevelSetFunction_h
#define __itkGeodesicActiveContourWithControlPointsLevelSetFunction_h

#include "itkGeodesicActiveContourLevelSetFunction.h"

namespace itk
{
/** \class GeodesicActiveContourLevelSetFunction
 *
 * \brief This function is used in GeodesicActiveContourLevelSetImageFilter to
 * segment structures in an image based on a user supplied edge potential map.
 *
 * \par IMPORTANT
 * The LevelSetFunction class contain additional information necessary
 * to gain full understanding of how to use this function.
 *
 * GeodesicActiveContourLevelSetFunction is a subclass of the generic LevelSetFunction.
 * It is used to segment structures in an image based on a user supplied
 * edge potential map \f$ g(I) \f$, which
 * has values close to zero in regions near edges (or high image gradient) and values
 * close to one in regions with relatively constant intensity. Typically, the edge
 * potential map is a function of the gradient, for example:
 *
 * \f[ g(I) = 1 / ( 1 + | (\nabla * G)(I)| ) \f]
 * \f[ g(I) = \exp^{-|(\nabla * G)(I)|} \f]
 *
 * where \f$ I \f$ is image intensity and
 * \f$ (\nabla * G) \f$ is the derivative of Gaussian operator.
 *
 * The edge potential image is set via the SetFeatureImage() method.
 *
 * In this function both the propagation term \f$ P(\mathbf{x}) \f$
 * and the curvature spatial modifier term \f$ Z(\mathbf{x}) \f$ are taken directly
 * from the edge potential image such that:
 *
 * \f[ P(\mathbf{x}) = g(\mathbf{x}) \f]
 * \f[ Z(\mathbf{x}) = g(\mathbf{x}) \f]
 *
 * An advection term \f$ \mathbf{A}(\mathbf{x}) \f$ is constructed
 * from the negative gradient of the edge potential image.
 *
 * \f[ \mathbf{A}(\mathbf{x}) = -\nabla g(\mathbf{x}) \f]
 *
 * This term behaves like a doublet attracting the contour to the edges.
 *
 * This implementation is based on:
 * "Geodesic Active Contours",
 * V. Caselles, R. Kimmel and G. Sapiro.
 * International Journal on Computer Vision,
 * Vol 22, No. 1, pp 61-97, 1997
 *
 * \sa LevelSetFunction
 * \sa SegmentationLevelSetImageFunction
 * \sa GeodesicActiveContourLevelSetImageFilter
 *
 * \ingroup FiniteDifferenceFunctions
 */
template< class TImageType, class TFeatureImageType = TImageType >
class ITK_EXPORT GeodesicActiveContourWithControlPointsLevelSetFunction:
public GeodesicActiveContourLevelSetFunction< TImageType, TFeatureImageType >
{
public:
	/** Standard class typedefs. */
	typedef GeodesicActiveContourWithControlPointsLevelSetFunction Self;
	typedef GeodesicActiveContourLevelSetFunction< TImageType, TFeatureImageType >
	Superclass;
	typedef SmartPointer< Self >       Pointer;
	typedef SmartPointer< const Self > ConstPointer;
	typedef TFeatureImageType          FeatureImageType;

	/** Method for creation through the object factory. */
	itkNewMacro(Self);

	/** Run-time type information (and related methods) */
	itkTypeMacro(GeodesicActiveContourWithControlPointsLevelSetFunction, GeodesicActiveContourLevelSetFunction);

	/** Extract some parameters from the superclass. */
	typedef typename Superclass::ImageType         ImageType;
	typedef typename Superclass::NeighborhoodType  NeighborhoodType;
	typedef typename Superclass::ScalarValueType   ScalarValueType;
	typedef typename Superclass::FeatureScalarType FeatureScalarType;
	typedef typename Superclass::RadiusType        RadiusType;
	typedef typename Superclass::FloatOffsetType   FloatOffsetType;
	typedef typename Superclass::VectorImageType   VectorImageType;
	typedef typename Superclass::GlobalDataStruct  GlobalDataStruct;

	/** Extract some parameters from the superclass. */
	itkStaticConstMacro(ImageDimension, unsigned int,
			Superclass::ImageDimension);


	/** Compute the advection field from feature image. */
	virtual void CalculateAdvectionImage();


	/** Set the distance image. JUANC */
	virtual const FeatureImageType * GetDistanceImage() const
	{ return m_DistanceImage.GetPointer(); }
	virtual void SetDistanceImage(const FeatureImageType *d)
	{    m_DistanceImage = d;  }

	void SetControlPointsWeight(const double v)
	{ m_weight = v; }
	double GetControlPointsWeight()
	{ return m_weight; }

	void SetControlPointsRadius(const double v)
	{ m_radius = v; }
	double GetControlPointsRadius()
	{ return m_radius; }

	void SetVerbose(const int v)
	{ m_verbose = v; }
	int GetVerbose()
	{ return m_verbose; }

	virtual void Initialize(const RadiusType & r)
	{
		Superclass::Initialize(r);

		this->SetControlPointsWeight(NumericTraits< ScalarValueType >::One);
		this->SetControlPointsRadius(10.0);
	}

protected:
	/** distance image. JUANC */
	typename FeatureImageType::ConstPointer m_DistanceImage;
	GeodesicActiveContourWithControlPointsLevelSetFunction()
	{

		this->SetAdvectionWeight(NumericTraits< ScalarValueType >::One);
		this->SetPropagationWeight(NumericTraits< ScalarValueType >::One);
		this->SetCurvatureWeight(NumericTraits< ScalarValueType >::One);

		this->SetDerivativeSigma(1.0);
		m_weight=0.1;	//JUANC
		m_verbose=0;
	}

	virtual ~GeodesicActiveContourWithControlPointsLevelSetFunction() {}

	GeodesicActiveContourWithControlPointsLevelSetFunction(const Self &); //purposely not
	// implemented
	void operator=(const Self &);                        //purposely not
	// implemented

	void PrintSelf(std::ostream & os, Indent indent) const
	{
		Superclass::PrintSelf(os, indent);
		os << indent << "ControlPointWeight: " << m_weight << std::endl;
	}

private:
	double m_weight;	//JUANC
	double m_radius;
	int m_verbose;


};
} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkGeodesicActiveContourWithControlPointsLevelSetFunction.txx"
#endif

#endif
