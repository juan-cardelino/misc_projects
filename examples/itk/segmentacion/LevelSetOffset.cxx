#include "util/util.h"
#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)


#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkShiftScaleImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkSignedDanielssonDistanceMapImageFilter.h"
#include "itkZeroCrossingImageFilter.h"

  typedef   float           InternalPixelType;
  const     unsigned int    Dimension = 2;
  typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;
  typedef itk::ImageFileWriter< InternalImageType > InternalWriterType;
  typedef itk::MinimumMaximumImageCalculator< InternalImageType > MaxType;

class GACDemo: public util::Configurable
{

public:
	std::string inputImage;
	std::string outputImage;
	double initialDistance;

	GACDemo()
	{
	}

	  void configure(const util::Configuration& conf)
	    throw (util::ConfException)
	  {
		  inputImage = conf.getValue("inputImage");
		  outputImage = conf.getValue("outputImage");
		  initialDistance=conf.getFloatValue("initialDistance");;
	  }

	util::Configuration getConfiguration() const
	{
		util::Configuration miConf("base","Configuracion base");
		miConf.addOption("inputImage","","in.png");
		miConf.addOption("outputImage","","out.png");
		miConf.addOption("initialDistance","","1");
		return miConf;
	}

	void run();
};




int main( int argc, const char *argv[] )
{
	XML_IN;
	util::Logger::setLogLevel(util::Logger::INFO);
	//	cout << 	setprecision(5);
	util::infolog.logln("Starting algorithm");
	GACDemo demo;


	try {
		//
		// get core configuration parameters
		//
		util::Configuration cfg = demo.getConfiguration();
		//
		// create a configuration manager to read and write the configuration
		//
		util::ConfManager conf_manager(cfg);
		//
		// read config. from command line
		//
		cfg = conf_manager.parseConf(argc,argv);
		//
		// configure the algorithm
		//
		demo.configure(cfg);

		//and finally run the algorithm
		demo.run();

	} catch (base::Exception ex) {
				std::cout << "EPA! " << ex.getMessage() << std::endl;
	}

	XML_OUT;
	  return 0;
}

using namespace std;


void GACDemo::run()
{
	XML_IN;

  //
  typedef unsigned char                            OutputPixelType;
  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

  // We instantiate reader and writer types in the following lines.
  //
  typedef  itk::ImageFileReader< InternalImageType > ReaderType;
  typedef  itk::ImageFileWriter<  InternalImageType  > WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();


  cout<<"reader filename: "<<inputImage <<endl;
  reader->SetFileName( inputImage );
  writer->SetFileName( outputImage);


  //  We declare now the type of the FastMarchingImageFilter that
  //  will be used to generate the initial level set in the form of a distance
  //  map.
  //
  typedef  itk::FastMarchingImageFilter<
                              InternalImageType,
                              InternalImageType >    FastMarchingFilterType;


  //  Next we construct one filter of this class using the \code{New()}
  //  method.
  //
  FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();


  typedef itk::ZeroCrossingImageFilter<InternalImageType,InternalImageType> ZcrFilterType;

  	ZcrFilterType::Pointer zcr = ZcrFilterType::New();

  	zcr->SetForegroundValue(255);
  	zcr->SetBackgroundValue(0);

   typedef itk::ShiftScaleImageFilter<InternalImageType,InternalImageType> ScalerType;
   ScalerType::Pointer scaler=ScalerType::New();

   scaler->SetScale( 1.0 );
   scaler->SetShift( initialDistance );


   typedef itk::BinaryThresholdImageFilter<
                           InternalImageType,
                           InternalImageType    >       ThresholdingFilterType;

     ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

     thresholder->SetLowerThreshold( -1000.0 );
     thresholder->SetUpperThreshold(     0.0 );

     thresholder->SetOutsideValue(  0  );
     thresholder->SetInsideValue(  255 );

     typedef itk::SignedDanielssonDistanceMapImageFilter<
   		  InternalImageType,
   		  InternalImageType >  DistanceFilterType;

     DistanceFilterType::Pointer danielsson = DistanceFilterType::New();

	 thresholder->SetInput(reader->GetOutput());
	 danielsson->SetInput(thresholder->GetOutput());
	 scaler->SetInput(danielsson->GetOutput());
	 writer->SetInput(scaler->GetOutput());

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  
  XML_OUT;
}



