/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
#ifndef __itkGeodesicActiveContourWithControlPointsLevelSetFunction_txx
#define __itkGeodesicActiveContourWithControlPointsLevelSetFunction_txx

#include "itkGeodesicActiveContourWithControlPointsLevelSetFunction.h"
#include "itkImageRegionIterator.h"
#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkGradientImageFilter.h"
#include "itkVectorCastImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkImageFileWriter.h"

namespace itk
{

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)

template< class TImageType, class TFeatureImageType >
void GeodesicActiveContourWithControlPointsLevelSetFunction< TImageType, TFeatureImageType >
::CalculateAdvectionImage()
 {
	XML_IN;
	/* compute the gradient of the feature image. */

	typename VectorImageType::Pointer gradientImage;

	if ( this->GetControlPointsWeight() == NumericTraits< float >::Zero )
	{
		Superclass::CalculateAdvectionImage();
		XML_OUT;
		return;
	}

	if ( this->GetDerivativeSigma() != NumericTraits< float >::Zero )
	{
		typedef GradientRecursiveGaussianImageFilter< FeatureImageType, VectorImageType >
		DerivativeFilterType;

		typename DerivativeFilterType::Pointer derivative = DerivativeFilterType::New();
		derivative->SetInput( this->GetFeatureImage() );
		derivative->SetSigma(this->GetDerivativeSigma());
		derivative->Update();

		gradientImage = derivative->GetOutput();
	}
	else
	{
		typedef GradientImageFilter< FeatureImageType > DerivativeFilterType;

		typename DerivativeFilterType::Pointer derivative = DerivativeFilterType::New();
		derivative->SetInput( this->GetFeatureImage() );
		derivative->SetUseImageSpacingOn();
		derivative->Update();

		typedef typename DerivativeFilterType::OutputImageType                      DerivativeOutputImageType;
		typedef VectorCastImageFilter< DerivativeOutputImageType, VectorImageType > GradientCasterType;

		typename GradientCasterType::Pointer caster = GradientCasterType::New();
		caster->SetInput( derivative->GetOutput() );
		caster->Update();

		gradientImage = caster->GetOutput();
	}


	//compute the derivative of the distance map
	typedef GradientRecursiveGaussianImageFilter< FeatureImageType, VectorImageType >
	DerivativeFilterType;
	typename VectorImageType::Pointer distGradientImage;
	typename DerivativeFilterType::Pointer distDerivative = DerivativeFilterType::New();
	distDerivative->SetInput( this->GetDistanceImage() );
	distDerivative->SetSigma(this->GetDerivativeSigma());
	distDerivative->Update();
	distGradientImage = distDerivative->GetOutput();
	ImageRegionIterator< VectorImageType >
	dgit( distGradientImage, distGradientImage->GetRequestedRegion() );


if(m_verbose>=2)
{
	//this is for debbugging the distance function only
	typedef itk::MinimumMaximumImageCalculator< FeatureImageType > MaxType;
	typename MaxType::Pointer max=MaxType::New();
	max->SetImage(this->GetDistanceImage());
	max->ComputeMaximum();
	max->ComputeMinimum();
	std::cout<<"max: " << max->GetMaximum()<< " min: "<<max->GetMinimum()<<std::endl;

	typedef itk::ImageFileWriter< FeatureImageType > InternalWriterType;
	typename InternalWriterType::Pointer writer = InternalWriterType::New();
	writer->SetInput(this->GetDistanceImage());
	writer->SetFileName("debug/dist.mha");	//FIXME: this should go away soon
	writer->Update();
}

	/* copy negative gradient into the advection image. */
	ImageRegionIterator< VectorImageType >
	dit( gradientImage, this->GetFeatureImage()->GetRequestedRegion() );
	ImageRegionConstIterator< FeatureImageType >
	ddit( this->GetDistanceImage(), this->GetDistanceImage()->GetRequestedRegion() );
	//iterate over the advection image
	ImageRegionIterator< VectorImageType >
	ait( this->GetAdvectionImage(), this->GetFeatureImage()->GetRequestedRegion() );

	//float d_th=50;
	for ( dgit.GoToBegin(), dit.GoToBegin(), ddit.GoToBegin(),ait.GoToBegin(); !dit.IsAtEnd(); ++dgit, ++dit, ++ait,++ddit )
	{
		typename VectorImageType::PixelType v = dit.Get();
		typename FeatureImageType::PixelType d = ddit.Get();
		typename VectorImageType::PixelType dv = dgit.Get();//JUANC
		typename FeatureImageType::IndexType idx=dgit.GetIndex();

		for ( unsigned int j = 0; j < ImageDimension; j++ )
		{
			if(d<m_radius)
			{
				v[j] *= -1.0L * (1-m_weight);
				v[j]-= m_weight*dv[j];	//JUANC
			}else
			{
				v[j] *= -1.0L;
			}
			bool cond=fabs(dv[j]) >1.1;
			if(cond)
			{

				std::cout<<"idx: " << idx<<std::endl;
				std::cout<<"dv: " << dv<<std::endl;
			}
			assert(!cond);
			assert(fabs(dv[j]) >=0);
		}
		ait.Set(v);
	}
	XML_OUT;
 }
} // end namespace itk

#endif
