#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)

#include "util/util.h"
#include "itkLevelSetSegmentationCommon.h"



class GACDemo: public util::Configurable
{

public:
	std::string inputImage1;
	std::string inputImage2;
	std::string outputImage;
	int operation;

	GACDemo()
	{
	}

	  void configure(const util::Configuration& conf)
	    throw (util::ConfException)
	  {
		  inputImage1 = conf.getValue("inputImage1");
		  inputImage2 = conf.getValue("inputImage2");
		  outputImage = conf.getValue("outputImage");
		  operation=conf.getIntValue("operation");;
	  }

	util::Configuration getConfiguration() const
	{
		util::Configuration miConf("base","Configuracion base");
		miConf.addOption("inputImage1","","in1.png");
		miConf.addOption("inputImage2","","in2.png");
		miConf.addOption("outputImage","","out.png");
		miConf.addOption("operation","","1");
		return miConf;
	}

	void run();
};




int main( int argc, const char *argv[] )
{
	XML_IN;
	util::Logger::setLogLevel(util::Logger::INFO);
	//	cout << 	setprecision(5);
	util::infolog.logln("Starting algorithm");
	GACDemo demo;


	try {
		//
		// get core configuration parameters
		//
		util::Configuration cfg = demo.getConfiguration();
		//
		// create a configuration manager to read and write the configuration
		//
		util::ConfManager conf_manager(cfg);
		//
		// read config. from command line
		//
		cfg = conf_manager.parseConf(argc,argv);
		//
		// configure the algorithm
		//
		demo.configure(cfg);

		//and finally run the algorithm
		demo.run();

	} catch (base::Exception ex) {
				std::cout << "EPA! " << ex.getMessage() << std::endl;
	}

	XML_OUT;
	  return 0;
}

using namespace std;


void GACDemo::run()
{
	XML_IN;

  
  // We instantiate reader and writer types in the following lines.
  //
  typedef  itk::ImageFileReader< InternalImageType > ReaderType;
  typedef  itk::ImageFileWriter<  InternalImageType  > WriterType;

  ReaderType::Pointer reader1 = ReaderType::New();
  ReaderType::Pointer reader2 = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();


  cout<<"image1 : "<<inputImage1 <<endl;
  cout<<"image2 : "<<inputImage2 <<endl;
  reader1->SetFileName( inputImage1 );
  reader2->SetFileName( inputImage2 );


  writer->SetFileName( outputImage);

  reader1->Update();
  reader2->Update();
  InternalImageType::Pointer out;
  switch(operation)
  {
  case 1:
  	out=levelset_union(reader1->GetOutput(),reader2->GetOutput());
  	break;
  case 2:
	  out=levelset_intersection(reader1->GetOutput(),reader2->GetOutput());
	  break;
  }
  writer->SetInput(out);

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  
  XML_OUT;
}




