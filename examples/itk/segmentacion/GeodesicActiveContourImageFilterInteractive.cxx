#include "GeodesicActiveContourImageFilterInteractive.h"
#include "itkVector.h"

#ifdef GAC_USE_LOG
#include "util/Logger.h"
#endif

GACDemo::GACDemo()
{
	max_iter=4000;
	use_initial_level_set=false;
	use_tracking=false;
	step=-1;
	use_fixed_level_set=false;
	use_offset=false;
	max_rms=0.02;
	ind_start=1;
	ind_end=2;
	control_points_weight=0;
	magnet_type=0;
	curvatureScaling=1;
	propagationScaling=1;
	verbose=0;
	magnet_radius=10.0;
	seed_type=0;
	alpha=1;
	beta=1;
	sigma=1;
	use_library_mode=true;
	outputImage="out";

	#ifdef GAC_USE_LOG
	  util::Logger::setLogLevel(util::Logger::INFO);
	  //std::cout << std::setprecision(5);
	  util::Logger::setLogFile("log.txt");
	  util::infolog.logln("Example configuration");
	#endif
}

#ifdef GAC_USE_CONF
void GACDemo::configure(const util::Configuration& conf)
throw (util::ConfException)
{
	max_iter = conf.getIntValue("max_iter");
	inputImage = conf.getValue("inputImage");
	outputImage = conf.getValue("outputImage");
	initialLevelSet = conf.getValue("initialLevelSet");
	userDistanceFilename = conf.getValue("userDistanceFilename");
	propagationScaling = conf.getFloatValue("propagationScaling");
	curvatureScaling = conf.getFloatValue("curvatureScaling");
	seedFile = conf.getValue("seedFile");
	initialDistance=conf.getFloatValue("initialDistance");;
	sigma=conf.getFloatValue("sigma");
	alpha=conf.getFloatValue("alpha");
	beta=conf.getFloatValue("beta");
	use_initial_level_set = conf.getBoolValue("use_initial_level_set");
	use_tracking = conf.getBoolValue("use_tracking");
	use_fixed_level_set = conf.getBoolValue("use_fixed_level_set");
	max_rms = conf.getFloatValue("max_rms");
	use_offset = conf.getBoolValue("use_offset");
	ind_start = conf.getIntValue("ind_start");
	ind_end = conf.getIntValue("ind_end");
	control_points_weight=conf.getFloatValue("control_points_weight");
	magnet_type=conf.getIntValue("magnet_type");
	magnet_radius=conf.getFloatValue("magnet_radius");
	verbose=conf.getIntValue("verbose");
	seed_type=conf.getIntValue("seed_type");
}

util::Configuration GACDemo::getConfiguration() const
{
	util::Configuration miConf("base","Configuracion base");
	miConf.addOption("max_iter","","500");
	miConf.addOption("use_initial_level_set","","false");
	miConf.addOption("inputImage","","in.png");
	miConf.addOption("outputImage","","out.png");
	miConf.addOption("initialLevelSet","","phi.mha");
	miConf.addOption("userDistanceFilename","","dist.mha");
	miConf.addOption("propagationScaling","","1");
	miConf.addOption("curvatureScaling","","1");
	miConf.addOption("seedFile","seed points (text file)","seeds.xml");
	miConf.addOption("initialDistance","","5");
	miConf.addOption("sigma","","1");
	miConf.addOption("alpha","","1");
	miConf.addOption("beta","","1");
	miConf.addOption("use_initial_level_set","","false");
	miConf.addOption("use_tracking","","false");
	miConf.addOption("use_fixed_level_set","","false");
	miConf.addOption("max_rms","","0.02");
	miConf.addOption("use_offset","","false");
	miConf.addOption("ind_start","","1");
	miConf.addOption("ind_end","","2");
	miConf.addOption("control_points_weight","","0.0");
	miConf.addOption("magnet_type","","0");
	miConf.addOption("verbose","","0");
	miConf.addOption("magnet_radius","","10");
	miConf.addOption("seed_type","","0");

	return miConf;
}
#endif //GAC_USE_CONF


using namespace std;

void GACDemo::run()
{
	if (use_tracking)
		tracking();
	else
	{
		inputImageFilename=inputImage;
		outputImageFilename=outputImage;
		initialLevelSetFilename=initialLevelSet;
		seedFilename=seedFile;
		segment();
	}
}

void GACDemo::tracking()
{


	itk::NumericSeriesFileNames::Pointer filenames=itk::NumericSeriesFileNames::New	();
	filenames->SetSeriesFormat(inputImage);
	filenames->SetStartIndex(ind_start);
	filenames->SetEndIndex(ind_end);
	vector<string> fnames=filenames->GetFileNames();

	itk::NumericSeriesFileNames::Pointer filenamesOut=itk::NumericSeriesFileNames::New();
	filenamesOut->SetSeriesFormat(outputImage);
	filenamesOut->SetStartIndex(ind_start);
	filenamesOut->SetEndIndex(ind_end);
	vector<string> fnamesOut=filenamesOut->GetFileNames();

	itk::NumericSeriesFileNames::Pointer seedFilenames=itk::NumericSeriesFileNames::New();
	seedFilenames->SetSeriesFormat(seedFile);
	seedFilenames->SetStartIndex(ind_start);
	seedFilenames->SetEndIndex(ind_end);
	vector<string> seedFnames=seedFilenames->GetFileNames();

	for(unsigned int i=0;i<fnames.size();i++)
	{
		step=i;
		inputImageFilename=fnames[i]+".png";
		outputImageFilename=fnamesOut[i];
		seedFilename=seedFnames[i];
		if(i==0 || use_fixed_level_set)
			initialLevelSetFilename=initialLevelSet;
		else if (use_offset)
		{
			initialLevelSetFilename=fnamesOut[i-1]+"_offset.mha";
			offset(fnamesOut[i-1]+".mha",initialLevelSetFilename, initialDistance);
		}else
			initialLevelSetFilename=fnamesOut[i-1]+".mha";

		cout<<"i: "<<i<<" fname: "<<fnames[i]<<endl;
		cout<<"i: "<<i<<" fname: "<<fnamesOut[i]<<endl;
		cout<<"i: "<<i<<" seeds: "<<seedFnames[i]<<endl;
		segment();

	}

}

typedef  itk::GeodesicActiveContourLevelSetImageFilter2< InternalImageType,
		InternalImageType >    GeodesicActiveContourFilterType;

void GACDemo::segment()
{
	XML_IN;

	ReaderType::Pointer reader = ReaderType::New();

	cout<<"reader filename: "<<inputImageFilename <<endl;
	reader->SetFileName( inputImageFilename );
	reader->Update();

	GeodesicActiveContourFilterType::Pointer geodesicActiveContour =
			GeodesicActiveContourFilterType::New();

	InternalImageType::Pointer im_orig;
	im_orig=reader->GetOutput();

	MaxType::Pointer max=MaxType::New();
	max->SetImage(im_orig);
	max->ComputeMaximum();
	max->ComputeMinimum();
	cout<<"max: " << max->GetMaximum()<< " min: "<<max->GetMinimum()<<endl;

	InternalImageType::Pointer imMagnet;
	imMagnet=configure_magnet(im_orig, NULL);
	geodesicActiveContour->SetDistanceImage(imMagnet);

	InternalImageType::Pointer im_feat;
	im_feat=compute_features( im_orig );
	geodesicActiveContour->SetFeatureImage( im_feat );

	InternalImageType::RegionType region=im_orig->GetLargestPossibleRegion();
	InternalImageType::SizeType size=region.GetSize();

	InternalImageType::Pointer im_ini;
	im_ini=initialize(NULL, size, NULL);
	geodesicActiveContour->SetInput( im_ini );


	geodesicActiveContour->SetPropagationScaling( propagationScaling );
	geodesicActiveContour->SetCurvatureScaling( curvatureScaling);
	geodesicActiveContour->SetAdvectionScaling( 1.0 );
	geodesicActiveContour->SetControlPointsWeight(control_points_weight);
	geodesicActiveContour->SetMaximumRMSError( max_rms );
	geodesicActiveContour->SetNumberOfIterations( max_iter );


	typedef CommandIterationUpdate<GeodesicActiveContourFilterType> CommandType;
	CommandType::Pointer observer = CommandType::New();
	geodesicActiveContour->AddObserver( itk::IterationEvent(), observer );
	observer->setVerbose(verbose);

	geodesicActiveContour->Update();

	if(verbose>=2) save_output(im_orig,geodesicActiveContour->GetOutput());

	// Print out some useful information
	std::cout << std::endl;
	std::cout << "Max. no. iterations: " << geodesicActiveContour->GetNumberOfIterations() << std::endl;
	std::cout << "Max. RMS error: " << geodesicActiveContour->GetMaximumRMSError() << std::endl;
	std::cout << std::endl;
	std::cout << "No. elapsed iterations: " << geodesicActiveContour->GetElapsedIterations() << std::endl;
	std::cout << "RMS change: " << geodesicActiveContour->GetRMSChange() << std::endl;
	XML_OUT;
}

InternalImageType::Pointer  GACDemo::configure_magnet(InternalImageType::Pointer im_orig, NodeContainer::Pointer magnets)
{
	XML_IN;

	ReaderType::Pointer readerUserDist = ReaderType::New();
	cout<<"reading user points: "<< userDistanceFilename<<endl;
	readerUserDist->SetFileName(userDistanceFilename);

	FastMarchingFilterType::Pointer  fastMarchingMagnet = FastMarchingFilterType::New();

	InternalImageType::Pointer imMagnet;
	NodeContainer::Pointer seedsMagnet;
	switch(magnet_type)
	{
	case 0:
		imMagnet=NULL;
		control_points_weight=0;	//FIXME: this is redundant but needed
		break;
	case 1:
		readerUserDist->Update();
		imMagnet=readerUserDist->GetOutput();
		break;
	case 2:
		seedsMagnet = read_seeds(userDistanceFilename,0);
		fastMarchingMagnet->SetTrialPoints(  seedsMagnet  );
		fastMarchingMagnet->SetSpeedConstant( 1.0 );
		fastMarchingMagnet->SetOutputSize(im_orig->GetBufferedRegion().GetSize() );
		imMagnet=fastMarchingMagnet->GetOutput();
		fastMarchingMagnet->Update();
		break;
	case 3:
		fastMarchingMagnet->SetTrialPoints(  magnets  );
		fastMarchingMagnet->SetSpeedConstant( 1.0 );
		fastMarchingMagnet->SetOutputSize(im_orig->GetBufferedRegion().GetSize() );
		imMagnet=fastMarchingMagnet->GetOutput();
		fastMarchingMagnet->Update();
		break;
	}
	XML_OUT;
	return imMagnet;
}

InternalImageType::Pointer GACDemo::initialize(InternalImageType::Pointer im_dist, InternalImageType::SizeType size, NodeContainer::Pointer ini_seeds)
{
	XML_IN;

	FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();

	InternalImageType::Pointer im_ini;

	if(use_initial_level_set && use_library_mode && !seed_type)
		{
		cout<<"ini mode 6"<<endl;
		return im_dist;
		}

	//seed initialization
	NodeContainer::Pointer seeds;

		switch(seed_type)
		{
		case 1:
			seeds=read_seeds(seedFilename,-initialDistance);
			break;
		case 2:
			seeds=ini_seeds;
			break;
		}


	fastMarching->SetTrialPoints(  seeds  );
	fastMarching->SetSpeedConstant( 1.0 );
	fastMarching->SetOutputSize( size );
	std::cout<<"size: "<<size<<std::endl;

	ReaderType::Pointer readerDistance=ReaderType::New();
	cout<<"readerDistance filename: "<< initialLevelSetFilename<<endl;
	readerDistance->SetFileName(initialLevelSetFilename);

	typedef itk::ShiftScaleImageFilter<InternalImageType,InternalImageType> ScalerType;
	ScalerType::Pointer scaler=ScalerType::New();
	scaler->SetScale( 1.0 );
	scaler->SetShift( 0 );

	if(use_initial_level_set && use_library_mode && seed_type)
	{
		cout<<"ini mode 7"<<endl;
		im_ini=fastMarching->GetOutput();
		fastMarching->Update();
		InternalImageType::Pointer im_union=levelset_intersection(im_dist,im_ini);
		return im_union;
	}


	if(use_initial_level_set)
	{
		if(use_tracking && step==0)
		{
			cout<<"ini mode 1"<<endl;
			scaler->SetInput( readerDistance->GetOutput() );
			//geodesicActiveContour->SetInput( scaler->GetOutput() );
			im_ini=scaler->GetOutput();
			scaler->Update();

		}else
		{
			cout<<"ini mode 2"<<endl;
			//geodesicActiveContour->SetInput(  readerDistance->GetOutput() );
			im_ini=readerDistance->GetOutput();
			readerDistance->Update();

		}
	}else
	{
		if(use_tracking && step==0)
		{
			cout<<"ini mode 3"<<endl;
			//geodesicActiveContour->SetInput( fastMarching->GetOutput() );
			im_ini=fastMarching->GetOutput();
			fastMarching->Update();

		}else if (!seed_type)
		{
			cout<<"ini mode 4"<<endl;
			scaler->SetInput( readerDistance->GetOutput());
			//geodesicActiveContour->SetInput( scaler->GetOutput() );
			scaler->Update();
			im_ini=scaler->GetOutput();

			InternalWriterType::Pointer mapWriter2 = InternalWriterType::New();
			mapWriter2->SetInput( scaler->GetOutput() );
			mapWriter2->SetFileName("debug/GeodesicActiveContourImageFilterOutput7.mha");
			mapWriter2->Update();
			MaxType::Pointer max=MaxType::New();
			max->SetImage(scaler->GetOutput());
			max->ComputeMaximum();
			max->ComputeMinimum();
			cout<<"max: " << max->GetMaximum()<< "min: "<<max->GetMinimum()<<endl;
		}else
		{
			cout<<"ini mode 5"<<endl;
			//geodesicActiveContour->SetInput( fastMarching->GetOutput() );
			im_ini=fastMarching->GetOutput();
			fastMarching->Update();

		}
		//geodesicActiveContour->SetInput(  readerDistance->GetOutput() );
	}

	if(verbose>=2) save_debug_initialization(im_ini);

	XML_OUT;
	return im_ini;
}


InternalImageType::Pointer  GACDemo::compute_features(InternalImageType::Pointer im_orig )
{
	XML_IN;

	//  The \doxygen{CurvatureAnisotropicDiffusionImageFilter} type is
	//  instantiated using the internal image type.
	//
	typedef   itk::CurvatureAnisotropicDiffusionImageFilter<
			InternalImageType,
			InternalImageType >  SmoothingFilterType;

	SmoothingFilterType::Pointer smoothing = SmoothingFilterType::New();


	//  The types of the
	//  GradientMagnitudeRecursiveGaussianImageFilter and
	//  SigmoidImageFilter are instantiated using the internal image
	//  type.
	//
	typedef   itk::GradientMagnitudeRecursiveGaussianImageFilter<
			InternalImageType,
			InternalImageType >  GradientFilterType;
	typedef   itk::SigmoidImageFilter<
			InternalImageType,
			InternalImageType >  SigmoidFilterType;

	GradientFilterType::Pointer  gradientMagnitude = GradientFilterType::New();

	SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();



	sigmoid->SetOutputMinimum(  0.0  );
	sigmoid->SetOutputMaximum(  1.0  );

	smoothing->SetInput( im_orig );
	gradientMagnitude->SetInput( smoothing->GetOutput() );
	sigmoid->SetInput( gradientMagnitude->GetOutput() );

	//  The CurvatureAnisotropicDiffusionImageFilter requires a couple of
	//  parameter to be defined. The following are typical values for $2D$
	//  images. However they may have to be adjusted depending on the amount of
	//  noise present in the input image. This filter has been discussed in
	//  section~\ref{sec:GradientAnisotropicDiffusionImageFilter}.

	smoothing->SetTimeStep( 0.125 );
	smoothing->SetNumberOfIterations(  5 );
	smoothing->SetConductanceParameter( 9.0 );


	//  The GradientMagnitudeRecursiveGaussianImageFilter performs the
	//  equivalent of a convolution with a Gaussian kernel, followed by a
	//  derivative operator. The sigma of this Gaussian can be used to control
	//  the range of influence of the image edges. This filter has been discussed
	//  in Section~\ref{sec:GradientMagnitudeRecursiveGaussianImageFilter}

	gradientMagnitude->SetSigma(  sigma  );


	//  The SigmoidImageFilter requires two parameters that define the linear
	//  transformation to be applied to the sigmoid argument. This parameters
	//  have been discussed in Sections~\ref{sec:IntensityNonLinearMapping} and
	//  \ref{sec:FastMarchingImageFilter}.

	sigmoid->SetAlpha( alpha );
	sigmoid->SetBeta(  beta  );


	//  The FastMarchingImageFilter requires the user to provide a seed
	//  point from which the level set will be generated. The user can actually
	//  pass not only one seed point but a set of them. Note the the
	//  FastMarchingImageFilter is used here only as a helper in the
	//  determination of an initial level set. We could have used the
	//  \doxygen{DanielssonDistanceMapImageFilter} in the same way.
	//
	//  The seeds are passed stored in a container. The type of this
	//  container is defined as \code{NodeContainer} among the
	//  FastMarchingImageFilter traits.
	//
	sigmoid->Update();
	if(verbose>=2) save_debug_results(smoothing->GetOutput(),gradientMagnitude->GetOutput(), sigmoid->GetOutput());

	XML_OUT;
	return sigmoid->GetOutput();
}

typedef  itk::ImageFileWriter<  OutputImageType  > WriterType;

void GACDemo::save_debug_initialization(InternalImageType::Pointer im_dist)
{
	XML_IN;

	InternalWriterType::Pointer mapWriter = InternalWriterType::New();
	mapWriter->SetInput( im_dist );
	mapWriter->SetFileName("debug/GeodesicActiveContourImageFilterOutput4.mha");
	mapWriter->Update();

	CastFilterType::Pointer caster4 = CastFilterType::New();
	caster4->SetInput(  im_dist);

	WriterType::Pointer writer4 = WriterType::New();
	writer4->SetInput( caster4->GetOutput() );
	writer4->SetFileName("debug/GeodesicActiveContourImageFilterOutput4.png");
	caster4->SetOutputMinimum(   0 );
	caster4->SetOutputMaximum( 255 );
	writer4->Update();

	XML_OUT;
}

void GACDemo::save_debug_results(InternalImageType::Pointer im_smooth, InternalImageType::Pointer im_grad, InternalImageType::Pointer im_sigmoid)
{
	XML_IN;

	CastFilterType::Pointer caster1 = CastFilterType::New();
	CastFilterType::Pointer caster2 = CastFilterType::New();
	CastFilterType::Pointer caster3 = CastFilterType::New();


	WriterType::Pointer writer1 = WriterType::New();
	WriterType::Pointer writer2 = WriterType::New();
	WriterType::Pointer writer3 = WriterType::New();


	caster1->SetInput( im_smooth );
	writer1->SetInput( caster1->GetOutput() );
	writer1->SetFileName("debug/GeodesicActiveContourImageFilterOutput1.png");
	caster1->SetOutputMinimum(   0 );
	caster1->SetOutputMaximum( 255 );
	writer1->Update();

	caster2->SetInput( im_grad );
	writer2->SetInput( caster2->GetOutput() );
	writer2->SetFileName("debug/GeodesicActiveContourImageFilterOutput2.png");
	caster2->SetOutputMinimum(   0 );
	caster2->SetOutputMaximum( 255 );
	writer2->Update();

	caster3->SetInput( im_sigmoid );
	writer3->SetInput( caster3->GetOutput() );
	writer3->SetFileName("debug/GeodesicActiveContourImageFilterOutput3.png");
	caster3->SetOutputMinimum(   0 );
	caster3->SetOutputMaximum( 255 );
	writer3->Update();


	InternalWriterType::Pointer speedWriter = InternalWriterType::New();
	speedWriter->SetInput( im_sigmoid );
	speedWriter->SetFileName("debug/GeodesicActiveContourImageFilterOutput3.mha");


	InternalWriterType::Pointer gradientWriter = InternalWriterType::New();
	gradientWriter->SetInput( im_grad );
	gradientWriter->SetFileName("debug/GeodesicActiveContourImageFilterOutput2.mha");

	try
	{
		speedWriter->Update();
		gradientWriter->Update();

	}
	catch( itk::ExceptionObject & excep )
	{
		std::cerr << "Exception caught !" << std::endl;
		std::cerr << excep << std::endl;
	}


	XML_OUT;
}

void GACDemo::save_output(InternalImageType::Pointer im_orig, InternalImageType::Pointer im_out)
{
	XML_IN;
	WriterType::Pointer writer6 = WriterType::New();
	typedef itk::ZeroCrossingImageFilter<InternalImageType, OutputImageType > ZcrFilterType;
	ZcrFilterType::Pointer zcr = ZcrFilterType::New();
	writer6->SetFileName(outputImageFilename+".png");
	zcr->SetForegroundValue(255);
	zcr->SetBackgroundValue(0);
	zcr->SetInput(im_out);
	writer6->SetInput(zcr->GetOutput());
	writer6->Update();

	overlay(im_orig,zcr->GetOutput(),outputImageFilename+"_overlay.png");

	InternalWriterType::Pointer writer7 = InternalWriterType::New();
	cout<<"writing: "<<outputImageFilename+".mha"<<endl;
	writer7->SetFileName(outputImageFilename+".mha");
	writer7->SetInput(im_out);
	writer7->Update();

	ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
	thresholder->SetLowerThreshold( -1000.0 );
	thresholder->SetUpperThreshold(     0.0 );
	thresholder->SetOutsideValue(  0  );
	thresholder->SetInsideValue(  255 );
	thresholder->SetInput( im_out );

	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName( outputImageFilename+".th.png" );
	writer->SetInput( thresholder->GetOutput() );
	writer->Update();

	XML_OUT;
}

void GACDemo::configure_use_case(int case_number)
{
	//opciones fijas
	this->setAlpha(-0.3);
	this->setBeta(2);
	this->setCurvatureScaling(0.2);

	//opciones de comportamiento
	this->setMax_iter(1500);
	this->setPropagationScaling(0.6);
	this->setMax_rms(0.02);

	//opciones de modo
	switch(case_number)
	{
	case 1:
		this->setMagnet_radius(10);
		this->setMagnet_type(3);
		this->setSeed_type(2);
		this->setControl_points_weight(1);
		break;
	case 2:
		this->setMagnet_radius(10);
		this->setMagnet_type(3);
		this->setSeed_type(0);
		this->setUse_initial_level_set(true);
		this->setControl_points_weight(1);
		break;
	case 3:
		this->setMagnet_type(0);
		this->setSeed_type(0);
		this->setUse_initial_level_set(true);
		this->setControl_points_weight(1);
		break;
	case 4:
		this->setMagnet_type(0);
		this->setSeed_type(2);
		this->setUse_initial_level_set(true);
		this->setControl_points_weight(1);
		break;
	case 5:
		this->setMagnet_radius(10);
		this->setMagnet_type(3);
		this->setSeed_type(2);
		this->setUse_initial_level_set(true);
		this->setControl_points_weight(1);
		break;
	}
}

InternalImageType::Pointer GACDemo::execute( InternalImageType::Pointer im_orig, InternalImageType::Pointer im_dist, NodeContainer::Pointer seeds, NodeContainer::Pointer magnets)
{
	XML_IN;
	outputImageFilename=outputImage;

	MaxType::Pointer max=MaxType::New();
	max->SetImage(im_orig);
	max->ComputeMaximum();
	max->ComputeMinimum();
	cout<<"max: " << max->GetMaximum()<< "min: "<<max->GetMinimum()<<endl;

	GeodesicActiveContourFilterType::Pointer geodesicActiveContour = GeodesicActiveContourFilterType::New();

	InternalImageType::Pointer imMagnet;
	imMagnet=configure_magnet(im_orig, magnets);
	geodesicActiveContour->SetDistanceImage(imMagnet);
	InternalImageType::RegionType region=im_orig->GetLargestPossibleRegion();
	InternalImageType::SizeType size=region.GetSize();
	cout<<"region: "<<region<<endl;

	InternalImageType::Pointer im_feat;
	im_feat=compute_features( im_orig );
	geodesicActiveContour->SetFeatureImage( im_feat );
	cout<<"region: "<<im_feat->GetLargestPossibleRegion()<<endl;

	InternalImageType::Pointer im_ini;
	im_ini=initialize(im_dist, size, seeds);
	geodesicActiveContour->SetInput( im_ini );
	cout<<"region: "<<im_ini->GetLargestPossibleRegion()<<endl;

	geodesicActiveContour->SetPropagationScaling( propagationScaling );
	geodesicActiveContour->SetCurvatureScaling( curvatureScaling);
	geodesicActiveContour->SetAdvectionScaling( 1.0 );
	geodesicActiveContour->SetControlPointsWeight(control_points_weight);
	geodesicActiveContour->SetMaximumRMSError( max_rms );
	geodesicActiveContour->SetNumberOfIterations( max_iter );


	typedef CommandIterationUpdate<GeodesicActiveContourFilterType> CommandType;
	CommandType::Pointer observer = CommandType::New();
	geodesicActiveContour->AddObserver( itk::IterationEvent(), observer );
	observer->setVerbose(verbose);

	geodesicActiveContour->Update();

	if(verbose>=2) save_output(im_orig, geodesicActiveContour->GetOutput() );

	// Print out some useful information
#ifdef GAC_USE_LOG
				util::infolog.log("Max. no. iterations: ");
				util::infolog.logln(geodesicActiveContour->GetElapsedIterations());
				util::infolog.log("Max. RMS error: ");
				util::infolog.logln(geodesicActiveContour->GetRMSChange());
				util::infolog.log("No. elapsed iterations: ");
				util::infolog.logln(geodesicActiveContour->GetElapsedIterations());
				util::infolog.log("RMS change: ");
				util::infolog.logln(geodesicActiveContour->GetRMSChange());
#else


	std::cout << std::endl;
	std::cout << "Max. no. iterations: " << geodesicActiveContour->GetNumberOfIterations() << std::endl;
	std::cout << "Max. RMS error: " << geodesicActiveContour->GetMaximumRMSError() << std::endl;
	std::cout << std::endl;
	std::cout << "No. elapsed iterations: " << geodesicActiveContour->GetElapsedIterations() << std::endl;
	std::cout << "RMS change: " << geodesicActiveContour->GetRMSChange() << std::endl;

#endif

	XML_OUT;
	return geodesicActiveContour->GetOutput();
}
