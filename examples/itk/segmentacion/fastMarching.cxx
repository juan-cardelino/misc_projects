/*
 * fastMarching.cxx
 *
 *  Created on: Feb 9, 2011
 *      Author: juanc
 */
#include <string>
#include <iostream>

#if 1

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)

//#include "itkGeodesicActiveContourLevelSetImageFilter2.h"
//#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
//#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
//#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
//#include "itkNumericSeriesFileNames.h"
//#include "itkShiftScaleImageFilter.h"
//#include "itkMinimumMaximumImageCalculator.h"
//#include "itkSignedDanielssonDistanceMapImageFilter.h"


  typedef   float           InternalPixelType;
  typedef   unsigned char           OutputPixelType;
  const     unsigned int    Dimension = 2;
  typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;
  typedef itk::Image< OutputPixelType, Dimension >  OutputImageType;
  typedef itk::ImageFileWriter< InternalImageType > InternalWriterType;
  //typedef itk::MinimumMaximumImageCalculator< InternalImageType > MaxType;

  typedef  itk::ImageFileReader< OutputImageType > OutputReaderType;
	  typedef  itk::ImageFileWriter<  InternalImageType > InternalWriterType;
		typedef  itk::FastMarchingImageFilter<
	                              InternalImageType,
	                              InternalImageType >    FastMarchingFilterType;

		typedef FastMarchingFilterType::NodeContainer  NodeContainer;
		  typedef FastMarchingFilterType::NodeType       NodeType;


		  typedef itk::Vector< unsigned char, 3 > ColorPixelType;
		  typedef itk::Image< ColorPixelType, 2 > ColorImageType;
		  typedef itk::ImageRegionIterator<InternalImageType> IteratorType;
		  typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
		  typedef itk::ImageRegionIterator<OutputImageType> OutputIteratorType;
		  typedef IteratorType::IndexType IndexType;
		  typedef ColorIteratorType::IndexType ColorIndexType;
		   typedef  itk::ImageFileWriter<  ColorImageType  > ColorWriterType;

void fm(std::string inFname, std::string outFname)
{
	  OutputReaderType::Pointer reader=OutputReaderType::New();
	  reader->SetFileName(inFname);

  FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();

  NodeContainer::Pointer seeds = NodeContainer::New();

  InternalImageType::IndexType  seedPosition;

  seeds->Initialize();

  	ColorImageType::Pointer out= ColorImageType::New();

  	InternalImageType::RegionType region;
  	OutputImageType::Pointer im=reader->GetOutput();
	reader->Update();
  	region=im->GetLargestPossibleRegion();
	std::cout<<"size: "<<region.GetSize()<<std::endl;

  	//now merge both images
  	OutputIteratorType iter1( im, im->GetRequestedRegion() );
  	int i=0;
  	for ( iter1.GoToBegin(); !iter1.IsAtEnd(); ++iter1)
  	{
  		OutputPixelType g1;
  		g1=iter1.Get();

  		if(g1>0)
  		{
  			seedPosition=iter1.GetIndex();
  			NodeType node;
  		    const double seedValue = 0;
  		    node.SetValue( seedValue );
  		    node.SetIndex( seedPosition );
  		    seeds->InsertElement(i++, node );

  		}
  	}

  	fastMarching->SetTrialPoints(  seeds  );
  	fastMarching->SetSpeedConstant( 1.0 );
  	fastMarching->SetOutputSize(reader->GetOutput()->GetBufferedRegion().GetSize() );

	InternalWriterType::Pointer writer=InternalWriterType::New();
	writer->SetInput(fastMarching->GetOutput());
	writer->SetFileName(outFname);
	writer->Update();

}

#endif

//int main(int argc, char* argv[])
int main()
{
	std::cout<<"hola"<<std::endl;
	//fm(argv[1],argv[2]);

	return 0;
}
