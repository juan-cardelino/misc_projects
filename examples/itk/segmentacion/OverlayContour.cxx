#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif


#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSpatialFunctionImageEvaluatorFilter.h"
#include "itkContourExtractor2DImageFilter.h"
#include "itkPathIterator.h"


int main( int argc, char *argv[] )
{
	if( argc < 3 )
	{
		std::cerr << "Missing Parameters " << std::endl;
		std::cerr << "Usage: " << argv[0];
		std::cerr << " input_image phi output_image" << std::endl;
		return 1;
	}

	const   unsigned int        Dimension = 2;
	typedef float    PixelType;
	typedef unsigned char    OutPixelType;
	typedef itk::Image< PixelType, Dimension >    InternalImageType;
	typedef itk::Image< OutPixelType, Dimension >    OutImageType;

	typedef  itk::ImageFileReader< InternalImageType > InternalReaderType;
	typedef  itk::ImageFileReader< OutImageType > ReaderType;
	typedef  itk::ImageFileWriter< OutImageType  > WriterType;

	typedef itk::ContourExtractor2DImageFilter<InternalImageType> ExtractorType;

	ReaderType::Pointer reader = ReaderType::New();
	InternalReaderType::Pointer readerDist = InternalReaderType::New();

	ExtractorType::Pointer extractor = ExtractorType::New();

	reader->SetFileName(argv[1]); 
	readerDist->SetFileName(argv[2]);

	extractor->SetInput(readerDist->GetOutput());
	extractor->SetContourValue(2.5);
	extractor->VertexConnectHighPixelsOff();
	extractor->ReverseContourOrientationOff();
	extractor->Update();

	// Run and output data to data object
	typedef ExtractorType::OutputPathType PathType;
	PathType::Pointer path;
	path = extractor->GetOutput();
	path->Print(std::cout);

	typedef itk::PathIterator< InternalImageType, PathType > IteratorType;
	typedef PathType::ContinuousIndexType VertexType;

	//create output image
	typedef itk::Vector< unsigned char, 3 > ColorPixelType;
	typedef itk::Image< ColorPixelType, 2 > ColorImageType;
	typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
	typedef ColorIteratorType::IndexType ColorIndexType;
	typedef itk::ImageFileWriter<  ColorImageType  > ColorWriterType;

	ColorImageType::Pointer out= ColorImageType::New();

	//imt1
	InternalImageType::RegionType region;
	region=readerDist->GetOutput()->GetLargestPossibleRegion();
	out->SetRegions( region );
	out->Allocate();

	ColorPixelType v_color;
	v_color[0]=255;
	v_color[1]=0;
	v_color[2]=0;


typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
typedef ColorIteratorType::IndexType ColorIndexType;

	ColorPixelType v;
		v[0]=0;
		v[1]=0;
		v[2]=0;	

	ColorIteratorType iter3( out, out->GetRequestedRegion() );
	for (iter3.GoToBegin(); !iter3.IsAtEnd();  ++iter3 )
	{
			iter3.Set(v);

		}


	// Create iterator following path through image
	IteratorType iter( readerDist->GetOutput(), path );
	int i=0;
	while (!iter.IsAtEnd())
	{
		//std::cout << iter.GetPathPosition() << " -> " << std::flush;
		std::cout <<"id: "<<i++<<" path pos: "<< iter.GetPathPosition() << " im pos: "<<iter.GetIndex() << std::endl;
		out->SetPixel(iter.GetIndex(),v_color);

		try
		{
			++iter;
		}
		catch( itk::ExceptionObject & err )
		{
			std::cout << "ExceptionObject caught !" << std::endl;
			std::cout << err << std::endl;
			return -1;
		}

	}

	ColorWriterType::Pointer writer = ColorWriterType::New();
	writer->SetFileName(argv[3]);
	writer->SetInput(out);
	writer->Update();

	return 0;
}
