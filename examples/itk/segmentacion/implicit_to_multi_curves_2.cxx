#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

#include "itkLevelSetSegmentationCommon.h"

#include "itkImage.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkLabelObject.h"
#include "itkShapeLabelObject.h"
#include "itkLabelMap.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkShapeLabelMapFilter.h"


int main( int argc, char *argv[] )
{
	if( argc < 2 )
	{
		std::cerr << "Missing Parameters " << std::endl;
		std::cerr << "Usage: " << argv[0];
		std::cerr << " input_image " << std::endl;
		return 1;
	}

	const   unsigned int        Dimension = 2;
	typedef float    PixelType;
	typedef unsigned char    OutPixelType;
	typedef itk::Image< PixelType, Dimension >    ImageType;
	typedef itk::Image< OutPixelType, Dimension >    OutImageType;

	typedef  itk::ImageFileReader< ImageType > ReaderType;
	typedef  itk::ImageFileWriter< OutImageType  > WriterType;

	//reader distance map
	ReaderType::Pointer reader = ReaderType::New();
	reader->SetFileName(argv[1]); 
	reader->Update();

	segmentation_output_to_curves(reader->GetOutput());

	return 0;
}
