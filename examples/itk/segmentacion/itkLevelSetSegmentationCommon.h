/*
 * itkLevelSetSegmentationCommon.h
 *
 *  Created on: 30/03/2011
 *      Author: juan
 */

#ifndef ITKLEVELSETSEGMENTATIONCOMMON_H_
#define ITKLEVELSETSEGMENTATIONCOMMON_H_

#include "itkImage.h"
#include "itkImageFileWriter.h"
#include "itkSegmentationLevelSetImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkShiftScaleImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkSignedDanielssonDistanceMapImageFilter.h"
#include "itkImage.h"
#include "itkSpatialFunctionImageEvaluatorFilter.h"
#include "itkContourExtractor2DImageFilter.h"
#include "itkPathIterator.h"
#include <vector>

#ifdef GAC_USE_LOG
#include "util/Logger.h"
#endif

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)

typedef   float           InternalPixelType;
const     unsigned int    Dimension = 2;
typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;
typedef itk::ImageFileWriter< InternalImageType > InternalWriterType;
typedef itk::MinimumMaximumImageCalculator< InternalImageType > MaxType;


typedef unsigned char OutputPixelType;
typedef itk::Image<OutputPixelType, Dimension> OutputImageType;

typedef  itk::FastMarchingImageFilter<
		InternalImageType,
		InternalImageType >    FastMarchingFilterType;
typedef FastMarchingFilterType::NodeContainer  NodeContainer;
typedef FastMarchingFilterType::NodeType       NodeType;

typedef itk::BinaryThresholdImageFilter<InternalImageType, OutputImageType > ThresholdingFilterType;
typedef  itk::ImageFileReader< InternalImageType > ReaderType;
typedef  itk::ImageFileWriter<  OutputImageType  > WriterType;
typedef itk::RescaleIntensityImageFilter< InternalImageType, OutputImageType > CastFilterType;


typedef itk::ContourExtractor2DImageFilter<InternalImageType> ExtractorType;
typedef ExtractorType::OutputPathType PathType;
typedef itk::PathIterator< InternalImageType, PathType > IteratorType;
typedef PathType::ContinuousIndexType VertexType;


template<class TFilter>
class CommandIterationUpdate : public itk::Command
{
private:
	int m_verbose;
public:
	typedef CommandIterationUpdate Self;
	typedef itk::Command Superclass;
	typedef itk::SmartPointer<Self> Pointer;
	itkNewMacro( Self);
	void setVerbose(int verbose)
	{
		m_verbose=verbose;
	}
protected:
	CommandIterationUpdate()
	{
		m_verbose=0;
	}
public:

	void Execute(itk::Object *caller, const itk::EventObject & event)
	{
		Execute((const itk::Object *) caller, event);
		TFilter * filter = dynamic_cast<TFilter *> (caller);
		if (typeid( event ) != typeid(itk::IterationEvent))
		{
			return;
		}

		typedef float InternalPixelType;
		const     unsigned int    Dimension = 2;

		typedef itk::Image<InternalPixelType, Dimension>
		InternalImageType;
		typedef unsigned char OutputPixelType;
		typedef itk::Image<OutputPixelType, Dimension> OutputImageType;

		typedef itk::BinaryThresholdImageFilter<
				InternalImageType,
				OutputImageType> ThresholdingFilterType;
		typedef itk::ImageFileWriter<OutputImageType> WriterType;

		int max_iter=filter->GetNumberOfIterations();
		int n=50;
		//cout<<"max_iter: "<<filter->GetNumberOfIterations()<<endl;
		int step=max_iter/n;
		if(step==0) step=1;
		int iter = filter->GetElapsedIterations();


		if(m_verbose>=1)
		{
			if ((iter % step) == 0)
			{
#ifdef GAC_USE_LOG
				util::infolog.log("i: ");
				util::infolog.log(filter->GetElapsedIterations());
				util::infolog.log(" rms: ");
				util::infolog.logln(filter->GetRMSChange());
#else
				std::cout << filter->GetElapsedIterations() << ": ";
				std::cout << filter->GetRMSChange() << " "<<std::endl;
#endif
			}
		}
				if(m_verbose>=2)
						{
							if ((iter % step) == 0)
							{
				WriterType::Pointer writer = WriterType::New();
				char filename[64];
				sprintf(filename, "debug/debug.%05d.png", iter);
				writer->SetFileName(filename);

				ThresholdingFilterType::Pointer thresholder =
						ThresholdingFilterType::New();

				thresholder->SetLowerThreshold(-1000.0);
				thresholder->SetUpperThreshold(0.0);

				thresholder->SetOutsideValue(0);
				thresholder->SetInsideValue(255);
				thresholder->SetInput(filter->GetOutput(0));
				writer->SetInput(thresholder->GetOutput());
				writer->Update();

				if(m_verbose>0)
				{
					MaxType::Pointer max=MaxType::New();
					max->SetImage(filter->GetOutput(0));
					max->ComputeMaximum();
					max->ComputeMinimum();

					std::cout<<"max: " << max->GetMaximum()<< " min: "<<max->GetMinimum()<<std::endl;
				}
			}
		}
	}



	void Execute(const itk::Object * object, const itk::EventObject & event)
	{
		/*
 const TFilter * filter =
	dynamic_cast<const TFilter * >( object );
 if( typeid( event ) != typeid( itk::IterationEvent ) )
	{ return; }
		 */

		//std::cout << filter->GetElapsedIterations() << ": ";
		//std::cout << filter->GetRMSChange() << " "<<std::endl;

	}

};

void overlay(InternalImageType::Pointer imt1, OutputImageType::Pointer imt2, std::string filename);
void offset(std::string in,std::string out, double ls_offset);
NodeContainer::Pointer read_seeds(std::string in, const double seedValue);
void read_coords(FILE* fid, float *v);
void read_polyline(std::string filename,  int *m, int *n, float poses[][2]);
void print_coords_f_n(float *v, const char * name, int n);
InternalImageType::Pointer levelset_union( InternalImageType::Pointer im1, InternalImageType::Pointer im2);
InternalImageType::Pointer levelset_intersection( InternalImageType::Pointer im1, InternalImageType::Pointer im2);
void save_debug_results(InternalImageType::Pointer im_smooth, InternalImageType::Pointer im_grad, InternalImageType::Pointer im_sigmoid, InternalImageType::Pointer im_dist);
NodeContainer::Pointer convert_coordinates(float points[][2],int nb_points, const double seedValue);
/** Computes the zero level set of an implicit function and returns it as a curve (list of 2D points) */
PathType::Pointer implicit_to_curve( InternalImageType::Pointer im);
OutputImageType::Pointer points_to_mask( OutputImageType::SizeType size, float points[][2], const unsigned int numberOfPoints  );
std::vector<PathType::Pointer> segmentation_output_to_curves(InternalImageType::Pointer im_dist );
#endif /* ITKLEVELSETSEGMENTATIONCOMMON_H_ */
