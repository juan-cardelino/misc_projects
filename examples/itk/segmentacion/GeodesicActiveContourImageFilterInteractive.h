/*
 * GeodesicActiveContourImageFilter.h
 *
 *  Created on: Mar 22, 2011
 *      Author: juan
 */

#ifndef GEODESICACTIVECONTOURIMAGEFILTER_H_
#define GEODESICACTIVECONTOURIMAGEFILTER_H_

#include "util/util.h"

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkLevelSetSegmentationCommon.h"


// Software Guide : BeginCodeSnippet
#include "itkGeodesicActiveContourLevelSetImageFilter2.h"
// Software Guide : EndCodeSnippet


#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkShiftScaleImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkSignedDanielssonDistanceMapImageFilter.h"

#ifdef GAC_USE_CONF
class GACDemo: public util::Configurable
#else
class GACDemo
#endif
{
private:
	int step;
	int max_iter;
	double max_rms;
	std::string inputImage;
	std::string outputImage;
	std::string initialLevelSet;
	std::string seedFile;
	double propagationScaling;
	double curvatureScaling;
	double initialDistance;
	double sigma;
	double alpha;
	double beta;
	double control_points_weight;
	bool use_initial_level_set;
	bool use_tracking;
	bool use_fixed_level_set;
	bool use_offset;
	bool use_library_mode;
	/** 1: read them from disk 2: compute from give coordinates*/
	int seed_type;
	int ind_start;
	int ind_end;
	int magnet_type;
	float magnet_radius;
	int verbose;
	std::string inputImageFilename;
	std::string outputImageFilename;
	std::string seedFilename;
	std::string initialLevelSetFilename;
	std::string userDistanceFilename;

public:

	GACDemo();

#ifdef GAC_USE_CONF
	void configure(const util::Configuration& conf) throw (util::ConfException);
	util::Configuration getConfiguration() const;
#endif

	void run();
	void tracking();
	void segment();
	InternalImageType::Pointer compute_features(InternalImageType::Pointer im_orig);

	void save_debug_results(InternalImageType::Pointer im_smooth, InternalImageType::Pointer im_grad, InternalImageType::Pointer im_sigmoid);
	void save_output(InternalImageType::Pointer im_orig,InternalImageType::Pointer im_out);
	void save_debug_initialization(InternalImageType::Pointer im_dist);
	InternalImageType::Pointer  initialize(InternalImageType::Pointer im_dist, InternalImageType::SizeType size, NodeContainer::Pointer seeds );
	InternalImageType::Pointer  configure_magnet(InternalImageType::Pointer im_orig, NodeContainer::Pointer magnets);
	InternalImageType::Pointer execute( InternalImageType::Pointer im_orig, InternalImageType::Pointer im_dist, NodeContainer::Pointer seeds, NodeContainer::Pointer magnets);
	void configure_use_case(int case_number);

	//ACCESS METHODS

	bool getUse_library_mode() const
	    {
	        return use_library_mode;
	    }
	void setUse_library_mode(bool v)
	    {
	        this->use_library_mode=v;
	    }

    double getAlpha() const
    {
        return alpha;
    }

    double getBeta() const
    {
        return beta;
    }

    double getControl_points_weight() const
    {
        return control_points_weight;
    }

    double getCurvatureScaling() const
    {
        return curvatureScaling;
    }

    int getInd_end() const
    {
        return ind_end;
    }

    int getInd_start() const
    {
        return ind_start;
    }

    double getInitialDistance() const
    {
        return initialDistance;
    }

    std::string getInitialLevelSet() const
    {
        return initialLevelSet;
    }

    std::string getInitialLevelSetFilename() const
    {
        return initialLevelSetFilename;
    }

    std::string getInputImage() const
    {
        return inputImage;
    }

    std::string getInputImageFilename() const
    {
        return inputImageFilename;
    }

    float getMagnet_radius() const
    {
        return magnet_radius;
    }

    int getMagnet_type() const
    {
        return magnet_type;
    }

    int getMax_iter() const
    {
        return max_iter;
    }

    double getMax_rms() const
    {
        return max_rms;
    }

    std::string getOutputImage() const
    {
        return outputImage;
    }

    std::string getOutputImageFilename() const
    {
        return outputImageFilename;
    }

    double getPropagationScaling() const
    {
        return propagationScaling;
    }

    std::string getSeedFile() const
    {
        return seedFile;
    }

    std::string getSeedFilename() const
    {
        return seedFilename;
    }

    double getSigma() const
    {
        return sigma;
    }

    int getStep() const
    {
        return step;
    }

    bool getUse_fixed_level_set() const
    {
        return use_fixed_level_set;
    }

    bool getUse_initial_level_set() const
    {
        return use_initial_level_set;
    }

    bool getUse_offset() const
    {
        return use_offset;
    }



    bool getUse_tracking() const
    {
        return use_tracking;
    }

    std::string getUserDistanceFilename() const
    {
        return userDistanceFilename;
    }

    int getVerbose() const
    {
        return verbose;
    }

    void setAlpha(double alpha)
    {
        this->alpha = alpha;
    }

    void setBeta(double beta)
    {
        this->beta = beta;
    }

    void setControl_points_weight(double control_points_weight)
    {
        this->control_points_weight = control_points_weight;
    }

    void setCurvatureScaling(double curvatureScaling)
    {
        this->curvatureScaling = curvatureScaling;
    }

    void setInd_end(int ind_end)
    {
        this->ind_end = ind_end;
    }

    void setInd_start(int ind_start)
    {
        this->ind_start = ind_start;
    }

    void setInitialDistance(double initialDistance)
    {
        this->initialDistance = initialDistance;
    }

    void setInitialLevelSet(std::string initialLevelSet)
    {
        this->initialLevelSet = initialLevelSet;
    }

    void setInitialLevelSetFilename(std::string initialLevelSetFilename)
    {
        this->initialLevelSetFilename = initialLevelSetFilename;
    }

    void setInputImage(std::string inputImage)
    {
        this->inputImage = inputImage;
    }

    void setInputImageFilename(std::string inputImageFilename)
    {
        this->inputImageFilename = inputImageFilename;
    }

    void setMagnet_radius(float magnet_radius)
    {
        this->magnet_radius = magnet_radius;
    }

    void setMagnet_type(int magnet_type)
    {
        this->magnet_type = magnet_type;
    }

    void setMax_iter(int max_iter)
    {
        this->max_iter = max_iter;
    }

    void setMax_rms(double max_rms)
    {
        this->max_rms = max_rms;
    }

    void setOutputImage(std::string outputImage)
    {
        this->outputImage = outputImage;
    }

    void setOutputImageFilename(std::string outputImageFilename)
    {
        this->outputImageFilename = outputImageFilename;
    }

    void setPropagationScaling(double propagationScaling)
    {
        this->propagationScaling = propagationScaling;
    }

    void setSeedFile(std::string seedFile)
    {
        this->seedFile = seedFile;
    }

    void setSeedFilename(std::string seedFilename)
    {
        this->seedFilename = seedFilename;
    }

    void setSigma(double sigma)
    {
        this->sigma = sigma;
    }

    void setStep(int step)
    {
        this->step = step;
    }

    void setUse_fixed_level_set(bool use_fixed_level_set)
    {
        this->use_fixed_level_set = use_fixed_level_set;
    }

    void setUse_initial_level_set(bool use_initial_level_set)
    {
        this->use_initial_level_set = use_initial_level_set;
    }

    void setUse_offset(bool use_offset)
    {
        this->use_offset = use_offset;
    }


    void setUse_tracking(bool use_tracking)
    {
        this->use_tracking = use_tracking;
    }

    void setUserDistanceFilename(std::string userDistanceFilename)
    {
        this->userDistanceFilename = userDistanceFilename;
    }

    void setVerbose(int verbose)
    {
        this->verbose = verbose;
    }

    void setSeed_type(int seed_type)
    {
    	this->seed_type=seed_type;
    }

    int getSeed_type()
	{
		return this->seed_type;
	}
};


#endif /* GEODESICACTIVECONTOURIMAGEFILTER_H_ */
