/*
 * GeodesicActiveContourImageFilterInteractiveTest.cxx
 *
 *  Created on: Jun 21, 2011
 *      Author: juan
 */


#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "GeodesicActiveContourImageFilterInteractive.h"

int main( int argc, const char *argv[] )
{
	std::string svn_version="svn_version: $Id: GeodesicActiveContourImageFilterInteractiveTest.cxx 379 2012-01-24 19:44:21Z juanc $";
	std::cout<<"svn_version: "<<svn_version<<std::endl;

	//read original image
	ReaderType::Pointer reader = ReaderType::New();
	std::string inputImageFilename="../data/iman.png";
	std::cout<<"reader filename: "<<inputImageFilename <<std::endl;
	reader->SetFileName( inputImageFilename );
	reader->Update();

	InternalImageType::Pointer im_orig;
	im_orig=reader->GetOutput();

	//set magnet points
	float magnet_points[5][2]={
						{30, 25},
						{40, 25},
						{50, 25},
						{60, 25},
						{70, 25}};
	NodeContainer::Pointer magnets = convert_coordinates(magnet_points,5,0);

	//set seed points
	//aca nuevamente puedo hacerlo desde un array, te digo para ahorrar código.
	float seed_points[1][2]={{50, 50}};
	float radius=10;
	NodeContainer::Pointer seeds = convert_coordinates(seed_points,1,-radius);

	GACDemo algo;
	algo.configure_use_case(1);
	algo.setVerbose(1);	//put verbose to zero to avoid saving debug output
	InternalImageType::Pointer im_out=algo.execute(im_orig, NULL, seeds, magnets);

	//get the curve from the implicit function
	PathType::Pointer path=implicit_to_curve( im_out);

	return 0;
}
