#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

#include "itkLevelSetSegmentationCommon.h"

#include "itkImage.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkLabelObject.h"
#include "itkShapeLabelObject.h"
#include "itkLabelMap.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkShapeLabelMapFilter.h"


int main( int argc, char *argv[] )
{
	if( argc < 2 )
	{
		std::cerr << "Missing Parameters " << std::endl;
		std::cerr << "Usage: " << argv[0];
		std::cerr << " input_image " << std::endl;
		return 1;
	}

	const   unsigned int        Dimension = 2;
	typedef float    PixelType;
	typedef unsigned char    OutPixelType;
	typedef itk::Image< PixelType, Dimension >    ImageType;
	typedef itk::Image< OutPixelType, Dimension >    OutImageType;

	typedef  itk::ImageFileReader< ImageType > ReaderType;
	typedef  itk::ImageFileWriter< OutImageType  > WriterType;

	//reader distance map
	ReaderType::Pointer reader = ReaderType::New();
	reader->SetFileName(argv[1]); 

	//generate a mask
	ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
	thresholder->SetLowerThreshold( -1000.0 );
	thresholder->SetUpperThreshold(     0.0 );
	thresholder->SetOutsideValue(  0  );
	thresholder->SetInsideValue(  255 );
	thresholder->SetInput( reader->GetOutput() );

	//write mask down (for debug only)
	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName( "debug_mask.png" );
	writer->SetInput( thresholder->GetOutput() );
	writer->Update();


	//convert to label map
	 typedef unsigned char BinaryPixelType;
	  typedef unsigned char LabelPixelType;


	  //typedef itk::LabelObject< LabelPixelType, Dimension >   LabelObjectType;

	  typedef itk::ShapeLabelObject< LabelPixelType, Dimension > LabelObjectType;

	  typedef itk::LabelMap< LabelObjectType >                LabelMapType;


	  typedef itk::BinaryImageToLabelMapFilter< OutImageType, LabelMapType> I2LType;
	  I2LType::Pointer i2l = I2LType::New();
	  // test the behavior without input

	  i2l->SetFullyConnected( 0 );
	  i2l->SetInputForegroundValue( 255 );
	  i2l->SetOutputBackgroundValue( 0 );


	  typedef itk::LabelMapToLabelImageFilter< LabelMapType, OutImageType> L2IType;
	  L2IType::Pointer l2i = L2IType::New();

	  WriterType::Pointer writer2 = WriterType::New();

	  writer2->SetFileName( "debug_label_map.png" );
	  writer2->UseCompressionOn();


	  i2l->SetInput( thresholder->GetOutput() );
	  l2i->SetInput( i2l->GetOutput() );
	  writer2->SetInput( l2i->GetOutput() );

	    writer2->Update();

	    std::cout<<"nb: "<<i2l->GetOutput()->GetNumberOfLabelObjects()<<std::endl;

	    typedef itk::ShapeLabelMapFilter< LabelMapType > ShapeFilterType;
	    ShapeFilterType::Pointer shape = ShapeFilterType::New();
	    shape->SetInput( i2l->GetOutput() );
	    shape->Update();


	    i2l->GetOutput()->PrintLabelObjects();

	    LabelMapType::Pointer labelMap = i2l->GetOutput();
	    for( unsigned int label=1; label<=labelMap->GetNumberOfLabelObjects(); label++ )
	    {
	    	// we don’t need a SmartPointer of the label object here, because the reference is kept in
	    	// in the label map.
	    	const LabelObjectType * labelObject = labelMap->GetLabelObject( label );
	    	std::cout << label << "\t" << labelObject->GetPhysicalSize() << "\t" << labelObject->GetCentroid()<<std::endl;
	    	//std::cout << *labelObject<<std::endl;
	    	//labelObject->Print( std::cout );



	    //convert to contours
	    typedef itk::ContourExtractor2DImageFilter<OutImageType> ExtractorType;

	    	ExtractorType::Pointer extractor = ExtractorType::New();

	    	extractor->SetInput(l2i->GetOutput());
	    	extractor->SetContourValue(label-0.5);
	    	extractor->VertexConnectHighPixelsOff();
	    	extractor->ReverseContourOrientationOff();
	    	extractor->Update();

	    	// Run and output data to data object
	    	typedef ExtractorType::OutputPathType PathType;
	    	PathType::Pointer path;
	    	path = extractor->GetOutput();
	    	path->Print(std::cout);

	    	typedef itk::PathIterator< OutImageType, PathType > IteratorType;
	    	typedef PathType::ContinuousIndexType VertexType;

	    	//create output image
	    	typedef itk::Vector< unsigned char, 3 > ColorPixelType;
	    	typedef itk::Image< ColorPixelType, 2 > ColorImageType;
	    	typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
	    	typedef ColorIteratorType::IndexType ColorIndexType;
	    	typedef itk::ImageFileWriter<  ColorImageType  > ColorWriterType;

	    	ColorImageType::Pointer out= ColorImageType::New();

	    	//imt1
	    	InternalImageType::RegionType region;
	    	region=l2i->GetOutput()->GetLargestPossibleRegion();
	    	out->SetRegions( region );
	    	out->Allocate();

	    	ColorPixelType v_color;
	    	v_color[0]=255;
	    	v_color[1]=0;
	    	v_color[2]=0;


	    typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
	    typedef ColorIteratorType::IndexType ColorIndexType;

	    	ColorPixelType v;
	    		v[0]=0;
	    		v[1]=0;
	    		v[2]=0;

	    	ColorIteratorType iter3( out, out->GetRequestedRegion() );
	    	for (iter3.GoToBegin(); !iter3.IsAtEnd();  ++iter3 )
	    	{
	    			iter3.Set(v);
    		}


	    	// Create iterator following path through image
	    	IteratorType iter( l2i->GetOutput(), path );
	    	int i=0;
	    	while (!iter.IsAtEnd())
	    	{
	    		//std::cout << iter.GetPathPosition() << " -> " << std::flush;
	    		std::cout <<"id: "<<i++<<" path pos: "<< iter.GetPathPosition() << " im pos: "<<iter.GetIndex() << std::endl;
	    		out->SetPixel(iter.GetIndex(),v_color);

	    		try
	    		{
	    			++iter;
	    		}
	    		catch( itk::ExceptionObject & err )
	    		{
	    			std::cout << "ExceptionObject caught !" << std::endl;
	    			std::cout << err << std::endl;
	    			return -1;
	    		}

	    	}

	    	ColorWriterType::Pointer writer = ColorWriterType::New();
	    	const char* filename_base="debug_contour_%02d.png";
	    	char filename[64];
	    	sprintf(filename,filename_base,label);
	    	std::cout<<"filename: "<<filename<<std::endl;
	    	writer->SetFileName(filename);
	    	writer->SetInput(out);
	    	writer->Update();

	    }



	return 0;
}
