#include <util/util.h>
#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)



// Software Guide : BeginCodeSnippet
#include "itkGeodesicActiveContourLevelSetImageFilter.h"
// Software Guide : EndCodeSnippet


#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkShiftScaleImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkSignedDanielssonDistanceMapImageFilter.h"

  typedef   float           InternalPixelType;
  const     unsigned int    Dimension = 2;
  typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;
  typedef itk::ImageFileWriter< InternalImageType > InternalWriterType;
  typedef itk::MinimumMaximumImageCalculator< InternalImageType > MaxType;


	typedef unsigned char OutputPixelType;
	typedef itk::Image<OutputPixelType, Dimension> OutputImageType;

  template<class TFilter>
class CommandIterationUpdate : public itk::Command
{
public:
	typedef CommandIterationUpdate Self;
	typedef itk::Command Superclass;
	typedef itk::SmartPointer<Self> Pointer;
	itkNewMacro( Self);
protected:
	CommandIterationUpdate()
	{}
public:

	void Execute(itk::Object *caller, const itk::EventObject & event)
	{
		Execute((const itk::Object *) caller, event);
		TFilter * filter = dynamic_cast<TFilter *> (caller);
		if (typeid( event ) != typeid(itk::IterationEvent))
				{
					return;
				}

		typedef float InternalPixelType;


					typedef itk::Image<InternalPixelType, Dimension>
							InternalImageType;

					typedef itk::BinaryThresholdImageFilter<
								InternalImageType,
								OutputImageType> ThresholdingFilterType;
					typedef itk::ImageFileWriter<OutputImageType> WriterType;

				int iter = filter->GetElapsedIterations();

				if ((iter % 50) == 0 && 1)
				{
					WriterType::Pointer writer = WriterType::New();
					char filename[64];
					sprintf(filename, "debug/debug.%05d.png", iter);
					writer->SetFileName(filename);

					ThresholdingFilterType::Pointer thresholder =
							ThresholdingFilterType::New();

					thresholder->SetLowerThreshold(-1000.0);
					thresholder->SetUpperThreshold(0.0);

					thresholder->SetOutsideValue(0);
					thresholder->SetInsideValue(255);
					thresholder->SetInput(filter->GetOutput());
					writer->SetInput(thresholder->GetOutput());
					writer->Update();
				}
			}



void Execute(const itk::Object * object, const itk::EventObject & event)
 {
 const TFilter * filter =
	dynamic_cast<const TFilter * >( object );
 if( typeid( event ) != typeid( itk::IterationEvent ) )
	{ return; }

 //std::cout << filter->GetElapsedIterations() << ": ";
 //std::cout << filter->GetRMSChange() << " "<<std::endl;


 typedef   float           InternalPixelType;


typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;

typedef itk::BinaryThresholdImageFilter<
							InternalImageType,
							OutputImageType    >       ThresholdingFilterType;
typedef  itk::ImageFileWriter<  OutputImageType  > WriterType;

#if 0
 WriterType::Pointer writer = WriterType::New();
char filename[64];
sprintf(filename,"debug.%04d.png",filter->GetElapsedIterations() );
writer->SetFileName(  filename);

ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

thresholder->SetLowerThreshold( -1000.0 );
thresholder->SetUpperThreshold(     0.0 );

thresholder->SetOutsideValue(  0  );
thresholder->SetInsideValue(  255 );
thresholder->SetInput(filter->GetOutput());
writer->SetInput(thresholder->GetOutput());
writer->Update();
#endif
 }

};


class GACDemo: public util::Configurable
{
private:
	int step;

public:
	int max_iter;
	double max_rms;
	std::string inputImage;
	std::string outputImage;
	std::string initialLevelSet;
	double propagationScaling;
	InternalImageType::IndexType  seedPosition;
	double initialDistance;
	double sigma;
	double alpha;
	double beta;
	bool use_initial_level_set;
	bool use_tracking;
	bool use_fixed_level_set;
	bool use_offset;
	int ind_start;
	int ind_end;

	std::string inputImageFilename;
	std::string outputImageFilename;
	std::string initialLevelSetFilename;


	GACDemo()
	{
		max_iter=4000;
		use_initial_level_set=false;
		seedPosition[0] = 0;
		seedPosition[1] = 0;
		use_tracking=false;
		step=-1;
		use_fixed_level_set=false;
		use_offset=false;
		max_rms=0.02;
		ind_start=1;
		ind_end=2;
	}

	  void configure(const util::Configuration& conf)
	    throw (util::ConfException)
	  {
		  max_iter = conf.getIntValue("max_iter");
		  inputImage = conf.getValue("inputImage");
		  outputImage = conf.getValue("outputImage");
		  initialLevelSet = conf.getValue("initialLevelSet");
		  propagationScaling = conf.getFloatValue("propagationScaling");
		  seedPosition[0] = conf.getFloatValue("seedPositionX");
		  seedPosition[1] = conf.getFloatValue("seedPositionY");
		initialDistance=conf.getFloatValue("initialDistance");;
		sigma=conf.getFloatValue("sigma");
		alpha=conf.getFloatValue("alpha");
		beta=conf.getFloatValue("beta");
		 use_initial_level_set = conf.getBoolValue("use_initial_level_set");
		 use_tracking = conf.getBoolValue("use_tracking");
		 use_fixed_level_set = conf.getBoolValue("use_fixed_level_set");
		 max_rms = conf.getFloatValue("max_rms");
		 use_offset = conf.getBoolValue("use_offset");
		 ind_start = conf.getIntValue("ind_start");
		 ind_end = conf.getIntValue("ind_end");
	  }

	util::Configuration getConfiguration() const
	{
		util::Configuration miConf("base","Configuracion base");
		miConf.addOption("max_iter","","500");
		miConf.addOption("use_initial_level_set","","false");
		miConf.addOption("inputImage","","in.png");
		miConf.addOption("outputImage","","out.png");
		miConf.addOption("initialLevelSet","","phi.mha");
		miConf.addOption("propagationScaling","","1");
		miConf.addOption("seedPositionX","","0");
		miConf.addOption("seedPositionY","","0");
		miConf.addOption("initialDistance","","5");
		miConf.addOption("sigma","","1");
		miConf.addOption("alpha","","1");
		miConf.addOption("beta","","1");
		miConf.addOption("use_initial_level_set","","false");
		miConf.addOption("use_tracking","","false");
		miConf.addOption("use_fixed_level_set","","false");
		miConf.addOption("max_rms","","0.02");
		miConf.addOption("use_offset","","false");
		miConf.addOption("ind_start","","1");
		miConf.addOption("ind_end","","2");
		return miConf;
	}

	void run();
	void tracking();
	void segment();
	void overlay(InternalImageType::Pointer imt1, OutputImageType::Pointer imt2, std::string filename);
	void offset(std::string in,std::string out);
};




int main( int argc, const char *argv[] )
{

	util::Logger::setLogLevel(util::Logger::INFO);
	//	cout << 	setprecision(5);
	util::infolog.logln("Starting algorithm");
	GACDemo demo;


	try {
		//
		// get core configuration parameters
		//
		util::Configuration cfg = demo.getConfiguration();
		//
		// create a configuration manager to read and write the configuration
		//
		util::ConfManager conf_manager(cfg);
		//
		// read config. from command line
		//
		cfg = conf_manager.parseConf(argc,argv);
		//
		// configure the algorithm
		//
		demo.configure(cfg);

		//and finally run the algorithm
		demo.run();

	} catch (base::Exception ex) {
				std::cout << "EPA! " << ex.getMessage() << std::endl;
	}

	  return 0;
}
using namespace std;

void GACDemo::run()
{
	if (use_tracking)
		tracking();
	else
	{
		inputImageFilename=inputImage;
		outputImageFilename=outputImage;
		initialLevelSetFilename=initialLevelSet;
		segment();
	}
}

void GACDemo::tracking()
{


	itk::NumericSeriesFileNames::Pointer filenames=itk::NumericSeriesFileNames::New();
	filenames->SetSeriesFormat(inputImage);
	filenames->SetStartIndex(ind_start);
	filenames->SetEndIndex(ind_end);
	vector<string> fnames=filenames->GetFileNames();

	itk::NumericSeriesFileNames::Pointer filenamesOut=itk::NumericSeriesFileNames::New();
	filenamesOut->SetSeriesFormat(outputImage);
	filenamesOut->SetStartIndex(ind_start);
	filenamesOut->SetEndIndex(ind_end);
	vector<string> fnamesOut=filenamesOut->GetFileNames();
	for(unsigned int i=0;i<fnames.size();i++)
	{
		step=i;
		inputImageFilename=fnames[i]+".png";
		outputImageFilename=fnamesOut[i];
		if(i==0 || use_fixed_level_set)
			initialLevelSetFilename=initialLevelSet;
		else if (use_offset)
		{
			initialLevelSetFilename=fnamesOut[i-1]+"_offset.mha";
			offset(fnamesOut[i-1]+".mha",initialLevelSetFilename);
		}else
			initialLevelSetFilename=fnamesOut[i-1]+".mha";

		cout<<"i: "<<i<<" fname: "<<fnames[i]<<endl;
		cout<<"i: "<<i<<" fname: "<<fnamesOut[i]<<endl;
		segment();

	}

}

#include "itkVector.h"


void GACDemo::overlay(InternalImageType::Pointer imt1, OutputImageType::Pointer imt2, std::string filename)
{
XML_IN;

typedef itk::Vector< unsigned char, 3 > ColorPixelType;
typedef itk::Image< ColorPixelType, 2 > ColorImageType;
typedef itk::ImageRegionIterator<InternalImageType> IteratorType;
typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
typedef itk::ImageRegionIterator<OutputImageType> OutputIteratorType;
typedef IteratorType::IndexType IndexType;
typedef ColorIteratorType::IndexType ColorIndexType;
 typedef  itk::ImageFileWriter<  ColorImageType  > ColorWriterType;


	ColorImageType::Pointer out= ColorImageType::New();

	//imt1
	InternalImageType::RegionType region;
	region=imt1->GetLargestPossibleRegion();
	out->SetRegions( region );
	out->Allocate();

	ColorPixelType v_color;
	v_color[0]=255;
	v_color[1]=0;
	v_color[2]=0;


	//now merge both images
	IteratorType iter1( imt1, imt1->GetRequestedRegion() );
	OutputIteratorType iter2( imt2, imt2->GetRequestedRegion() );
	ColorIteratorType iter3( out, out->GetRequestedRegion() );
	for ( iter1.GoToBegin(), iter2.GoToBegin(), iter3.GoToBegin(); !iter1.IsAtEnd(); ++iter1, ++iter2, ++iter3 )
	{
		InternalPixelType g1;
		OutputPixelType g2;
		ColorPixelType v;
		g1=iter1.Get();
		g2=iter2.Get();
		v[0]=g1;
		v[1]=g1;
		v[2]=g1;
		if(g2>0)
			iter3.Set(v_color);
		else
			iter3.Set(v);
	}

	ColorWriterType::Pointer writer = ColorWriterType::New();
	writer->SetFileName( filename );
	writer->SetInput(out);
	writer->Update();
	XML_OUT;
}

void GACDemo::segment()
{
XML_IN;
	//inputImageFilename=inputImage;
	//outputImageFilename=outputImage;
	//initialLevelSetFilename=initialLevelSet;
  //  Software Guide : BeginLatex
  //
  //  We now define the image type using a particular pixel type and
  //  dimension. In this case the \code{float} type is used for the pixels
  //  due to the requirements of the smoothing filter.
  //
  //  Software Guide : EndLatex

  // Software Guide : BeginCodeSnippet

  // Software Guide : EndCodeSnippet


  //  The following lines instantiate the thresholding filter that will
  //  process the final level set at the output of the
  //  GeodesicActiveContourLevelSetImageFilter.
  //

  typedef itk::BinaryThresholdImageFilter<
                        InternalImageType,
                        OutputImageType    >       ThresholdingFilterType;

  typedef  itk::ImageFileReader< InternalImageType > ReaderType;
  typedef  itk::ImageFileWriter<  OutputImageType  > WriterType;

  ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

  thresholder->SetLowerThreshold( -1000.0 );
  thresholder->SetUpperThreshold(     0.0 );

  thresholder->SetOutsideValue(  0  );
  thresholder->SetInsideValue(  255 );


  // We instantiate reader and writer types in the following lines.
  //


  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();


  cout<<"reader filename: "<<inputImageFilename <<endl;
  reader->SetFileName( inputImageFilename );
  writer->SetFileName( outputImageFilename+".th.png" );


  //  The RescaleIntensityImageFilter type is declared below. This filter will
  //  renormalize image before sending them to writers.
  //
  typedef itk::RescaleIntensityImageFilter<
                               InternalImageType,
                               OutputImageType >   CastFilterType;


  //  The \doxygen{CurvatureAnisotropicDiffusionImageFilter} type is
  //  instantiated using the internal image type.
  //
  typedef   itk::CurvatureAnisotropicDiffusionImageFilter<
                               InternalImageType,
                               InternalImageType >  SmoothingFilterType;

  SmoothingFilterType::Pointer smoothing = SmoothingFilterType::New();


  //  The types of the
  //  GradientMagnitudeRecursiveGaussianImageFilter and
  //  SigmoidImageFilter are instantiated using the internal image
  //  type.
  //
  typedef   itk::GradientMagnitudeRecursiveGaussianImageFilter<
                               InternalImageType,
                               InternalImageType >  GradientFilterType;
  typedef   itk::SigmoidImageFilter<
                               InternalImageType,
                               InternalImageType >  SigmoidFilterType;

  GradientFilterType::Pointer  gradientMagnitude = GradientFilterType::New();

  SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();


  //  The minimum and maximum values of the SigmoidImageFilter output
  //  are defined with the methods \code{SetOutputMinimum()} and
  //  \code{SetOutputMaximum()}. In our case, we want these two values to be
  //  $0.0$ and $1.0$ respectively in order to get a nice speed image to feed
  //  the \code{FastMarchingImageFilter}. Additional details on the user of the
  //  \doxygen{SigmoidImageFilter} are presented in
  //  section~\ref{sec:IntensityNonLinearMapping}.

  sigmoid->SetOutputMinimum(  0.0  );
  sigmoid->SetOutputMaximum(  1.0  );


  //  We declare now the type of the FastMarchingImageFilter that
  //  will be used to generate the initial level set in the form of a distance
  //  map.
  //
  typedef  itk::FastMarchingImageFilter<
                              InternalImageType,
                              InternalImageType >    FastMarchingFilterType;


  //  Next we construct one filter of this class using the \code{New()}
  //  method.
  //
  FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();

  //  Software Guide : BeginLatex
  //
  //  In the following lines we instantiate the type of the
  //  GeodesicActiveContourLevelSetImageFilter and create an object of this
  //  type using the \code{New()} method.
  //
  //  Software Guide : EndLatex

  // Software Guide : BeginCodeSnippet
  typedef  itk::GeodesicActiveContourLevelSetImageFilter< InternalImageType,
                InternalImageType >    GeodesicActiveContourFilterType;
  GeodesicActiveContourFilterType::Pointer geodesicActiveContour =
                                     GeodesicActiveContourFilterType::New();
  // Software Guide : EndCodeSnippet


  //  Software Guide : BeginLatex
  //
  //  For the GeodesicActiveContourLevelSetImageFilter, scaling parameters
  //  are used to trade off between the propagation (inflation), the
  //  curvature (smoothing) and the advection terms. These parameters are set
  //  using methods \code{SetPropagationScaling()},
  //  \code{SetCurvatureScaling()} and \code{SetAdvectionScaling()}. In this
  //  example, we will set the curvature and advection scales to one and let
  //  the propagation scale be a command-line argument.
  //
  //  \index{itk::Geodesic\-Active\-Contour\-LevelSet\-Image\-Filter!SetPropagationScaling()}
  //  \index{itk::Segmentation\-Level\-Set\-Image\-Filter!SetPropagationScaling()}
  //  \index{itk::Geodesic\-Active\-Contour\-LevelSet\-Image\-Filter!SetCurvatureScaling()}
  //  \index{itk::Segmentation\-Level\-Set\-Image\-Filter!SetCurvatureScaling()}
  //  \index{itk::Geodesic\-Active\-Contour\-LevelSet\-Image\-Filter!SetAdvectionScaling()}
  //  \index{itk::Segmentation\-Level\-Set\-Image\-Filter!SetAdvectionScaling()}
  //
  //  Software Guide : EndLatex



  //  Software Guide : BeginCodeSnippet
  geodesicActiveContour->SetPropagationScaling( propagationScaling );
  geodesicActiveContour->SetCurvatureScaling( 1.0 );
  geodesicActiveContour->SetAdvectionScaling( 1.0 );
  //  Software Guide : EndCodeSnippet

  //  Once activiated the level set evolution will stop if the convergence
  //  criteria or if the maximum number of iterations is reached.  The
  //  convergence criteria is defined in terms of the root mean squared (RMS)
  //  change in the level set function. The evolution is said to have
  //  converged if the RMS change is below a user specified threshold.  In a
  //  real application is desirable to couple the evolution of the zero set
  //  to a visualization module allowing the user to follow the evolution of
  //  the zero set. With this feedback, the user may decide when to stop the
  //  algorithm before the zero set leaks through the regions of low gradient
  //  in the contour of the anatomical structure to be segmented.

  geodesicActiveContour->SetMaximumRMSError( max_rms );
  geodesicActiveContour->SetNumberOfIterations( max_iter );


  //  Software Guide : BeginLatex
  //
  //  The filters are now connected in a pipeline indicated in
  //  Figure~\ref{fig:GeodesicActiveContoursCollaborationDiagram} using the
  //  following lines:
  //
  //  Software Guide : EndLatex

  // Software Guide : BeginCodeSnippet
  smoothing->SetInput( reader->GetOutput() );
  gradientMagnitude->SetInput( smoothing->GetOutput() );
  sigmoid->SetInput( gradientMagnitude->GetOutput() );

  ReaderType::Pointer readerDistance=ReaderType::New();
  cout<<"readerDistance filename: "<< initialLevelSetFilename<<endl;
  readerDistance->SetFileName(initialLevelSetFilename);
  typedef itk::ShiftScaleImageFilter<InternalImageType,InternalImageType> ScalerType;
  ScalerType::Pointer scaler=ScalerType::New();

  scaler->SetScale( 1.0 );
  scaler->SetShift( 0 );

  InternalWriterType::Pointer mapWriter2 = InternalWriterType::New();


   if(use_initial_level_set)
   {

  	if(use_tracking && step==0)
  	{
  		scaler->SetInput( readerDistance->GetOutput() );
  		geodesicActiveContour->SetInput( scaler->GetOutput() );


  	}else
  	{
  		geodesicActiveContour->SetInput(  readerDistance->GetOutput() );

  	}
   }else
   {
	   if(use_tracking && step==0)
	     	{
	     		geodesicActiveContour->SetInput( fastMarching->GetOutput() );
	     	}else
	     	{
	     		scaler->SetInput( readerDistance->GetOutput());
				geodesicActiveContour->SetInput( scaler->GetOutput() );

				mapWriter2->SetInput( scaler->GetOutput() );
				mapWriter2->SetFileName("GeodesicActiveContourImageFilterOutput7.mha");
				MaxType::Pointer max=MaxType::New();
				max->SetImage(scaler->GetOutput());
				max->ComputeMaximum();
				max->ComputeMinimum();
				mapWriter2->Update();
				cout<<"max: " << max->GetMaximum()<< "min: "<<max->GetMinimum()<<endl;

	     	}

	     		geodesicActiveContour->SetInput(  readerDistance->GetOutput() );
   }


  geodesicActiveContour->SetFeatureImage( sigmoid->GetOutput() );

  thresholder->SetInput( geodesicActiveContour->GetOutput() );
  writer->SetInput( thresholder->GetOutput() );

  typedef CommandIterationUpdate<GeodesicActiveContourFilterType> CommandType;
  CommandType::Pointer observer = CommandType::New();
  geodesicActiveContour->AddObserver( itk::IterationEvent(), observer );


  // Software Guide : EndCodeSnippet


  //  The CurvatureAnisotropicDiffusionImageFilter requires a couple of
  //  parameter to be defined. The following are typical values for $2D$
  //  images. However they may have to be adjusted depending on the amount of
  //  noise present in the input image. This filter has been discussed in
  //  section~\ref{sec:GradientAnisotropicDiffusionImageFilter}.

  smoothing->SetTimeStep( 0.125 );
  smoothing->SetNumberOfIterations(  5 );
  smoothing->SetConductanceParameter( 9.0 );


  //  The GradientMagnitudeRecursiveGaussianImageFilter performs the
  //  equivalent of a convolution with a Gaussian kernel, followed by a
  //  derivative operator. The sigma of this Gaussian can be used to control
  //  the range of influence of the image edges. This filter has been discussed
  //  in Section~\ref{sec:GradientMagnitudeRecursiveGaussianImageFilter}



  gradientMagnitude->SetSigma(  sigma  );


  //  The SigmoidImageFilter requires two parameters that define the linear
  //  transformation to be applied to the sigmoid argument. This parameters
  //  have been discussed in Sections~\ref{sec:IntensityNonLinearMapping} and
  //  \ref{sec:FastMarchingImageFilter}.



  sigmoid->SetAlpha( alpha );
  sigmoid->SetBeta(  beta  );


  //  The FastMarchingImageFilter requires the user to provide a seed
  //  point from which the level set will be generated. The user can actually
  //  pass not only one seed point but a set of them. Note the the
  //  FastMarchingImageFilter is used here only as a helper in the
  //  determination of an initial level set. We could have used the
  //  \doxygen{DanielssonDistanceMapImageFilter} in the same way.
  //
  //  The seeds are passed stored in a container. The type of this
  //  container is defined as \code{NodeContainer} among the
  //  FastMarchingImageFilter traits.
  //
  typedef FastMarchingFilterType::NodeContainer  NodeContainer;
  typedef FastMarchingFilterType::NodeType       NodeType;

  NodeContainer::Pointer seeds = NodeContainer::New();





  //  Nodes are created as stack variables and initialized with a value and an
  //  \doxygen{Index} position. Note that here we assign the value of minus the
  //  user-provided distance to the unique node of the seeds passed to the
  //  FastMarchingImageFilter. In this way, the value will increment
  //  as the front is propagated, until it reaches the zero value corresponding
  //  to the contour. After this, the front will continue propagating until it
  //  fills up the entire image. The initial distance is taken here from the
  //  command line arguments. The rule of thumb for the user is to select this
  //  value as the distance from the seed points at which she want the initial
  //  contour to be.


  NodeType node;

  const double seedValue = - initialDistance;

  node.SetValue( seedValue );
  node.SetIndex( seedPosition );


  //  The list of nodes is initialized and then every node is inserted using
  //  the \code{InsertElement()}.

  seeds->Initialize();
  seeds->InsertElement( 0, node );


  //  The set of seed nodes is passed now to the
  //  FastMarchingImageFilter with the method
  //  \code{SetTrialPoints()}.
  //
  fastMarching->SetTrialPoints(  seeds  );


  //  Since the FastMarchingImageFilter is used here just as a
  //  Distance Map generator. It does not require a speed image as input.
  //  Instead the constant value $1.0$ is passed using the
  //  \code{SetSpeedConstant()} method.
  //
  fastMarching->SetSpeedConstant( 1.0 );


  //  Here we configure all the writers required to see the intermediate
  //  outputs of the pipeline. This is added here only for
  //  pedagogical/debugging purposes. These intermediate output are normaly not
  //  required. Only the output of the final thresholding filter should be
  //  relevant.  Observing intermediate output is helpful in the process of
  //  fine tuning the parameters of filters in the pipeline.
  //
  CastFilterType::Pointer caster1 = CastFilterType::New();
  CastFilterType::Pointer caster2 = CastFilterType::New();
  CastFilterType::Pointer caster3 = CastFilterType::New();
  CastFilterType::Pointer caster4 = CastFilterType::New();

  WriterType::Pointer writer1 = WriterType::New();
  WriterType::Pointer writer2 = WriterType::New();
  WriterType::Pointer writer3 = WriterType::New();
  WriterType::Pointer writer4 = WriterType::New();

  caster1->SetInput( smoothing->GetOutput() );
  writer1->SetInput( caster1->GetOutput() );
  writer1->SetFileName("debug/GeodesicActiveContourImageFilterOutput1.png");
  caster1->SetOutputMinimum(   0 );
  caster1->SetOutputMaximum( 255 );
  writer1->Update();

  caster2->SetInput( gradientMagnitude->GetOutput() );
  writer2->SetInput( caster2->GetOutput() );
  writer2->SetFileName("debug/GeodesicActiveContourImageFilterOutput2.png");
  caster2->SetOutputMinimum(   0 );
  caster2->SetOutputMaximum( 255 );
  writer2->Update();

  caster3->SetInput( sigmoid->GetOutput() );
  writer3->SetInput( caster3->GetOutput() );
  writer3->SetFileName("debug/GeodesicActiveContourImageFilterOutput3.png");
  caster3->SetOutputMinimum(   0 );
  caster3->SetOutputMaximum( 255 );
  writer3->Update();

  caster4->SetInput( fastMarching->GetOutput() );
  writer4->SetInput( caster4->GetOutput() );
  writer4->SetFileName("debug/GeodesicActiveContourImageFilterOutput4.png");
  caster4->SetOutputMinimum(   0 );
  caster4->SetOutputMaximum( 255 );


  //  The FastMarchingImageFilter requires the user to specify the
  //  size of the image to be produced as output. This is done using the
  //  \code{SetOutputSize()}. Note that the size is obtained here from the
  //  output image of the smoothing filter. The size of this image is valid
  //  only after the \code{Update()} methods of this filter has been called
  //  directly or indirectly.
  //
  fastMarching->SetOutputSize(
           reader->GetOutput()->GetBufferedRegion().GetSize() );


  //  Software Guide : BeginLatex
  //
  //  The invocation of the \code{Update()} method on the writer triggers the
  //  execution of the pipeline.  As usual, the call is placed in a
  //  \code{try/catch} block should any errors occur or exceptions be thrown.
  //
  //  Software Guide : EndLatex

  // Software Guide : BeginCodeSnippet
  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  // Software Guide : EndCodeSnippet

  // Print out some useful information
  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << geodesicActiveContour->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << geodesicActiveContour->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elpased iterations: " << geodesicActiveContour->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << geodesicActiveContour->GetRMSChange() << std::endl;

  writer4->Update();


  // The following writer type is used to save the output of the time-crossing
  // map in a file with apropiate pixel representation. The advantage of saving
  // this image in native format is that it can be used with a viewer to help
  // determine an appropriate threshold to be used on the output of the
  // fastmarching filter.
  //


  InternalWriterType::Pointer mapWriter = InternalWriterType::New();
  mapWriter->SetInput( fastMarching->GetOutput() );
  mapWriter->SetFileName("GeodesicActiveContourImageFilterOutput4.mha");
  mapWriter->Update();


  InternalWriterType::Pointer speedWriter = InternalWriterType::New();
  speedWriter->SetInput( sigmoid->GetOutput() );
  speedWriter->SetFileName("GeodesicActiveContourImageFilterOutput3.mha");


  InternalWriterType::Pointer gradientWriter = InternalWriterType::New();
  gradientWriter->SetInput( gradientMagnitude->GetOutput() );
  gradientWriter->SetFileName("GeodesicActiveContourImageFilterOutput2.mha");
  

  WriterType::Pointer writer6 = WriterType::New();
  typedef itk::ZeroCrossingImageFilter<InternalImageType, OutputImageType > ZcrFilterType;
  	ZcrFilterType::Pointer zcr = ZcrFilterType::New();
  	writer6->SetFileName(outputImageFilename+".png");
  	zcr->SetForegroundValue(255);
  	zcr->SetBackgroundValue(0);
  zcr->SetInput(geodesicActiveContour->GetOutput());
  writer6->SetInput(zcr->GetOutput());
  writer6->Update();

  InternalWriterType::Pointer writer7 = InternalWriterType::New();
  cout<<"writing: "<<outputImageFilename+".mha"<<endl;
  writer7->SetFileName(outputImageFilename+".mha");
  writer7->SetInput(geodesicActiveContour->GetOutput());
  writer7->Update();

  overlay(reader->GetOutput(),zcr->GetOutput(),outputImageFilename+"_overlay.png");

  try
    {


	  speedWriter->Update();
	  gradientWriter->Update();


    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  //  Software Guide : BeginLatex
  //
  //  Let's now run this example using as input the image
  //  \code{BrainProtonDensitySlice.png} provided in the directory
  //  \code{Examples/Data}. We can easily segment the major anatomical
  //  structures by providing seeds in the appropriate locations.
  //  Table~\ref{tab:GeodesicActiveContourImageFilterOutput2} presents the
  //  parameters used for some structures.
  //
  //  \begin{table}
  //  \begin{center}
  //  \begin{tabular}{|l|c|c|c|c|c|c|c|c|}
  //  \hline
  //  Structure    & Seed Index &  Distance   &   $\sigma$  &
  //  $\alpha$     &  $\beta$   & Propag. & Output Image \\  \hline
  //  Left Ventricle  & $(81,114)$ & 5.0 & 1.0 & -0.5 & 3.0  &  2.0 & First   \\  \hline
  //  Right Ventricle & $(99,114)$ & 5.0 & 1.0 & -0.5 & 3.0  &  2.0 & Second  \\  \hline
  //  White matter    & $(56, 92)$ & 5.0 & 1.0 & -0.3 & 2.0  & 10.0 & Third   \\  \hline
  //  Gray matter     & $(40, 90)$ & 5.0 & 0.5 & -0.3 & 2.0  & 10.0 & Fourth  \\  \hline
  //  \end{tabular}
  //  \end{center}
  //  \itkcaption[GeodesicActiveContour segmentation example parameters]{Parameters used
  //  for segmenting some brain structures shown in
  //  Figure~\ref{fig:GeodesicActiveContourImageFilterOutput2} using the filter
  //  GeodesicActiveContourLevelSetImageFilter.
  //  \label{tab:GeodesicActiveContourImageFilterOutput2}}
  //  \end{table}
  //
  //  Figure~\ref{fig:GeodesicActiveContourImageFilterOutput} presents the
  //  intermediate outputs of the pipeline illustrated in
  //  Figure~\ref{fig:GeodesicActiveContoursCollaborationDiagram}. They are
  //  from left to right: the output of the anisotropic diffusion filter, the
  //  gradient magnitude of the smoothed image and the sigmoid of the gradient
  //  magnitude which is finally used as the edge potential for the
  //  GeodesicActiveContourLevelSetImageFilter.
  //
  // \begin{figure} \center
  // \includegraphics[height=0.40\textheight]{BrainProtonDensitySlice.eps}
  // \includegraphics[height=0.40\textheight]{GeodesicActiveContourImageFilterOutput1.eps}
  // \includegraphics[height=0.40\textheight]{GeodesicActiveContourImageFilterOutput2.eps}
  // \includegraphics[height=0.40\textheight]{GeodesicActiveContourImageFilterOutput3.eps}
  // \itkcaption[GeodesicActiveContourLevelSetImageFilter intermediate
  // output]{Images generated by the segmentation process based on the
  // GeodesicActiveContourLevelSetImageFilter. From left to right and top to
  // bottom: input image to be segmented, image smoothed with an
  // edge-preserving smoothing filter, gradient magnitude of the smoothed
  // image, sigmoid of the gradient magnitude. This last image, the sigmoid, is
  // used to compute the speed term for the front propagation.}
  // \label{fig:GeodesicActiveContourImageFilterOutput} \end{figure}
  //
  //  Segmentations of the main brain structures are presented in
  //  Figure~\ref{fig:GeodesicActiveContourImageFilterOutput2}. The results
  //  are quite similar to those obtained with the
  //  ShapeDetectionLevelSetImageFilter in
  //  Section~\ref{sec:ShapeDetectionLevelSetFilter}.
  //
  //  Note that a relatively larger propagation scaling value was required to
  //  segment the white matter. This is due to two factors: the lower
  //  contrast at the border of the white matter and the complex shape of the
  //  structure. Unfortunately the optimal value of these scaling parameters
  //  can only be determined by experimentation. In a real application we
  //  could imagine an interactive mechanism by which a user supervises the
  //  contour evolution and adjusts these parameters accordingly.
  //
  // \begin{figure} \center
  // \includegraphics[width=0.24\textwidth]{GeodesicActiveContourImageFilterOutput5.eps}
  // \includegraphics[width=0.24\textwidth]{GeodesicActiveContourImageFilterOutput6.eps}
  // \includegraphics[width=0.24\textwidth]{GeodesicActiveContourImageFilterOutput7.eps}
  // \includegraphics[width=0.24\textwidth]{GeodesicActiveContourImageFilterOutput8.eps}
  // \itkcaption[GeodesicActiveContourImageFilter segmentations]{Images generated by the
  // segmentation process based on the GeodesicActiveContourImageFilter. From left to
  // right: segmentation of the left ventricle, segmentation of the right
  // ventricle, segmentation of the white matter, attempt of segmentation of
  // the gray matter.}
  // \label{fig:GeodesicActiveContourImageFilterOutput2}
  // \end{figure}
  //
  //  Software Guide : EndLatex

  XML_OUT;
}



void GACDemo::offset(std::string in,std::string out)
{
	XML_IN;


  // We instantiate reader and writer types in the following lines.
  //
  typedef  itk::ImageFileReader< InternalImageType > ReaderType;
  typedef  itk::ImageFileWriter<  InternalImageType  > WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();


  cout<<"reader filename: "<<in <<endl;
  reader->SetFileName( in );
  writer->SetFileName( out);


  //  We declare now the type of the FastMarchingImageFilter that
  //  will be used to generate the initial level set in the form of a distance
  //  map.
  //
  typedef  itk::FastMarchingImageFilter<
                              InternalImageType,
                              InternalImageType >    FastMarchingFilterType;


  //  Next we construct one filter of this class using the \code{New()}
  //  method.
  //
  FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();


  typedef itk::ZeroCrossingImageFilter<InternalImageType,InternalImageType> ZcrFilterType;

  	ZcrFilterType::Pointer zcr = ZcrFilterType::New();

  	zcr->SetForegroundValue(255);
  	zcr->SetBackgroundValue(0);

   typedef itk::ShiftScaleImageFilter<InternalImageType,InternalImageType> ScalerType;
   ScalerType::Pointer scaler=ScalerType::New();

   scaler->SetScale( 1.0 );
   scaler->SetShift( initialDistance );


   typedef itk::BinaryThresholdImageFilter<
                           InternalImageType,
                           InternalImageType    >       ThresholdingFilterType;

     ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

     thresholder->SetLowerThreshold( -1000.0 );
     thresholder->SetUpperThreshold(     0.0 );

     thresholder->SetOutsideValue(  0  );
     thresholder->SetInsideValue(  255 );

     typedef itk::SignedDanielssonDistanceMapImageFilter<
   		  InternalImageType,
   		  InternalImageType >  DistanceFilterType;

     DistanceFilterType::Pointer danielsson = DistanceFilterType::New();

	 thresholder->SetInput(reader->GetOutput());
	 danielsson->SetInput(thresholder->GetOutput());
	 scaler->SetInput(danielsson->GetOutput());
	 writer->SetInput(scaler->GetOutput());

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  XML_OUT;
}
