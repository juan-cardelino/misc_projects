/*
 * GeodesicActiveContourImageFilterInteractiveTest.cxx
 *
 *  Created on: Jun 21, 2011
 *      Author: juan
 */

#include "util/util.h"

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkLevelSetSegmentationCommon.h"
#include "GeodesicActiveContourImageFilterInteractive.h"

int main( int argc, const char *argv[] )
{
	util::infolog.date();
	std::string svn_version="svn_version: $Id: GeodesicActiveContourImageFilterInteractiveTestParametros.cxx 379 2012-01-24 19:44:21Z juanc $";
	util::infolog.logln(svn_version);
	util::Logger::setLogLevel(util::Logger::INFO);
	//	cout << 	setprecision(5);
	util::infolog.logln("Starting algorithm");
	util::infolog.begin("GeodesicActiveContourImageFilterInteractive");
	GACDemo demo;


	try {
		//
		// get core configuration parameters
		//
#ifdef GAC_USE_CONF
		util::Configuration cfg = demo.getConfiguration();
		//
		// create a configuration manager to read and write the configuration
		//
		util::ConfManager conf_manager(cfg);
		//
		// read config. from command line
		//
		cfg = conf_manager.parseConf(argc,argv);
		//
		// configure the algorithm
		//
		demo.configure(cfg);
#endif
		//and finally run the algorithm
		demo.run();
		util::infolog.end();
	} catch (base::Exception ex) {
		std::cout << "EPA! " << ex.getMessage() << std::endl;
	}

	return 0;
}
