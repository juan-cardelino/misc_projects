/*
 * itkLevelSetSegmentationCommon.cxx
 *
 *  Created on: 30/03/2011
 *      Author: juan
 */
#include "itkLevelSetSegmentationCommon.h"
#include <itkMaximumImageFilter.h>
#include <itkMinimumImageFilter.h>

void overlay(InternalImageType::Pointer imt1, OutputImageType::Pointer imt2, std::string filename)
{
XML_IN;

typedef itk::Vector< unsigned char, 3 > ColorPixelType;
typedef itk::Image< ColorPixelType, 2 > ColorImageType;
typedef itk::ImageRegionIterator<InternalImageType> IteratorType;
typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
typedef itk::ImageRegionIterator<OutputImageType> OutputIteratorType;
typedef IteratorType::IndexType IndexType;
typedef ColorIteratorType::IndexType ColorIndexType;
typedef  itk::ImageFileWriter<  ColorImageType  > ColorWriterType;


	ColorImageType::Pointer out= ColorImageType::New();

	//imt1
	InternalImageType::RegionType region;
	region=imt1->GetLargestPossibleRegion();
	out->SetRegions( region );
	out->Allocate();

	ColorPixelType v_color;
	v_color[0]=255;
	v_color[1]=0;
	v_color[2]=0;


	//now merge both images
	IteratorType iter1( imt1, imt1->GetRequestedRegion() );
	OutputIteratorType iter2( imt2, imt2->GetRequestedRegion() );
	ColorIteratorType iter3( out, out->GetRequestedRegion() );
	for ( iter1.GoToBegin(), iter2.GoToBegin(), iter3.GoToBegin(); !iter1.IsAtEnd(); ++iter1, ++iter2, ++iter3 )
	{
		InternalPixelType g1;
		OutputPixelType g2;
		ColorPixelType v;
		g1=iter1.Get();
		g2=iter2.Get();
		v[0]=g1;
		v[1]=g1;
		v[2]=g1;
		if(g2>0)
			iter3.Set(v_color);
		else
			iter3.Set(v);
	}

	ColorWriterType::Pointer writer = ColorWriterType::New();
	writer->SetFileName( filename );
	writer->SetInput(out);
	writer->Update();
	XML_OUT;
}

void offset(std::string in,std::string out, double ls_offset)
{
	XML_IN;


  // We instantiate reader and writer types in the following lines.
  //
  typedef  itk::ImageFileReader< InternalImageType > ReaderType;
  typedef  itk::ImageFileWriter<  InternalImageType  > WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();


  std::cout<<"reader filename: "<<in <<std::endl;
  reader->SetFileName( in );
  writer->SetFileName( out);


  //  We declare now the type of the FastMarchingImageFilter that
  //  will be used to generate the initial level set in the form of a distance
  //  map.
  //
  typedef  itk::FastMarchingImageFilter<
                              InternalImageType,
                              InternalImageType >    FastMarchingFilterType;


  //  Next we construct one filter of this class using the \code{New()}
  //  method.
  //
  FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();


  typedef itk::ZeroCrossingImageFilter<InternalImageType,InternalImageType> ZcrFilterType;

  	ZcrFilterType::Pointer zcr = ZcrFilterType::New();

  	zcr->SetForegroundValue(255);
  	zcr->SetBackgroundValue(0);

   typedef itk::ShiftScaleImageFilter<InternalImageType,InternalImageType> ScalerType;
   ScalerType::Pointer scaler=ScalerType::New();

   scaler->SetScale( 1.0 );
   scaler->SetShift( ls_offset );


   typedef itk::BinaryThresholdImageFilter<
                           InternalImageType,
                           InternalImageType    >       ThresholdingFilterType;

     ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

     thresholder->SetLowerThreshold( -1000.0 );
     thresholder->SetUpperThreshold(     0.0 );

     thresholder->SetOutsideValue(  0  );
     thresholder->SetInsideValue(  255 );

     typedef itk::SignedDanielssonDistanceMapImageFilter<
   		  InternalImageType,
   		  InternalImageType >  DistanceFilterType;

     DistanceFilterType::Pointer danielsson = DistanceFilterType::New();

	 thresholder->SetInput(reader->GetOutput());
	 danielsson->SetInput(thresholder->GetOutput());
	 scaler->SetInput(danielsson->GetOutput());
	 writer->SetInput(scaler->GetOutput());

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  XML_OUT;
}

NodeContainer::Pointer read_seeds(std::string in, const double seedValue)
{
	XML_IN;

	NodeContainer::Pointer seeds = NodeContainer::New();
	seeds->Initialize();

	int nb_curves;
	int nb_poses;
#define MAX_SEEDS 100

	float poses[MAX_SEEDS][2];

	read_polyline(in,&nb_curves,&nb_poses, poses);
	printf("seeds read: %3d\n",nb_poses);
	if(nb_poses>=MAX_SEEDS) exit(1);
	InternalImageType::IndexType  seedPosition;

	for(int i=0;i<nb_poses;i++)
	{
		NodeType node;

		seedPosition[0]=poses[i][0];
		seedPosition[1]=poses[i][1];

		std::cout<<"i: "<<i<<" pos:"<<seedPosition<<std::endl;

		node.SetValue( seedValue );
		node.SetIndex( seedPosition );

		seeds->InsertElement( i, node );
	}

XML_OUT;
	return seeds;
}

NodeContainer::Pointer convert_coordinates(float points[][2],int nb_points, const double seedValue)
{
	XML_IN;

	NodeContainer::Pointer seeds = NodeContainer::New();
	seeds->Initialize();


	InternalImageType::IndexType  seedPosition;

	for(int i=0;i<nb_points;i++)
	{
		NodeType node;

		seedPosition[0]=points[i][0];
		seedPosition[1]=points[i][1];

		std::cout<<"i: "<<i<<" pos:"<<seedPosition<<std::endl;

		node.SetValue( seedValue );
		node.SetIndex( seedPosition );

		seeds->InsertElement( i, node );
	}

	XML_OUT;
	return seeds;
}


void read_coords(FILE* fid, float *v)
{
	fscanf(fid," %f %f",v,v+1);
}

void read_polyline(std::string filename, int *m, int *n, float poses[][2])
{
	XML_IN;

	printf("opening: %s\n",filename.c_str());
	FILE* fid=fopen(filename.c_str(),"r");
	fscanf(fid,"%d\n",m);	//number of curves
	printf("m: %d\n",*m);	
	fscanf(fid,"%d\n",n);	//number of points of the curve
	printf("n: %d\n",*n);	

	for(int i=0;i<*n;i++)
	{
		read_coords(fid,poses[i]);
		print_coords_f_n(poses[i],"coord",2);
	}
	fclose(fid);

	XML_OUT;
}

void print_coords_f_n(float *v, const char * name, int n)
{

	printf("%s: (",name);

	for(int i=0;i<n;i++)
	{
		printf(" %5.2f",v[i]);
	}
	printf(" )\n");

}


InternalImageType::Pointer levelset_union( InternalImageType::Pointer im1, InternalImageType::Pointer im2)
{
	 typedef itk::MaximumImageFilter< InternalImageType, InternalImageType, InternalImageType > MaxFilterType;

	 MaxFilterType::Pointer maxFilter=MaxFilterType::New();
	 maxFilter->SetInput(0,im1);
	 maxFilter->SetInput(1,im2);

	 InternalImageType::Pointer out;
	 out=maxFilter->GetOutput();
	 maxFilter->Update();
	 return out;
}


InternalImageType::Pointer levelset_intersection( InternalImageType::Pointer im1, InternalImageType::Pointer im2)
{
	 typedef itk::MinimumImageFilter< InternalImageType, InternalImageType, InternalImageType > MaxFilterType;

	 MaxFilterType::Pointer maxFilter=MaxFilterType::New();
	 maxFilter->SetInput(0,im1);
	 maxFilter->SetInput(1,im2);

	 InternalImageType::Pointer out;
	 out=maxFilter->GetOutput();
	 maxFilter->Update();
	 return out;
}






PathType::Pointer implicit_to_curve( InternalImageType::Pointer im)
{
	XML_IN;

	int verbose=1;

	ExtractorType::Pointer extractor = ExtractorType::New();

	extractor->SetInput(im);
	extractor->SetContourValue(0.0);
	extractor->VertexConnectHighPixelsOff();
	extractor->ReverseContourOrientationOff();
	extractor->Update();

	// Run and output data to data object
	PathType::Pointer path;
	path = extractor->GetOutput();
	if(verbose) path->Print(std::cout);
	PathType::IndexType index;

	// Create iterator following path through image
	// this is optional and could be ignored safely
	if(verbose)
	{
	IteratorType iter( im, path );
	int i=0;
	while (!iter.IsAtEnd())
	{
		index=iter.GetIndex();
		std::cout <<"id: "<<i++<<" path pos: "<< iter.GetPathPosition() << "\t im pos: "<< index << " im pos a mano: "<<index[0]<<","<<index[1]<< std::endl;

		try
		{
			++iter;
		}
		catch( itk::ExceptionObject & err )
		{
			std::cout << "ExceptionObject caught !" << std::endl;
			std::cout << err << std::endl;
			return NULL;
		}

	}
	}

	XML_OUT;
	return path;
}



#include "itkPolygonSpatialObject.h"
#include "itkSpatialObjectToImageFilter.h"

OutputImageType::Pointer points_to_mask( OutputImageType::SizeType size, float points[][2], const unsigned int numberOfPoints  )
{
  typedef itk::PolygonSpatialObject< Dimension >  PolygonType;

  // Software Guide : BeginCodeSnippet
  typedef itk::SpatialObjectToImageFilter<
    PolygonType, OutputImageType >   SpatialObjectToImageFilterType;

  SpatialObjectToImageFilterType::Pointer imageFilter =
    SpatialObjectToImageFilterType::New();
  
  imageFilter->SetSize( size );
  // Software Guide : EndCodeSnippet

  // Software Guide : BeginCodeSnippet
  InternalImageType::SpacingType spacing;
  spacing[0] =  1.0 ;
  spacing[1] =  1.0 ;

  imageFilter->SetSpacing( spacing );
  PolygonType::Pointer polygon = PolygonType::New();
  
  PolygonType::PointType coord;
  
  for( unsigned int i=0; i < numberOfPoints; i++ )
    {
    coord[0] = points[i][0];
	coord[1] = points[i][1];
    polygon->AddPoint( coord );
    }
  // Software Guide : EndCodeSnippet

  std::cout << "Polygon Perimeter = " << polygon->MeasurePerimeter() << std::endl;
  std::cout << "Polygon Area      = " << polygon->MeasureArea() << std::endl;

  //  Software Guide : BeginLatex
  //
  //  We connect the polygon as the input to the SpatialObjectToImageFilter.
  //
  //  Software Guide : EndLatex

  // Software Guide : BeginCodeSnippet
  imageFilter->SetInput(  polygon  );
  // Software Guide : EndCodeSnippet

  polygon->SetDefaultInsideValue(255);

  polygon->SetDefaultOutsideValue(0);

   imageFilter->SetUseObjectValue( true );

   imageFilter->SetOutsideValue( 0 );

  try{
    imageFilter->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    std::cerr << excp << std::endl;
    return NULL;
    }
  // Software Guide : EndCodeSnippet


  return imageFilter->GetOutput();
}

#include "itkImage.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkLabelObject.h"
#include "itkShapeLabelObject.h"
#include "itkLabelMap.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkShapeLabelMapFilter.h"


std::vector<PathType::Pointer> segmentation_output_to_curves(InternalImageType::Pointer im_dist )
{
#define GAC_WRITE 0

	const   unsigned int        Dimension = 2;
	typedef float    PixelType;
	typedef unsigned char    OutPixelType;
	typedef itk::Image< PixelType, Dimension >    ImageType;
	typedef itk::Image< OutPixelType, Dimension >    OutImageType;

	typedef  itk::ImageFileReader< ImageType > ReaderType;
	typedef  itk::ImageFileWriter< OutImageType  > WriterType;


	//generate a mask
	ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
	thresholder->SetLowerThreshold( -1000.0 );
	thresholder->SetUpperThreshold(     0.0 );
	thresholder->SetOutsideValue(  0  );
	thresholder->SetInsideValue(  255 );
	thresholder->SetInput( im_dist );

#if GAC_WRITE
	//write mask down (for debug only)
	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName( "debug_mask.png" );
	writer->SetInput( thresholder->GetOutput() );
	writer->Update();
#endif

	//convert to label map
	 typedef unsigned char BinaryPixelType;
	  typedef unsigned char LabelPixelType;


	  //typedef itk::LabelObject< LabelPixelType, Dimension >   LabelObjectType;

	  typedef itk::ShapeLabelObject< LabelPixelType, Dimension > LabelObjectType;

	  typedef itk::LabelMap< LabelObjectType >                LabelMapType;


	  typedef itk::BinaryImageToLabelMapFilter< OutImageType, LabelMapType> I2LType;
	  I2LType::Pointer i2l = I2LType::New();
	  // test the behavior without input

	  i2l->SetFullyConnected( 0 );
	  i2l->SetInputForegroundValue( 255 );
	  i2l->SetOutputBackgroundValue( 0 );


	  typedef itk::LabelMapToLabelImageFilter< LabelMapType, OutImageType> L2IType;
	  L2IType::Pointer l2i = L2IType::New();

	  i2l->SetInput( thresholder->GetOutput() );
	  l2i->SetInput( i2l->GetOutput() );

#if GAC_WRITE
	  WriterType::Pointer writer2 = WriterType::New();
	  writer2->SetFileName( "debug_label_map.png" );
	  writer2->UseCompressionOn();
	  writer2->SetInput( l2i->GetOutput() );
	  writer2->Update();
#endif

	    std::cout<<"nb: "<<i2l->GetOutput()->GetNumberOfLabelObjects()<<std::endl;

	    typedef itk::ShapeLabelMapFilter< LabelMapType > ShapeFilterType;
	    ShapeFilterType::Pointer shape = ShapeFilterType::New();
	    shape->SetInput( i2l->GetOutput() );
	    shape->Update();


	    i2l->GetOutput()->PrintLabelObjects();

	    std::vector<PathType::Pointer> salida;

	    LabelMapType::Pointer labelMap = i2l->GetOutput();
	    for( unsigned int label=1; label<=labelMap->GetNumberOfLabelObjects(); label++ )
	    {
	    	// we don’t need a SmartPointer of the label object here, because the reference is kept in
	    	// in the label map.
	    	const LabelObjectType * labelObject = labelMap->GetLabelObject( label );
	    	std::cout << label << "\t" << labelObject->GetPhysicalSize() << "\t" << labelObject->GetCentroid()<<std::endl;
	    	//std::cout << *labelObject<<std::endl;
	    	//labelObject->Print( std::cout );



	    //convert to contours
	    typedef itk::ContourExtractor2DImageFilter<OutImageType> ExtractorType;

	    	ExtractorType::Pointer extractor = ExtractorType::New();

	    	extractor->SetInput(l2i->GetOutput());
	    	extractor->SetContourValue(label-0.5);
	    	extractor->VertexConnectHighPixelsOff();
	    	extractor->ReverseContourOrientationOff();
	    	extractor->Update();

	    	// Run and output data to data object
	    	typedef ExtractorType::OutputPathType PathType;
	    	PathType::Pointer path;
	    	path = extractor->GetOutput();
	    	path->Print(std::cout);
	    	salida.push_back(path);

	    	typedef itk::PathIterator< OutImageType, PathType > IteratorType;
	    	typedef PathType::ContinuousIndexType VertexType;

	    	//create output image
	    	typedef itk::Vector< unsigned char, 3 > ColorPixelType;
	    	typedef itk::Image< ColorPixelType, 2 > ColorImageType;
	    	typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
	    	typedef ColorIteratorType::IndexType ColorIndexType;
	    	typedef itk::ImageFileWriter<  ColorImageType  > ColorWriterType;

	    	ColorImageType::Pointer out= ColorImageType::New();

	    	//imt1
	    	InternalImageType::RegionType region;
	    	region=l2i->GetOutput()->GetLargestPossibleRegion();
	    	out->SetRegions( region );
	    	out->Allocate();

	    	ColorPixelType v_color;
	    	v_color[0]=255;
	    	v_color[1]=0;
	    	v_color[2]=0;


	    typedef itk::ImageRegionIterator<ColorImageType> ColorIteratorType;
	    typedef ColorIteratorType::IndexType ColorIndexType;

	    	ColorPixelType v;
	    		v[0]=0;
	    		v[1]=0;
	    		v[2]=0;

	    	ColorIteratorType iter3( out, out->GetRequestedRegion() );
	    	for (iter3.GoToBegin(); !iter3.IsAtEnd();  ++iter3 )
	    	{
	    			iter3.Set(v);
    		}


	    	// Create iterator following path through image
	    	IteratorType iter( l2i->GetOutput(), path );
	    	int i=0;
	    	while (!iter.IsAtEnd())
	    	{
	    		//std::cout << iter.GetPathPosition() << " -> " << std::flush;
	    		std::cout <<"id: "<<i++<<" path pos: "<< iter.GetPathPosition() << " im pos: "<<iter.GetIndex() << std::endl;
	    		out->SetPixel(iter.GetIndex(),v_color);

	    		try
	    		{
	    			++iter;
	    		}
	    		catch( itk::ExceptionObject & err )
	    		{
	    			std::cout << "ExceptionObject caught !" << std::endl;
	    			std::cout << err << std::endl;

	    		}

	    	}

#if GAC_WRITE
	    	ColorWriterType::Pointer writer = ColorWriterType::New();
	    	const char* filename_base="debug_contour_%02d.png";
	    	char filename[64];
	    	sprintf(filename,filename_base,label);
	    	std::cout<<"filename: "<<filename<<std::endl;
	    	writer->SetFileName(filename);
	    	writer->SetInput(out);
	    	writer->Update();
#endif
	    }



	return salida;
}
