#!/bin/bash

orig_name=$1
file_base=`basename $orig_name .mha`
out_name=$file_base.png
ZeroContourLevelSet $orig_name $out_name
