/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    itkBinaryImageToLabelMapFilterTest.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$

  Copyright (c) Insight Software Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif


#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkLabelObject.h"
#include "itkShapeLabelObject.h"
#include "itkLabelMap.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkShapeLabelMapFilter.h"

int main( int argc, char * argv [] )
{

  if( argc != 6 )
    {
    std::cerr << "usage: " << argv[0];
    std::cerr << " inputBinaryImage outputLabelImage";
    std::cerr << " fullyConnected(0/1)  foregroundValue backgroundValue";
    std::cerr << std::endl;
    return EXIT_FAILURE;
    }

  const unsigned int Dimension = 2;
  
  typedef unsigned char BinaryPixelType;
  typedef unsigned char LabelPixelType;

  typedef itk::Image< BinaryPixelType, Dimension > ImageType;

  //typedef itk::LabelObject< LabelPixelType, Dimension >   LabelObjectType;

  typedef itk::ShapeLabelObject< LabelPixelType, Dimension > LabelObjectType;

  typedef itk::LabelMap< LabelObjectType >                LabelMapType;
  
  typedef itk::ImageFileReader< ImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName( argv[1] );
  
  typedef itk::BinaryImageToLabelMapFilter< ImageType, LabelMapType> I2LType;
  I2LType::Pointer i2l = I2LType::New();
  // test the behavior without input
 
  i2l->SetFullyConnected( atoi(argv[3]) );
  i2l->SetInputForegroundValue( atoi(argv[4]) );
  i2l->SetOutputBackgroundValue( atoi(argv[5]) );

 
  typedef itk::LabelMapToLabelImageFilter< LabelMapType, ImageType> L2IType;
  L2IType::Pointer l2i = L2IType::New();

  typedef itk::ImageFileWriter< ImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();

  writer->SetFileName( argv[2] );
  writer->UseCompressionOn();


  i2l->SetInput( reader->GetOutput() );
  l2i->SetInput( i2l->GetOutput() );
  writer->SetInput( l2i->GetOutput() );

    writer->Update();
    
std::cout<<"nb: "<<i2l->GetOutput()->GetNumberOfLabelObjects()<<std::endl;


  //std::cout << i2l->GetNameOfClass() << std::endl;
   
  //i2l->Print( std::cout );

#if 1
typedef itk::ShapeLabelMapFilter< LabelMapType > ShapeFilterType;
ShapeFilterType::Pointer shape = ShapeFilterType::New();
shape->SetInput( i2l->GetOutput() );
shape->Update();
#endif
  
i2l->GetOutput()->PrintLabelObjects();

LabelMapType::Pointer labelMap = i2l->GetOutput();
for( unsigned int label=1; label<=labelMap->GetNumberOfLabelObjects(); label++ )
{
// we don’t need a SmartPointer of the label object here, because the reference is kept in
// in the label map.
const LabelObjectType * labelObject = labelMap->GetLabelObject( label );
std::cout << label << "\t" << labelObject->GetPhysicalSize() << "\t" << labelObject->GetCentroid()<<std::endl;
	//std::cout << *labelObject<<std::endl;
	//labelObject->Print( std::cout );
}



  return EXIT_SUCCESS;
}
