/*
 * GeodesicActiveContourImageFilterInteractiveTest.cxx
 *
 *  Created on: Jun 21, 2011
 *      Author: juan
 */


#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "GeodesicActiveContourImageFilterInteractive.h"

int main( int argc, const char *argv[] )
{
	std::string svn_version="svn_version: $Id: GeodesicActiveContourImageFilterInteractiveTest.cxx 313 2011-06-29 11:49:15Z juanc $";
	std::cout<<"svn_version: "<<svn_version<<std::endl;

	//read original image
	ReaderType::Pointer reader = ReaderType::New();
	std::string inputImageFilename="../data/iman.png";
	std::cout<<"reader filename: "<<inputImageFilename <<std::endl;
	reader->SetFileName( inputImageFilename );
	reader->Update();

	InternalImageType::Pointer im_orig;
	im_orig=reader->GetOutput();

	//set magnet points
	float magnet_points[5][2]={
						{30, 25},
						{40, 25},
						{50, 25},
						{60, 25},
						{70, 25}};
	NodeContainer::Pointer magnets = convert_coordinates(magnet_points,5,0);

	//set seed points
	//aca nuevamente puedo hacerlo desde un array, te digo para ahorrar código.
	float seed_points[1][2]={{50, 30}};
	float radius=10;
	NodeContainer::Pointer seeds = convert_coordinates(seed_points,1,-radius);

//curva inicial a transformar en mascara
	float curve_points[5][2]={
		{50, 40},
		{60,50},
		{60,60},
		{40,60},
		{40,50}};
	
	InternalImageType::SizeType size=im_orig->GetLargestPossibleRegion().GetSize();
	OutputImageType::Pointer im_mask=points_to_mask(size, curve_points,5);

	//calcular función distancia a partir de máscara.
    typedef itk::SignedDanielssonDistanceMapImageFilter<
                                         OutputImageType,
                                         InternalImageType >  DistanceComputationFilterType;
    DistanceComputationFilterType::Pointer filter = DistanceComputationFilterType::New();
   filter->SetInput( im_mask );
   filter->Update();
   InternalImageType::Pointer im_dist=filter->GetOutput();

	GACDemo algo;
	algo.configure_use_case(5);
	algo.setVerbose(2);	//put verbose to zero to avoid saving debug output
	InternalImageType::Pointer im_out=algo.execute(im_orig, im_dist, seeds, magnets);

	//get the curve from the implicit function
	PathType::Pointer path=implicit_to_curve( im_out);
	//get the curves in case they are disjoint
	std::vector<PathType::Pointer> curvas=segmentation_output_to_curves(im_out);
	return 0;
}
