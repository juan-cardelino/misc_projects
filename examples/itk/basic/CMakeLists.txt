# This project is designed to be built outside the Insight source tree.
PROJECT(basic)
cmake_minimum_required(VERSION 2.6)

# Find ITK.
FIND_PACKAGE(ITK REQUIRED)
IF(ITK_FOUND)
  INCLUDE(${ITK_USE_FILE})
ENDIF(ITK_FOUND)

FIND_PACKAGE(VTK REQUIRED)
INCLUDE(${VTK_USE_FILE})

#find_package(ItkVtkGlue REQUIRED)
#include(${ItkVtkGlue_USE_FILE})

#set(LIBS ItkVtkGlue ${LIBS} vtkCommon vtkHybrid)
set(LIBS ItkVtkGlue ${LIBS} ${VTK_LIBRARIES})

SET( EXAMPLE_LIST
HelloWorld
#RescaleIntensityImageFilter
 )

FOREACH( var ${EXAMPLE_LIST} )
  ADD_EXECUTABLE( ${var} ${var}.cxx)
  TARGET_LINK_LIBRARIES( ${var} ${ITK_LIBRARIES} ${LIBS})
ENDFOREACH( var )
