#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

// Software Guide : BeginLatex
//
// In medical imaging applications, the general shape, location and 
// orientation of an anatomical structure of interest is typically
// known \emph{a priori}. This information can be used to aid the
// segmentation process especially when image contrast is low or
// when the object boundary is not distinct.
//
// In \cite{Leventon2000}, Leventon \emph{et al.} extended the
// geodesic active contours method with an additional shape-influenced term in
// the driving PDE. The \doxygen{GeodesicActiveContourShapePriorLevelSetFilter}
// is a generalization of Leventon's approach and its use is illustrated 
// in the following example.
//
// To support shape-guidance, the generic level set 
// equation (Eqn(~\ref{eqn:LevelSetEquation})) is extended to incorporate a 
// shape guidance term:
//
// \begin{equation}
// \label{eqn:ShapeInfluenceTerm}
// \xi \left(\psi^{*}(\mathbf{x}) - \psi(\mathbf{x})\right)
// \end{equation}
//
// where $\psi^{*}$ is the signed distance function of the ``best-fit'' shape
// with respect to a shape model. The new term has the effect of driving the
// contour towards the best-fit shape. The scalar $\xi$ weights the influence
// of the shape term in the overall evolution. In general, the best-fit shape
// is not known ahead of time and has to be iteratively estimated in 
// conjunction with the contour evolution.
//
// As with the \doxygen{GeodesicActiveContourLevelSetImageFilter}, the 
// GeodesicActiveContourShapePriorLevelSetImageFilter expects two input
// images: the first is an initial level set and the second a feature image
// that represents the image edge potential. The configuration of this
// example is quite similar to the example in 
// Section~\ref{sec:GeodesicActiveContourImageFilter} and hence the description 
// will focus on the new objects involved in the segmentation process as shown
// in Figure~\ref{fig:GeodesicActiveContourShapePriorCollaborationDiagram}.
//
// \begin{figure} \center
// \includegraphics[width=\textwidth]{GeodesicActiveContourShapePriorCollaborationDiagram.eps}
// \itkcaption[GeodesicActiveContourShapePriorLevelSetImageFilter collaboration
// diagram]{Collaboration diagram for the GeodesicActiveContourShapePriorLevelSetImageFilter
// applied to a segmentation task.}
// \label{fig:GeodesicActiveContourShapePriorCollaborationDiagram}
// \end{figure}
//
// The process pipeline begins with centering the input image using the
// the \doxygen{ChangeInformationImageFilter} to simplify the estimation of the pose
// of the shape, to be explained later. 
// The centered image is then smoothed using non-linear diffusion to 
// remove noise and the gradient magnitude is computed from the smoothed image. 
// For simplicity, this example uses the \doxygen{BoundedReciprocalImageFilter}
// to produce the edge potential image.
//
// The \doxygen{FastMarchingImageFilter} creates an initial level set using three
// user specified seed positions and a initial contour radius. Three seeds are
// used in this example to facilitate the segmentation of long narrow objects
// in a smaller number of iterations.
// The output of the FastMarchingImageFilter is passed
// as the input to the GeodesicActiveContourShapePriorLevelSetImageFilter.
// At then end of the segmentation process, the output level set is passed
// to the \doxygen{BinaryThresholdImageFilter} to produce a binary mask
// representing the segmented object.
//
// The remaining objects in 
// Figure~\ref{fig:GeodesicActiveContourShapePriorCollaborationDiagram}
// are used for shape modeling and estimation.
// The \doxygen{PCAShapeSignedDistanceFunction} represents a statistical
// shape model defined by a mean signed distance and the first $K$ 
// principal components modes; while the \doxygen{Euler2DTransform} is used
// to represent the pose of the shape. In this implementation, the
// best-fit shape estimation problem is reformulated as a minimization problem
// where the \doxygen{ShapePriorMAPCostFunction} is the cost function to
// be optimized using the \doxygen{OnePlusOneEvolutionaryOptimizer}.
//
// It should be noted that, although particular shape model, transform
// cost function, and optimizer are used in this example, the implementation
// is generic, allowing different instances of these components to be
// plugged in. This flexibility allows a user to tailor the behavior of the
// segmentation process to suit the circumstances of the targeted application. 
//  
// Let's start the example by including the headers of the new filters 
// involved in the segmentation. 
//
// Software Guide : EndLatex 

#include "itkImage.h"

// Software Guide : BeginCodeSnippet
#include "itkGeodesicActiveContourShapePriorLevelSetImageFilter.h"
#include "itkChangeInformationImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
// Software Guide : EndCodeSnippet


//  Software Guide : BeginLatex
//
//  Next, we include the headers of the objects involved in shape 
//  modeling and estimation.  
//
//  Software Guide : EndLatex 


// Software Guide : BeginCodeSnippet
#include "itkPCAShapeSignedDistanceFunction.h"
#include "itkEuler2DTransform.h"
#include "itkShapePriorMAPCostFunction.h"
#include "itkOnePlusOneEvolutionaryOptimizer.h"
#include "itkNormalVariateGenerator.h"
#include "vnl/vnl_sample.h"
#include "itkNumericSeriesFileNames.h"
// Software Guide : EndCodeSnippet

#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSpatialFunctionImageEvaluatorFilter.h"


// Software Guide : BeginLatex
//
// Given the numerous parameters involved in tuning this segmentation method 
// it is not uncommon for a segmentation process to
// run for several minutes and still produce an unsatisfactory result. For debugging
// purposes it is quite helpful to track the evolution of the
// segmentation as it progresses. The following defines a 
// custom \doxygen{Command} class
// for monitoring the RMS change and shape parameters at each iteration.
//
//  \index{itk::Geodesic\-Active\-Contour\-Shape\-Prior\-LevelSet\-Image\-Filter!Monitoring}
//  \index{itk::Shape\-Prior\-Segmentation\-Level\-Set\-Image\-Filter!Monitoring}
//
// Software Guide : EndLatex 

// Software Guide : BeginCodeSnippet
#include "itkCommand.h"


int main( int argc, char *argv[] )
{
  if( argc < 3 )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " startX startY" << std::endl;
    return 1;
    }

   const   unsigned int        Dimension = 2;
   typedef float    PixelType;
   typedef unsigned char    OutPixelType;
   typedef itk::Image< PixelType, Dimension >    ImageType;
   typedef itk::Image< OutPixelType, Dimension >    OutImageType;

   typedef  itk::ImageFileReader< ImageType > ReaderType;
   typedef  itk::ImageFileWriter< OutImageType  > WriterType;
	typedef itk::ZeroCrossingImageFilter<ImageType,OutImageType> ZcrFilterType;

	ReaderType::Pointer reader = ReaderType::New();
	WriterType::Pointer writer = WriterType::New();
	ZcrFilterType::Pointer filter = ZcrFilterType::New();

	reader->SetFileName(argv[1]); 
	writer->SetFileName(argv[2]); 
	filter->SetForegroundValue(255);
	filter->SetBackgroundValue(0);

	filter->SetInput(reader->GetOutput());
	writer->SetInput(filter->GetOutput());
	writer->Update();

	return 0;
}
