/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    $RCSfile: GeodesicActiveContourShapePriorLevelSetImageFilter.cxx,v $
  Language:  C++
  Date:      $Date: 2009-03-17 21:44:42 $
  Version:   $Revision: 1.21 $

  Copyright (c) Insight Software Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif


#include "itkImage.h"

// Software Guide : BeginCodeSnippet
#include "itkGeodesicActiveContourShapePriorLevelSetImageFilter.h"
#include "itkChangeInformationImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
#include "itkImagePCAShapeModelEstimator.h" // Software Guide : EndCodeSnippet


//  Software Guide : BeginLatex
//
//  Next, we include the headers of the objects involved in shape 
//  modeling and estimation.  
//
//  Software Guide : EndLatex 


// Software Guide : BeginCodeSnippet
#include "itkPCAShapeSignedDistanceFunction.h"
#include "itkEuler2DTransform.h"
#include "itkShapePriorMAPCostFunction.h"
#include "itkOnePlusOneEvolutionaryOptimizer.h"
#include "itkNormalVariateGenerator.h"
#include "vnl/vnl_sample.h"
#include "itkNumericSeriesFileNames.h"
// Software Guide : EndCodeSnippet

#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSpatialFunctionImageEvaluatorFilter.h"


// Software Guide : BeginLatex
//
// Given the numerous parameters involved in tuning this segmentation method 
// it is not uncommon for a segmentation process to
// run for several minutes and still produce an unsatisfactory result. For debugging
// purposes it is quite helpful to track the evolution of the
// segmentation as it progresses. The following defines a 
// custom \doxygen{Command} class
// for monitoring the RMS change and shape parameters at each iteration.
//
//  \index{itk::Geodesic\-Active\-Contour\-Shape\-Prior\-LevelSet\-Image\-Filter!Monitoring}
//  \index{itk::Shape\-Prior\-Segmentation\-Level\-Set\-Image\-Filter!Monitoring}
//
// Software Guide : EndLatex 

// Software Guide : BeginCodeSnippet
#include "itkCommand.h"

using namespace std;

int main( int argc, char *argv[] )
{
  if( argc < 5 )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " nb_train  trainFilePattern";
    std::cerr << " seed1X seed1Y";
    std::cerr << std::endl;
    return 1;
    }

for(int i=0; i<argc;i++)
{
cout<<"id: "<<i<<" arg: "<<argv[i]<<endl;
}
   const   unsigned int        Dimension = 2;
   typedef float    my_PixelType;
   typedef itk::Image< my_PixelType, Dimension >    signdis_ImageType;

   typedef  itk::ImageFileReader< signdis_ImageType > ReaderType;
   typedef  itk::ImageFileWriter<  signdis_ImageType  > WriterType;
	ReaderType::Pointer reader = ReaderType::New();
int nb_train=atoi(argv[1]);

  itk::NumericSeriesFileNames::Pointer fileNamesCreator = 
          itk::NumericSeriesFileNames::New();
   std::vector<signdis_ImageType::Pointer> signdisImages( nb_train );

  fileNamesCreator->SetStartIndex( 0 );
  fileNamesCreator->SetEndIndex( nb_train - 1 );
  fileNamesCreator->SetSeriesFormat( argv[2] );
  const std::vector<std::string> & shapeModeFileNames = 
          fileNamesCreator->GetFileNames();

  for ( unsigned int k = 0; k < nb_train; k++ )
    {
    reader->SetFileName( shapeModeFileNames[k].c_str() );
    reader->Update();
	signdisImages[k] = reader->GetOutput();
    }


	cout<<"step 1"<<endl;
	typedef itk::ImagePCAShapeModelEstimator<signdis_ImageType,   signdis_ImageType >  my_Estimatortype;
	my_Estimatortype::Pointer filter = my_Estimatortype::New();
filter->SetNumberOfTrainingImages(nb_train);
filter->SetNumberOfPrincipalComponentsRequired(2);
  
  for ( unsigned int k = 0; k < nb_train; k++ )
    {
	
	filter->SetInput(k, signdisImages[k] ); 
}
WriterType::Pointer writer = WriterType::New();
	cout<<"step 2"<<endl;
	writer->SetFileName( argv[3] );
	cout<<"step 3"<<endl;
	writer->SetInput( filter->GetOutput(0) );
	cout<<"step 4"<<endl;
	writer->Update();
	cout<<"step 5"<<endl;

	writer->SetFileName( argv[4] );
	cout<<"step 6"<<endl;
	writer->SetInput( filter->GetOutput(1) );
	cout<<"step 7"<<endl;
	writer->Update();
	cout<<"step 8"<<endl;
	

	return 0;
}
