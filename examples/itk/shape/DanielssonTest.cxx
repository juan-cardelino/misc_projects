
#include "itkImage.h"

// Software Guide : BeginCodeSnippet
#include "itkLaplacianSegmentationLevelSetImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
// Software Guide : EndCodeSnippet

#include "itkFastMarchingImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkSignedDanielssonDistanceMapImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"

int main( int argc, char *argv[] )
{
	if( argc < 2 )
	{
		std::cerr << "Missing Parameters " << std::endl;
		std::cerr << "Usage: " << argv[0];
		std::cerr << " InputImage  InitialModel OutputImage";
		return 1;
	}

	//pixel types
	typedef float           InternalPixelType;
	typedef unsigned char                            InputPixelType;
	typedef unsigned char                            OutputPixelType;

	const     unsigned int    Dimension = 2;

	//image types
	typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;
	typedef itk::Image< InputPixelType, Dimension > InputImageType;
	typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

	//filters
	typedef itk::BinaryThresholdImageFilter< 
		InternalImageType, 
		OutputImageType    >       ThresholdingFilterType;


	typedef  itk::ImageFileReader< InputImageType > ReaderType;
	typedef  itk::ImageFileWriter<  OutputImageType  > WriterType;

	typedef itk::RescaleIntensityImageFilter< 
		InternalImageType, 
		OutputImageType >   CastFilterType;
	typedef  itk::SignedDanielssonDistanceMapImageFilter < InputImageType , InternalImageType > MapFilterType;

	ReaderType::Pointer reader1 = ReaderType::New();
	reader1->SetFileName( argv[1] );

	MapFilterType::Pointer filter = MapFilterType::New();

	filter->SetInput(reader1->GetOutput());

	CastFilterType::Pointer converter=CastFilterType::New();
	converter->SetInput( filter->GetOutput() );
	WriterType::Pointer speedWriter
		= WriterType::New();
	speedWriter->SetInput( converter->GetOutput() );
	//speedWriter->SetInput( filter->GetOutput() );
	speedWriter->SetFileName( "speedImage.mha" );
	speedWriter->Update();

	return 0;
}
