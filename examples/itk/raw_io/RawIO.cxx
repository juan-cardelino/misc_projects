#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkRawImageIO.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkFlipImageFilter.h"
#include "itkCastImageFilter.h"

  //typedef   unsigned char           PixelType;
typedef   double PixelType;
  const     unsigned int    Dimension = 2;
  typedef itk::Image< PixelType, Dimension >  ImageType;
  typedef itk::ImageFileWriter< ImageType > WriterType;
     typedef itk::ImageFileReader< ImageType >  ReaderType;


     typedef itk::Image< unsigned short, 2 > ReaderImageType;
                 typedef ReaderImageType::PixelType ItkReaderPixelType;
                 typedef itk::RawImageIO< ItkReaderPixelType, Dimension> RawImageIOType;





void fillRegistrationImageData(
                               std::string itkImageFileName, 

                               ImageType::Pointer &rawImage,
                               std::string fileNameOutputPrefix
                               ) { 

    ///////////////////////////////////////////////////////////////////////////////////
    // itkImage
    ///////////////////////////////////////////////////////////////////////////////////

	typedef itk::Vector<float,3> Vector3DType;
	Vector3DType m_Origin_Offset;
    m_Origin_Offset[0] = 0.0;
    m_Origin_Offset[1] = 0.0;
    m_Origin_Offset[2] = 0.0;


    ReaderType::Pointer reader = NULL;
 
    bool has2flip = false;

    try {
        std::string::size_type testPNG  = itkImageFileName.find(".png");
        std::string::size_type testJPG  = itkImageFileName.find(".jpg");
        std::string::size_type testJPEG = itkImageFileName.find(".jpeg");

        if ( ( testPNG   != std::string::npos ) ||
             ( testJPG   != std::string::npos ) || 
             ( testJPEG  != std::string::npos ) )
        {
            reader = ReaderType::New(); 
            reader->SetFileName( itkImageFileName.c_str() );
            reader->Update();
        } else {

            ///////////////////////////////////////////////////////////////////////////////////////
            // Load raw image 
            ///////////////////////////////////////////////////////////////////////////////////////

            RawImageIOType::Pointer io = RawImageIOType::New();
            io->SetComponentType(itk::ImageIOBase::USHORT);
            io->SetFileName(itkImageFileName.c_str());
            io->SetHeaderSize(0);
            io->SetNumberOfComponents(1);
            io->SetFileDimensionality(2);
            io->SetNumberOfDimensions(2);
            io->SetByteOrderToLittleEndian();
            unsigned int dim[2] = {1024,1024};
            double spacing[2] = {1.0, 1.0};
            double origin[2] = {0.0,0.0};
            for(unsigned int i=0; i<2; i++)
            {
                io->SetDimensions(i,dim[i]);
                io->SetSpacing(i,spacing[i]);
                io->SetOrigin(i,origin[i]);
            }

            reader = ReaderType::New(); 
            reader->SetFileName( itkImageFileName.c_str() );
            reader->SetImageIO(io);
            reader->Update();

            has2flip = true;
        }
    }
    catch ( itk::ExceptionObject & e )
    {
        std::cout << e.what() << std::endl;
        exit(0);
     }     

    ///////////////////////////////////////////////////////////////////////////////////////
    // Rescale the intensity
    ///////////////////////////////////////////////////////////////////////////////////////
    typedef itk::RescaleIntensityImageFilter<ImageType, ImageType> RescaleIntensityFilterType;
    RescaleIntensityFilterType::Pointer rescaler = RescaleIntensityFilterType::New();
    rescaler->SetInput(reader->GetOutput());
    rescaler->SetOutputMinimum(0.0);
    rescaler->SetOutputMaximum(255);
    rescaler->Update();

    if ( has2flip ) {
        typedef itk::FlipImageFilter< ImageType >  FlipImageFilterType;
        FlipImageFilterType::Pointer flipImageFilter = FlipImageFilterType::New();

        typedef FlipImageFilterType::FlipAxesArrayType FlipAxesArrayType;
        FlipAxesArrayType flipArray;
        flipArray[0] = false;
        flipArray[1] = true;
        flipImageFilter->SetFlipAxes( flipArray );

        flipImageFilter->SetInput(rescaler->GetOutput());
        flipImageFilter->Update();
 
        rawImage = flipImageFilter->GetOutput();
        rawImage->DisconnectPipeline();

        const ImageType::PointType& orgn = rescaler->GetOutput()->GetOrigin();
        rawImage->SetOrigin( orgn );

    } else {
        rawImage = rescaler->GetOutput();
        rawImage->DisconnectPipeline();
    }




    typedef   unsigned char OutputPixelType;
      const     unsigned int    Dimension = 2;
      typedef itk::Image< OutputPixelType, Dimension >  OutputImageType;
      typedef itk::ImageFileWriter< OutputImageType > OutputWriterType;

    typedef itk::CastImageFilter<ImageType,OutputImageType> CastImageFilterType;
    CastImageFilterType::Pointer castFilter = CastImageFilterType::New();
    castFilter->SetInput(rawImage);


    OutputWriterType::Pointer writer=OutputWriterType::New();
    writer->SetInput(castFilter->GetOutput());
    writer->SetFileName(fileNameOutputPrefix);
    writer->Update();
}


int main(int argc, char* argv[])
{
	ImageType::Pointer rawImage=ImageType::New();

	fillRegistrationImageData(argv[1], rawImage, argv[2]);
	return 0;
}
