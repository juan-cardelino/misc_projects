cmake_minimum_required(VERSION 2.8)
 
project(IntermodesThresholdImageFilter)

FIND_PACKAGE(ITK REQUIRED)
IF(ITK_FOUND)
MESSAGE( STATUS "found ITK"  )
  INCLUDE(${ITK_USE_FILE})
ENDIF(ITK_FOUND)

add_executable(IntermodesThresholdImageFilter IntermodesThresholdImageFilter.cxx )
target_link_libraries(IntermodesThresholdImageFilter  ${ITK_LIBRARIES})



