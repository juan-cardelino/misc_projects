/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/

// Software Guide : BeginCommandLineArgs
// INPUTS:  {BrainProtonDensitySlice.png}
// OUTPUTS: {BinaryThresholdImageFilterOutput.png}
// ARGUMENTS:    150 180 0 255
// Software Guide : EndCommandLineArgs

// Software Guide : BeginLatex
//
//  \begin{floatingfigure}[rlp]{6cm}
//    \centering
//    \includegraphics[width=7cm]{BinaryThresholdTransferFunction.eps}
//    \caption[BinaryThresholdImageFilter transfer function]
//            {Transfer function of the BinaryThresholdImageFilter.
//            \label{fig:BinaryThresholdTransferFunction}}
//  \end{floatingfigure}
//
// This example illustrates the use of the binary threshold image filter.
// This filter is used to transform an image into a binary image by changing
// the pixel values according to the rule illustrated in
// Figure~\ref{fig:BinaryThresholdTransferFunction}. The user defines two
// thresholds---Upper and Lower---and two intensity values---Inside and
// Outside. For each pixel in the input image, the value of the pixel is
// compared with the lower and upper thresholds. If the pixel value is inside
// the range defined by $[Lower,Upper]$ the output pixel is assigned the
// InsideValue. Otherwise the output pixels are assigned to the OutsideValue.
// Thresholding is commonly applied as the last operation of a segmentation
// pipeline.
//
// \index{itk::Binary\-Threshold\-Image\-Filter!Instantiation}
// \index{itk::Binary\-Threshold\-Image\-Filter!Header}
//
// The first step required to use the \doxygen{BinaryThresholdImageFilter} is
// to include its header file.
//
// Software Guide : EndLatex

#include "itkBinaryThresholdImageFilter.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkBinaryBallStructuringElement.h"

#include "itkMaskImageFilter.h"
#include "itkMaskNegatedImageFilter.h"

#include "itkImageRegionIterator.h"
#include "itkBinaryImageToShapeLabelMapFilter.h"

#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkLabelGeometryImageFilter.h"

#include "itkLabelMapOverlayImageFilter.h"
#include "itkRGBPixel.h"

typedef itk::Image<unsigned char, 2>  ImageType;

void CreateCircularMask(ImageType::Pointer image, ImageType::Pointer &mask);
void CreateHalfMask(ImageType::Pointer image, ImageType::Pointer &mask);

int main( int argc, char * argv[] )
{

  /*========================================================*/
  if( argc < 7 )
    {
    std::cerr << "Usage: " << argv[0];
    std::cerr << " inputImageFile outputImageFile ";
    std::cerr << " lowerThreshold upperThreshold ";
    std::cerr << " outsideValue insideValue   "  << std::endl;
    return EXIT_FAILURE;
    }

  typedef  unsigned char  InputPixelType;
  typedef  unsigned char  OutputPixelType;

  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::Image< OutputPixelType, 2 >   OutputImageType;

  typedef itk::BinaryThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  typedef itk::ImageFileWriter< OutputImageType >  WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  FilterType::Pointer filter = FilterType::New();
  WriterType::Pointer writer = WriterType::New();

  typedef itk::BinaryBallStructuringElement<OutputImageType::PixelType, OutputImageType::ImageDimension> StructuringElementType;
  StructuringElementType structuringElement;
  structuringElement.SetRadius(5);
  structuringElement.CreateStructuringElement();
 
  typedef itk::BinaryMorphologicalOpeningImageFilter <OutputImageType, OutputImageType, StructuringElementType> BinaryMorphologicalOpeningImageFilterType;
  BinaryMorphologicalOpeningImageFilterType::Pointer openingFilter = BinaryMorphologicalOpeningImageFilterType::New();

  reader->SetFileName( argv[1] );
  reader->Update();

  /*==================MaskImageFilter.cxx===================*/ 
  InputImageType::Pointer mask = ImageType::New();
  CreateCircularMask(reader->GetOutput() , mask);
  //CreateHalfMask(reader->GetOutput(), mask);
 
  typedef itk::MaskImageFilter< InputImageType, InputImageType > MaskFilterType;
  MaskFilterType::Pointer maskFilter = MaskFilterType::New();
  maskFilter->SetInput(reader->GetOutput());
  maskFilter->SetMaskImage(mask);
//  mask->Print(std::cout);

      std::cout << "aaa" << std::endl;
  typedef  itk::ImageFileWriter< ImageType  > MaskWriterType;
  MaskWriterType::Pointer maskwriter = MaskWriterType::New();
  maskwriter->SetFileName("outputmask.png");
  maskwriter->SetInput( maskFilter->GetOutput() );
 // maskwriter->Update();
 // maskwriter->UpdateLargestPossibleRegion();
  
  OutputImageType::Pointer imageMask;
  imageMask = maskFilter->GetOutput();
   std::cout << "bbb" << std::endl;
  /*========================================================*/

  filter->SetInput( imageMask.GetPointer() );

  const OutputPixelType outsideValue = atoi( argv[5] );
  const OutputPixelType insideValue  = atoi( argv[6] );

  filter->SetOutsideValue( outsideValue );
  filter->SetInsideValue(  insideValue  );

  const InputPixelType lowerThreshold = atoi( argv[3] );
  const InputPixelType upperThreshold = atoi( argv[4] );

  filter->SetLowerThreshold( lowerThreshold );
  filter->SetUpperThreshold( upperThreshold );
  filter->Update();
  
  openingFilter->SetInput( filter->GetOutput() );
  openingFilter->SetKernel(structuringElement);
  openingFilter->Update();

  OutputImageType::Pointer image;
  image = openingFilter->GetOutput();
  
  writer->SetInput( image.GetPointer() );
  writer->SetFileName( argv[2] );
  writer->Update();
  /*========================================================*/
  
  /*==================MaskNegatedImageFilter.cxx===================*/ 
/*
  InputImageType::Pointer maskNeg = ImageType::New();
  CreateCircularMask(openingFilter->GetOutput(), maskNeg);
  //CreateHalfMask(reader->GetOutput(), mask);
 
  typedef itk::MaskNegatedImageFilter< InputImageType, InputImageType > MaskNegFilterType;
  MaskNegFilterType::Pointer maskNegFilter = MaskNegFilterType::New();
  maskNegFilter->SetInput(reader->GetOutput());
  maskNegFilter->SetMaskImage(maskNeg);
  maskNeg->Print(std::cout);


  typedef  itk::ImageFileWriter< ImageType  > MaskNegWriterType;
  MaskNegWriterType::Pointer maskNegwriter = MaskNegWriterType::New();
  maskNegwriter->SetFileName("outputmask.png");
  maskNegwriter->SetInput( maskNegFilter->GetOutput() );
  maskNegwriter->Update();
  
  OutputImageType::Pointer imageMask;
  imageMask = maskNegFilter->GetOutput();
*/

 /*========================================================*/  
  
  /*=========BinaryImageToShapeLabelMapFilter.cxx===========*/
  typedef itk::BinaryImageToShapeLabelMapFilter<OutputImageType> BinaryImageToShapeLabelMapFilterType;
  BinaryImageToShapeLabelMapFilterType::Pointer binaryImageToShapeLabelMapFilter = BinaryImageToShapeLabelMapFilterType::New();
  binaryImageToShapeLabelMapFilter->SetInput(image.GetPointer());
  binaryImageToShapeLabelMapFilter->Update();
  
    // The output of this filter is an itk::ShapeLabelMap, which contains itk::ShapeLabelObject's
  std::cout << "There are " << binaryImageToShapeLabelMapFilter->GetOutput()->GetNumberOfLabelObjects() << " objects." << std::endl;
 
  // Loop over all of the blobs
  for(unsigned int i = 0; i < binaryImageToShapeLabelMapFilter->GetOutput()->GetNumberOfLabelObjects(); i++)
    {
    BinaryImageToShapeLabelMapFilterType::OutputImageType::LabelObjectType* labelObject = binaryImageToShapeLabelMapFilter->GetOutput()->GetNthLabelObject(i);
    // Output the bounding box (an example of one possible property) of the ith region
    std::cout << "Object " << i << " has bounding box " << labelObject->GetBoundingBox() << std::endl;
    }
  /*========================================================*/


  /*=============LabelGeometryImageFilter.cxx===============*/
  typedef itk::BinaryImageToLabelMapFilter<OutputImageType> BinaryImageToLabelMapFilterType;
  BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
  binaryImageToLabelMapFilter->SetInput(image.GetPointer());
  binaryImageToLabelMapFilter->Update();

  typedef itk::LabelMapToLabelImageFilter<BinaryImageToLabelMapFilterType::OutputImageType, OutputImageType> LabelMapToLabelImageFilterType;
  LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
  labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
  labelMapToLabelImageFilter->Update(); 
    
  typedef itk::LabelGeometryImageFilter< OutputImageType > LabelGeometryImageFilterType;
  LabelGeometryImageFilterType::Pointer labelGeometryImageFilter = LabelGeometryImageFilterType::New();
  labelGeometryImageFilter->SetInput( labelMapToLabelImageFilter->GetOutput() );
 
  // These generate optional outputs.
  labelGeometryImageFilter->CalculatePixelIndicesOn();
  labelGeometryImageFilter->CalculateOrientedBoundingBoxOn();
  labelGeometryImageFilter->CalculateOrientedLabelRegionsOn();
 
  labelGeometryImageFilter->Update();
 
  LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryImageFilter->GetLabels();
  LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;
  std::cout << "Number of labels: " << labelGeometryImageFilter->GetNumberOfLabels() << std::endl;
  std::cout << std::endl;
 
  for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
    {
    LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;
    std::cout << "\tVolume: " << labelGeometryImageFilter->GetVolume(labelValue) << std::endl;
    std::cout << "\tIntegrated Intensity: " << labelGeometryImageFilter->GetIntegratedIntensity(labelValue) << std::endl;
    std::cout << "\tCentroid: " << labelGeometryImageFilter->GetCentroid(labelValue) << std::endl;
    std::cout << "\tWeighted Centroid: " << labelGeometryImageFilter->GetWeightedCentroid(labelValue) << std::endl;
    std::cout << "\tAxes Length: " << labelGeometryImageFilter->GetAxesLength(labelValue) << std::endl;
    std::cout << "\tMajorAxisLength: " << labelGeometryImageFilter->GetMajorAxisLength(labelValue) << std::endl;
    std::cout << "\tMinorAxisLength: " << labelGeometryImageFilter->GetMinorAxisLength(labelValue) << std::endl;
    std::cout << "\tEccentricity: " << labelGeometryImageFilter->GetEccentricity(labelValue) << std::endl;
    std::cout << "\tElongation: " << labelGeometryImageFilter->GetElongation(labelValue) << std::endl;
    std::cout << "\tOrientation: " << labelGeometryImageFilter->GetOrientation(labelValue) << std::endl;
    std::cout << "\tBounding box: " << labelGeometryImageFilter->GetBoundingBox(labelValue) << std::endl;
 
    std::cout << std::endl << std::endl;
 
    }
 /*========================================================*/ 
 
  
 /*=============LabelMapOverlayImageFilter.cxx=============*/ 
  typedef itk::RGBPixel<unsigned char> RGBPixelType;
  typedef itk::Image<RGBPixelType> RGBImageType;
 
  typedef itk::LabelMapOverlayImageFilter<BinaryImageToLabelMapFilterType::OutputImageType, OutputImageType, RGBImageType>
                                       LabelMapOverlayImageFilterType;
  LabelMapOverlayImageFilterType::Pointer labelMapOverlayImageFilter = LabelMapOverlayImageFilterType::New();
  labelMapOverlayImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
  labelMapOverlayImageFilter->SetFeatureImage(image);
  labelMapOverlayImageFilter->SetOpacity(.5);
  labelMapOverlayImageFilter->Update();
 
  typedef  itk::ImageFileWriter< RGBImageType  > RGBWriterType;
  RGBWriterType::Pointer rgbwriter = RGBWriterType::New();
  rgbwriter->SetFileName("output.png");
  rgbwriter->SetInput(labelMapOverlayImageFilter->GetOutput());
  rgbwriter->Update();
 /*========================================================*/
  

  return EXIT_SUCCESS;
}


void CreateCircularMask(ImageType::Pointer image, ImageType::Pointer &mask)
{
  ImageType::RegionType region = image->GetLargestPossibleRegion();
 
  mask->SetRegions(region);
  mask->Allocate();
 
  ImageType::SizeType regionSize = region.GetSize();
 
  itk::ImageRegionIterator<ImageType> imageIterator(mask,region);
 
 double const MY_RADIUS = 512;
 
  // Make the left half of the mask white and the right half black
  while(!imageIterator.IsAtEnd())
  {
    if ( ( (imageIterator.GetIndex()[0]-regionSize[0] / 2)*(imageIterator.GetIndex()[0]-regionSize[0] / 2 ) + 
    	(imageIterator.GetIndex()[1]-regionSize[1] / 2)*(imageIterator.GetIndex()[1]-regionSize[1] / 2 ) ) > MY_RADIUS*MY_RADIUS*0.95  )
        {
        imageIterator.Set(0);
        }
      else
        {
        imageIterator.Set(255);
        }
 
    ++imageIterator;
  }
}

void CreateHalfMask(ImageType::Pointer image, ImageType::Pointer &mask)
{
  ImageType::RegionType region = image->GetLargestPossibleRegion();
 
  mask->SetRegions(region);
  mask->Allocate();
 
  ImageType::SizeType regionSize = region.GetSize();
 
  itk::ImageRegionIterator<ImageType> imageIterator(mask,region);
 
  // Make the left half of the mask white and the right half black
  while(!imageIterator.IsAtEnd())
  {
    if(imageIterator.GetIndex()[0] > regionSize[0] / 2)
        {
        imageIterator.Set(0);
        }
      else
        {
        imageIterator.Set(255);
        }
 
    ++imageIterator;
  }
 
}
