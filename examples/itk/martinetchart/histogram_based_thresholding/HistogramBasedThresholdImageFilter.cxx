/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/

//    INPUTS:  {BrainProtonDensitySlice.png}
//    OUTPUTS: {OtsuThresholdImageFilterOutput.png}
//    ARGUMENTS:    255 0

#include "itkImageToImageFilter.h"

#include "itkHistogramThresholdImageFilter.h"
#include "itkHuangThresholdImageFilter.h"
#include "itkIsoDataThresholdImageFilter.h"
#include "itkKittlerIllingworthThresholdImageFilter.h"
#include "itkLiThresholdImageFilter.h"
#include "itkMaximumEntropyThresholdImageFilter.h"
#include "itkMomentsThresholdImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkRenyiEntropyThresholdImageFilter.h"
#include "itkShanbhagThresholdImageFilter.h"
#include "itkTriangleThresholdImageFilter.h"
#include "itkYenThresholdImageFilter.h"
#include "itkIntermodesThresholdImageFilter.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

int main( int argc, char * argv[] )
{
  if( argc < 5 )
    {
    std::cerr << "Usage: " << argv[0];
    std::cerr << " inputImageFile";
    std::cerr << " insideValue    outsideValue  thresholdMethod(1..13) "  << std::endl;
    return EXIT_FAILURE;
    }

  typedef  unsigned char  InputPixelType;
  typedef  unsigned char  OutputPixelType;

  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::Image< OutputPixelType, 2 >   OutputImageType;
  
  std::string outputfname;
  
  //std::vector< itk::ImagetoImageFilter< InputImageType, OutputImageType > > filterVector;

	//typedef itk::HistogramThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	///FilterType::Pointer filter = FilterType::New();
	//filterVector.push_back(filterHisto);
	outputfname = "OutHistogram.png";
	std::cout << "HistogramThresholdImageFilter" << std::endl;


	typedef itk::HuangThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutHuang.png";
	std::cout << "HuangThresholdImageFilter" << std::endl;
/*
	typedef itk::IsoDataThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutIsoData.png";
	std::cout << "IsoDataThresholdImageFilter" << std::endl;
		
	typedef itk::KittlerIllingworthThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutKittlerIllingworth.png";
	std::cout << "KittlerIllingworthThresholdImageFilter" << std::endl;

	typedef itk::LiThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutLi.png";
	std::cout << "LiThresholdImageFilter" << std::endl;

	typedef itk::MaximumEntropyThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutMaximumEntropy.png";
	std::cout << "MaximumEntropyThresholdImageFilter" << std::endl;

	typedef itk::MomentsThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutMoments.png";
	std::cout << "MomentsThresholdImageFilter" << std::endl;

	typedef itk::OtsuThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutOtsu.png";
	std::cout << "OtsuThresholdImageFilter" << std::endl;

	typedef itk::RenyiEntropyThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutRenyiEntropy.png";
	std::cout << "RenyiEntropyThresholdImageFilter" << std::endl;

	typedef itk::ShanbhagThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutShanbhag.png";
	std::cout << "ShanbhagThresholdImageFilter" << std::endl;

	typedef itk::TriangleThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
	FilterType::Pointer filter = FilterType::New();
	outputfname = "OutTriangle.png";
	std::cout << "TriangleThresholdImageFilter" << std::endl;
		break;
	case 12: 
		typedef itk::YenThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
		FilterType::Pointer filter = FilterType::New();
		outputfname = "OutYen.png";
		std::cout << "YenThresholdImageFilter" << std::endl;
		break;
	case 13: 
		typedef itk::IntermodesThresholdImageFilter< InputImageType, OutputImageType >  FilterType;
		FilterType::Pointer filter = FilterType::New();
		outputfname = "OutIntermodes.png";
		std::cout << "IntermodesThresholdImageFilter" << std::endl;
		break;
	default:
		std::cout << "Methods from 1..13" << std::endl;
		return -1;
	}
*/
	
  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  typedef itk::ImageFileWriter< OutputImageType >  WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  //FilterType::Pointer filter = FilterType::New();

  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( filter->GetOutput() );
  reader->SetFileName( argv[1] );

  filter->SetInput( reader->GetOutput() );

  const OutputPixelType outsideValue = atoi( argv[2] );
  const OutputPixelType insideValue  = atoi( argv[3] );

  filter->SetOutsideValue( outsideValue );
  filter->SetInsideValue(  insideValue  );
  filter->Update();

  int threshold = filter->GetThreshold();
  std::cout << "Threshold = " << threshold << std::endl;

  writer->SetFileName( outputfname );
  writer->Update();

  return EXIT_SUCCESS;
}
