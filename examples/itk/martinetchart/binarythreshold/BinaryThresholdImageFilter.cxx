/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/

// Software Guide : BeginCommandLineArgs
// INPUTS:  {BrainProtonDensitySlice.png}
// OUTPUTS: {BinaryThresholdImageFilterOutput.png}
// ARGUMENTS:    150 180 0 255
// Software Guide : EndCommandLineArgs

// Software Guide : BeginLatex
//
//  \begin{floatingfigure}[rlp]{6cm}
//    \centering
//    \includegraphics[width=7cm]{BinaryThresholdTransferFunction.eps}
//    \caption[BinaryThresholdImageFilter transfer function]
//            {Transfer function of the BinaryThresholdImageFilter.
//            \label{fig:BinaryThresholdTransferFunction}}
//  \end{floatingfigure}
//
// This example illustrates the use of the binary threshold image filter.
// This filter is used to transform an image into a binary image by changing
// the pixel values according to the rule illustrated in
// Figure~\ref{fig:BinaryThresholdTransferFunction}. The user defines two
// thresholds---Upper and Lower---and two intensity values---Inside and
// Outside. For each pixel in the input image, the value of the pixel is
// compared with the lower and upper thresholds. If the pixel value is inside
// the range defined by $[Lower,Upper]$ the output pixel is assigned the
// InsideValue. Otherwise the output pixels are assigned to the OutsideValue.
// Thresholding is commonly applied as the last operation of a segmentation
// pipeline.
//
// \index{itk::Binary\-Threshold\-Image\-Filter!Instantiation}
// \index{itk::Binary\-Threshold\-Image\-Filter!Header}
//
// The first step required to use the \doxygen{BinaryThresholdImageFilter} is
// to include its header file.
//
// Software Guide : EndLatex

#include "itkBinaryThresholdImageFilter.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

int main( int argc, char * argv[] )
{
  if( argc < 7 )
    {
    std::cerr << "Usage: " << argv[0];
    std::cerr << " inputImageFile outputImageFile ";
    std::cerr << " lowerThreshold upperThreshold ";
    std::cerr << " outsideValue insideValue   "  << std::endl;
    return EXIT_FAILURE;
    }

  typedef  unsigned char  InputPixelType;
  typedef  unsigned char  OutputPixelType;

  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::Image< OutputPixelType, 2 >   OutputImageType;

  typedef itk::BinaryThresholdImageFilter< InputImageType, OutputImageType >  FilterType;

  typedef itk::ImageFileReader< InputImageType >  ReaderType;

  typedef itk::ImageFileWriter< OutputImageType >  WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  FilterType::Pointer filter = FilterType::New();

  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( filter->GetOutput() );
  reader->SetFileName( argv[1] );

  filter->SetInput( reader->GetOutput() );

  const OutputPixelType outsideValue = atoi( argv[5] );
  const OutputPixelType insideValue  = atoi( argv[6] );

  filter->SetOutsideValue( outsideValue );
  filter->SetInsideValue(  insideValue  );

  const InputPixelType lowerThreshold = atoi( argv[3] );
  const InputPixelType upperThreshold = atoi( argv[4] );

  filter->SetLowerThreshold( lowerThreshold );
  filter->SetUpperThreshold( upperThreshold );
  filter->Update();

  writer->SetFileName( argv[2] );
  writer->Update();

  return EXIT_SUCCESS;
}
