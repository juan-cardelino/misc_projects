project(ReviewExamples)

cmake_minimum_required(VERSION 2.6)
# Find ITK.
FIND_PACKAGE(ITK REQUIRED)
IF(ITK_FOUND)
  INCLUDE(${ITK_USE_FILE})
ENDIF(ITK_FOUND)

add_executable(ScalarSinglePhaseDense2D itkScalarSinglePhaseDense2DTest.cxx )
target_link_libraries(ScalarSinglePhaseDense2D ${ITK_LIBRARIES} )

add_executable(ScalarSinglePhaseSparse2D itkScalarSinglePhaseSparse2DTest.cxx )
target_link_libraries(ScalarSinglePhaseSparse2D ${ITK_LIBRARIES} )



