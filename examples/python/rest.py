#!/usr/bin/env python3

import os, glob, sys
import numpy as np
import colorsys
import skfmm

import matplotlib.pyplot as plt

import numpy as np
from scipy.spatial import Delaunay
from collections import namedtuple

Circle = namedtuple('Circle', ['radius', 'points'])


from math import pi,sqrt,sin,cos,atan2


def haversine(pos1, pos2):
    lat1 = pos1[0]
    long1 = pos1[1]
    lat2 = pos2[0]
    long2 = pos2[1]

    degree_to_rad = float(pi / 180.0)

    d_lat = (lat2 - lat1) * degree_to_rad
    d_long = (long2 - long1) * degree_to_rad

    a = pow(sin(d_lat / 2), 2) + cos(lat1 * degree_to_rad) * cos(lat2 * degree_to_rad) * pow(sin(d_long / 2), 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    km = 6367 * c
    mi = 3956 * c

    return {"km":km, "miles":mi}



def define_circle(p1, p2, p3):
    """
    Returns the center and radius of the circle passing the given 3 points.
    In case the 3 points form a line, returns (None, infinity).
    """
    temp = p2[0] * p2[0] + p2[1] * p2[1]
    bc = (p1[0] * p1[0] + p1[1] * p1[1] - temp) / 2
    cd = (temp - p3[0] * p3[0] - p3[1] * p3[1]) / 2
    det = (p1[0] - p2[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p2[1])

    if abs(det) < 1.0e-6:
        return (None, np.inf)

    # Center of circle
    cx = (bc * (p2[1] - p3[1]) - cd * (p1[1] - p2[1])) / det
    cy = ((p1[0] - p2[0]) * cd - (p2[0] - p3[0]) * bc) / det

    radius = np.sqrt((cx - p1[0]) ** 2 + (cy - p1[1]) ** 2)
    return ((cx, cy), radius)


def knuthifier(points):
    circles = []

    triangles = Delaunay(points)
    for simplex in triangles.simplices:
        triangle = triangles.points[simplex]
        a = np.linalg.norm(triangle[0] - triangle[1])
        b = np.linalg.norm(triangle[1] - triangle[2])
        c = np.linalg.norm(triangle[2] - triangle[0])
        p = (a + b + c) / 2
        r = a * b * c / (4 * np.sqrt(p * (p - a) * (p - b) * (p - c)))
        circles.append(Circle(r, triangle))

    sorted_circles = sorted(circles, key=lambda x: x.radius, reverse=True)
    circ = sorted_circles[0]
    center, rad = define_circle(circ.points[0], circ.points[1], circ.points[2])

    print("\n".join(map(str, sorted_circles)))
    return center, circ


def gps2idx(p):
    idx=[]
    idx[0] = np.round((p[0]-lat_r[0])/dlat)
    idx[1] = np.round((p[1]-long_r[0])/dlong)
    return idx

def idx2gps(idx):
    print("hola")

if __name__ == "__main__":
    points = np.asarray([[-23.5575, -46.6903],
                         [-23.5919, -46.6786],
                         [-23.5805, -46.6747],
                         [-23.5632, -46.678],
                         [-23.546, -46.6491],
                         [-23.5553, -46.6628]])

    center, circ = knuthifier(points)
    print("center: ", center)

    plt.plot(points[:,0],points[:,1], '.')
    plt.plot(center[0], center[1], '.r')
    plt.plot(circ.points[0][0],circ.points[0][1],'.g')
    plt.plot(circ.points[1][0], circ.points[1][1], '.g')
    plt.plot(circ.points[2][0], circ.points[2][1], '.g')

    print("distances: ")
    for i,p in enumerate(circ.points):
        d=haversine(center, p)
        print(d)

    #fast marching example
    N=100
    lat_r=[-23.5, -23.6]
    dlat =abs(lat_r[1]-lat_r[0])/N
    long_r=[-46.6, -46.7]
    dlong = abs(long_r[1]-long_r[0])/N
    X, Y = np.meshgrid(np.linspace(lat_r[0], lat_r[1], N), np.linspace(long_r[0], long_r[1], N))
    phi = np.ones_like(X)

    idx_circ=gps2idx(center)
    phi[50, 50] = 0
    dm = skfmm.distance(phi)

    plt.figure()
    plt.imshow(dm)
    for i, p in enumerate(circ.points):
        plt.plot(gps2idx(p)[0],gps2idx(p)[1],'*r')
    plt.show()