#!/usr/bin/env python3
# coding=utf-8

import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dense
from tensorflow.keras.callbacks import TensorBoard
from time import time
import matplotlib.pyplot as plt


def learn_linear():
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

    N = 100
    xf = 10
    x = np.linspace(0, xf, N)
    y = 2 * x  # construyo una recta con pendiente 2
    yn = y + 0.2*np.random.randn(N)     # le agrego ruido para hacerlo realista
    plt.plot(x, y, '-b')
    plt.plot(x, yn,'.r')
    plt.legend(['recta real', 'datos con ruido'])
    plt.show()

    # datos de entrenamiento
    training_data = x
    target_data = y

    # construcción del modelo
    model = Sequential()
    model.add(Dense(1, input_dim=1))  # armo un modelo con una sola neurona y = w1*x + w2
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

    # entrenar el modelo
    model.fit(training_data, target_data, epochs=1000, verbose=2, callbacks=[tensorboard])

    # el resultado del entrenamiento son los pesos w1 y w2
    print(model.layers[0].get_weights())

    # predigo los valores de y en base a los x observados usando el modelo
    yp = model.predict(training_data)
    print(yp)

    #grafico los resultados reales
    plt.plot(x, yp, '-b')
    plt.plot(x, yn, '.r')
    plt.legend(['recta aprendida', 'valores reales'])
    plt.show()


def learn_quadratic():
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
    N = 1000
    xf = 10
    dx = xf / N
    x = np.arange(0, xf, dx)
    y = 10* x * x

    plt.plot(x, y)
    plt.show()

    training_data = x
    target_data = y

    model = Sequential()
    model.add(Dense(50, input_dim=1, activation='relu'))
    model.add(Dense(20, activation='sigmoid'))
    model.add(Dense(1))

    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

    model.fit(training_data, target_data, epochs=5000, verbose=2, callbacks=[tensorboard])

    yp = model.predict(training_data)
    yp = yp.squeeze()
    print(yp)

    plt.plot(x, y, '-r')
    plt.plot(x, yp, '-b')
    e = np.divide(np.abs(yp - y), np.abs(y))
    plt.show()


def learn_piecewise_linear():
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

    x1 = np.arange(0,6)
    x2 = np.arange(5,10)
    y1 = x1
    y2 = 2*x2

    x = np.append(x1, x2)
    y = np.append(y1, y2)

    plt.plot(x, y)
    plt.show()

    training_data = x
    target_data = y

    model = Sequential()
    model.add(Dense(1000, input_dim=1, activation='relu'))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(50, activation='relu'))
    model.add(Dense(5, activation='relu'))
    model.add(Dense(1))

    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

    model.fit(training_data, target_data, epochs=1000, verbose=2, callbacks=[tensorboard])

    yp = model.predict(training_data)
    print(yp)

    plt.plot(x, y, '-r')
    plt.plot(x, yp, '-b')
    plt.show()


if __name__ == "__main__" :
    learn_quadratic()
    # learn_piecewise_linear()
    # learn_linear()
