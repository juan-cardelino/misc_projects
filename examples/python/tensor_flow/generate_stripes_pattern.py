#!/usr/bin/env python3
# coding=utf-8

import numpy as np
import numpy.matlib as mlib
import matplotlib.pyplot as plt
import os

output_dir = 'output'
if not(os.path.isdir(output_dir)):
    os.mkdir(output_dir)

# f=1/20
n_freq = 80
offset = 10
size = 224
x = np.arange(size)
count = 0
plt.figure()

for f in range(2, n_freq):
    for off in range(offset):
        # for x in array:
        freq = 1/f
        y = np.sin(2*np.pi*freq*x+off)
        name_suffix = str(count) + '_f' + str(f) + '_o' + str(off) + '.png'
        plt.plot(x, y)

        im_v = mlib.repmat(y, size, 1)
        # plt.imshow(im,cmap='gray')
        plt.imsave(os.path.join(output_dir, 'v' + name_suffix), im_v, cmap='gray')

        im_h = np.transpose(im_v)
        plt.imsave(os.path.join(output_dir, 'h' + name_suffix), im_h, cmap='gray')

        # plt.imshow(im_v, cmap='gray')
        # plt.show()
    count += 1

# v =np.array([1,2])
# w=mlib.repmat(v,2,1)
# print(w)