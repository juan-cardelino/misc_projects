#!/usr/bin/env python3
# coding=utf-8

# Image convolution

# Read image from url
from skimage import io
from skimage.color import rgb2gray
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt


def convolve(img_path):
    image = io.imread(img_path)
    image = np.float32(rgb2gray(image))

    # 2d Convolution mask for smoothing
    ks = np.array((
        [1, 1, 1],
        [1, 1, 1],
        [1, 1, 1]), dtype="float32")
    ks = ks/9

    # 2d Convolution mask for dx
    dx = np.array((
        [1, 2, 1],
        [0, 0, 0],
        [-1, -2, -1]), dtype="float32")


    '''
    # 1d Convolution mask
    dx = np.array((
        [1, 0, -1]
        ), dtype="float32")
    '''

    # Convolution
    # cv.normalize(image, image, 0, 1)

    img_x = cv.filter2D(image, -1, dx)

    img_s = cv.filter2D(image, -1, ks)

    # Display of results

    #plt.figure(figsize=(10,30))

    #plt.subplot(1,3,1)
    plt.imshow(image, cmap='gray' )
    plt.axis("off")
    plt.show()

    #plt.subplot(1,3,2)
    plt.imshow(img_s, cmap='gray' )
    plt.axis("off")
    plt.show()
    #plt.subplot(1,3,3)
    plt.imshow(img_x, cmap='gray' )
    plt.axis("off")

    plt.show()


#convolve('https://homepages.cae.wisc.edu/~ece533/images/cameraman.tif')
convolve('stripes_data/predict/horizontal/h0_f2_o6.png')
#convolve('stripes_data/predict/horizontal/h41_f43_o8.png')
#convolve('stripes_data/predict/vertical/v20_f22_o0.png')