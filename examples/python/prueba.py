#!/usr/bin/python
import Image
import matplotlib.pyplot as plt
import matplotlib.cm as colorMap
import matplotlib.patches as patches
import math

I = Image.open('tota.png')

plt.figure()
plt.hold(True)

plt.imshow(I, cmap=colorMap.gray, origin='lower', aspect = 'equal')

plt.arrow(100, 100, 20, 30, head_width=1, head_length=1, ec='r', fc='r')
ell = patches.Ellipse((100, 100), 20, 30, angle=math.degrees(math.pi/4), ec='r', fc='none')
plt.axes().add_artist(ell)

plt.plot([200, 300], [400, 500], 'g-')
plt.plot([400, 300], [300, 500], 'b--')

plt.show()
