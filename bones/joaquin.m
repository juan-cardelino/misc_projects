close all
clear all
home

%preprocesamiento
%a=imread('orig/IM-0001-1001.jpg');
a=imread('orig/unnamed.0008.jpg');

b=double(a);

level=graythresh(b);
c=im2bw(b,level);


M=max(max(b));
m=min(min(b));


th=50;
d=(b<th).*c;

n_min=50;
e=bwareaopen(d,n_min);

f=bwlabel(e);
g=label2rgb(f);
props={'Area','Centroid','BoundingBox','Orientation','MinorAxisLength','MajorAxisLength','ConvexHull'};
p  = regionprops(e,props);
areas=[p.Area];
[um,ind]=max(areas);
obj=p(ind);

pm=2;
pn=3;
%reorientación

%%
%dibujado
subplot(pm,pn,1)
imshow(b,[])
subplot(pm,pn,2)
imshow(c,[])
subplot(pm,pn,3)
imshow(d,[])
subplot(pm,pn,4)
imshow(e,[])
subplot(pm,pn,4)
imshow(g,[])
%subplot(pm,pn,5)

%%
figure(2)
imshow(b,[])
hold on;

rectangle('Position', obj.BoundingBox,'EdgeColor',[1 0 0])
max_l=obj.MajorAxisLength;
min_l=obj.MinorAxisLength;
cm=obj.Centroid;line(obj.ConvexHull(:,1),obj.ConvexHull(:,2))
or=-obj.Orientation/180*pi;
or2=or+pi/2;
major_axis(1,:)=[cm(1)+max_l/2*cos(or) cm(2)+max_l/2*sin(or)];
major_axis(2,:)=[cm(1)-max_l/2*cos(or) cm(2)-max_l/2*sin(or)];
minor_axis(1,:)=[cm(1)+min_l/2*cos(or2) cm(2)+min_l/2*sin(or2)];
minor_axis(2,:)=[cm(1)-min_l/2*cos(or2) cm(2)-min_l/2*sin(or2)];
plot(major_axis(:,1),major_axis(:,2),'-y')
plot(minor_axis(:,1),minor_axis(:,2),'-m')
plot(cm(1),cm(2),'o')
line(obj.ConvexHull(:,1),obj.ConvexHull(:,2))
hold off;
%%
fig = gcf;
style = hgexport('factorystyle');
style.Bounds = 'tight';
hgexport(fig,'-clipboard',style,'applystyle', true);
drawnow;
