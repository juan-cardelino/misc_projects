import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;

public class NaviListener
{

	int port = 55556;
	PrintWriter out = null;
	BufferedReader in = null;
	Socket clientSocket = null;
	ServerSocket serverSocket = null;

	public String readLine(BufferedReader in) throws IOException {
		String s = "";
		int c; // guarda cada byte en una variable tipo int
		while ((c = in.read()) != 0) { // lee un byte en c
										// c = -1: no hay más bytes
			s += ((char) c); // escribe el byte
		}
		return s;
	}

	public NaviListener(int port)
	{
		this.port=port;
		try {
			serverSocket = new ServerSocket(port);
			System.err.println("Started server on port " + port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void listen() {
		// a "blocking" call which waits until a connection is requested
		try {
			clientSocket = serverSocket.accept();
			System.err.println("Accepted connection from client");

			// open up IO streams
			OutputStream os = clientSocket.getOutputStream();
			InputStream is = clientSocket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			out = new PrintWriter(os, true);
			in = new BufferedReader(isr);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String readLine() {

            String s=null;
            try {
	    		
        	 
            s = readLine(in);
        	} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}	
        	return s;
	}
	
	public void close() throws IOException {

		// close IO streams, then socket
		System.err.println("Closing connection with client");
		out.close();
		in.close();
		clientSocket.close();
	}
}
