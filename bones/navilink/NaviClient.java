import java.io.*;
import java.net.*;

public class NaviClient {
	
	int port = 55555;
	int port_dest = 55555;
	
	String hostname = "10.0.1.15";
	String dest_ip= "10.0.1.137";
	
	public static String readLine(BufferedReader in) throws IOException
	{
		String s="";
		  int c;    // guarda cada byte en una variable tipo int
			while ((c = in.read()) != 0) {   // lee un byte en c
			                                     // c = -1: no hay más bytes
			      s+=((char)c);                 // escribe el byte
			  }
		           return s;
	}
	
		
	public NaviClient(String hostname, String dest_ip)
	{
		this.hostname=hostname;
		this.dest_ip =dest_ip;
	}
	
		public static void main(String[] args) throws IOException {

			String hostname = "10.0.1.15";
			String dest_ip= "10.0.1.137";

			
			System.out.println("DEBUG: command line arguments: ");
			for(int i=0;i<args.length;i++)
				System.out.println("DEBUG: args["+i+"]: "+args[i]);
			if (args.length>=1)
				hostname=args[0];
			if (args.length>=2)
				dest_ip=args[1];
			
			System.out.println("DEBUG: hostname:"+hostname);
			System.out.println("DEBUG: dest_ip: "+dest_ip);
			NaviClient nc=new NaviClient(hostname,dest_ip);
			nc.run();
			
	}
	
		public String parseImageID(String s)
		{
			String so="";
			String[] fields=s.split(" ");
			for(int i=0;i<fields.length;i++)
			{
				System.out.println("fields["+i+"]: "+fields[i]);
			}
			so=fields[2];
			return so;
		}
		
		public void run() throws IOException
		{
	
		
		Socket echoSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		NaviListener nl=new NaviListener(port_dest);

		System.out.println("DEBUG: opening socket to: "+dest_ip+" : "+port);
		try {
			echoSocket = new Socket(dest_ip, port);
			OutputStream os = echoSocket.getOutputStream();
			out = new PrintWriter(os, true);

		} catch (UnknownHostException e) {
			System.err.println("Computador desconocido: " + dest_ip);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for " + "the connection to: "
					+ dest_ip);
			System.exit(1);
		}
		System.out.println("DEBUG: opened connection to: " + dest_ip + " : " +port);
		System.out.println("DEBUG: open keyboard");
		// conecto con el teclado
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.println("DEBUG: handshake");
		String userInput = "NAV_BRAINLAB HANDSHAKE_2D "+hostname+" "+port_dest+" BRAINLAB\0";
		System.out.print("DEBUG: message to transmit: " + userInput + "\n");
		out.print(userInput);
		out.flush();
		   
		System.out.println("DEBUG: waiting for incoming connection: HANDSHAKE");
		nl.listen();

		String s;
		System.out.println("DEBUG: waiting for server response");
        s = nl.readLine();
        System.out.println("DEBUG: server response: "+s);

        //RESET CONNECTION
        /*
		System.out.println("DEBUG: RESET");
		userInput = "NAV_BRAINLAB RESET CONNECTION\0";
		System.out.print("DEBUG: message to transmit: " + userInput + "\n");
		out.print(userInput);
		*/
		   
	
        
        boolean use_ping=false;
        if (use_ping)
        {
        	while(true)
        	{
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			userInput = "NAV_BRAINLAB PING\0";
			System.out.print("DEBUG: message to transmit: " + userInput + "\n");
			out.print(userInput);
			out.flush();
			System.out.println("DEBUG: waiting for incoming connection: PING");
			//nl.listen();
			System.out.println("DEBUG: waiting for server response");
	        s = nl.readLine();
	        System.out.println("DEBUG: server response: "+s);
        	}
        }
		
		//start listening on UDP port
		NaviListenerUDP nlu=new NaviListenerUDP(port_dest);
		
		
		boolean stop=false;
		while (!stop) 
		{
			//wait for test shot to be available
			
			System.out.println("DEBUG: waiting for server response");
	        s = nlu.readLine();
	        System.out.println("DEBUG: server response: "+s);
	        String imageID=parseImageID(s);
	        //request the corresponding image
			s = "NAV_BRAINLAB REQUEST "+ imageID +"\0";
			System.out.println("DEBUG: message to send: "+s);
			out.print(s);
			out.flush();
		}



        nlu.close();
        nl.close();
		out.close();
		in.close();
		stdIn.close();
		echoSocket.close();
	}
}