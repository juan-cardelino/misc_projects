#!/bin/bash

. daemon_config.sh

#execute dcmtk receiver
$DCMTK_BIN_DIR/storescp $PORT_DICOM  -od $OUTPUT_DIR -uf -fe .dcm -xcr $CONVERT_SCRIPT &
#this is for debug only
#$DCMTK_BIN_DIR/storescp 11112 -od $OUTPUT_DIR +uf -fe .dcm &
#run navilink client
java -classpath tests_completos.jar NaviClientFull $IP $IP_DEST
#java -classpath tests_completos.jar NaviClient $IP $IP_DEST
