#!/bin/bash

. daemon_config.sh

cd $OUTPUT_DIR
mkdir -p $CONV_DIR

for i in `ls *.dcm`
do
	orig=$i
	final=$CONV_DIR/`basename $orig .dcm`.png

	#echo "paso: " $i


	if [ -f $final ]; then
		#echo "$orig already converted"
		echo "" > /dev/null
	else
		#echo "converting $orig"
		#$CONVERT -geometry 1024x1024 -normalize $orig $final
		echo convirtiendo $orig a $final
		$DCMTK_BIN_DIR/dcm2pnm +on -il $orig $final
	fi

done

