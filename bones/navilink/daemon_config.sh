#!/bin/bash
USE_MAC=1

  if [ "$USE_MAC" = 1 ]; then
		echo "MAC OSX options enabled"
		export DCMTK_DIR=/Users/juan/juanc/soft/develop/dcmtk-3.6.0
		export OUTPUT_DIR=/Users/juan/juanc/tmp/vips/incoming
		#export DCMTK_BIN_DIR=$DCMTK_DIR/dcmnet/apps
		export DCMTK_BIN_DIR=/usr/local/bin
		export DCMDICTPATH=$DCMTK_DIR/dcmdata/data/dicom.dic
		export CONVERT=convert
		export CONVERT_SCRIPT=./convert_from_dicom.sh
	elif [ "$USE_MAC" = 2 ]; then
    	echo "LINUX options enabled"
		export DCMTK_DIR=/usr/share/dcmtk
		export OUTPUT_DIR=/home/juan/juanc/tmp/vips/incoming
		export DCMTK_BIN_DIR=/usr/bin/
		export DCMDICTPATH=$DCMTK_DIR/dicom.dic
		export CONVERT=convert
		export CONVERT_SCRIPT=./convert_from_dicom.sh
    else
		echo "Windows options enabled"
		export DCMTK_DIR=C:/juan/soft/dcmtk-3.6.0-win32-i386
		export DCMTK_BIN_DIR=$DCMTK_DIR/bin
		export DCMDICTPATH=$DCMTK_DIR/share/dcmtk/dicom.dic
		export OUTPUT_DIR=c:/vips/incoming
		export CONVERT=convert
		export CONVERT_SCRIPT=convert_from_dicom.bat
fi

#perfil para block
export IP_DEST=10.5.1.40
export IP=10.5.1.15
#perfil para malaga
#export IP_DEST=10.0.1.137
#export IP=10.0.1.15
#export IP=10.0.1.33
#export IP_DEST=10.0.1.141

export PORT_DICOM=11112
#export PORT_DICOM=104
export CONV_DIR=$OUTPUT_DIR/converted
