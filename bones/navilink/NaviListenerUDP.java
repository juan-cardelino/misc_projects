/*

 */ 

import java.io.*;
import java.net.*;
import java.util.*;

public class NaviListenerUDP extends Thread {

    protected DatagramSocket socket = null;
    protected BufferedReader in = null;
    int port=55556;
	int state=0;
	String stored=null;
	
    public NaviListenerUDP(int port)
    {
        this.port=port;
        try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        }

    public String readLine() {
    	
    	String received=null;
            try {
                byte[] buf = new byte[512];

                    // receive request
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                 received = new String(packet.getData(), 0, packet.getLength());
                System.out.println("Received packet: " + received);
                state=1;
                stored=received;
                
                } catch (IOException e) {
                e.printStackTrace();
           
            }
                return received;
        }

    public int getEstado()
    {
    	return state;
    }
    
    public void setEstado(int s)
    {
    	state=s;
    	stored=null;
    }
    
    public String getStored()
    {
    	return stored;
    }
    
    public void run()
    {
		boolean stop=false;
		while (!stop) 
		{
			//wait for test shot to be available
			System.out.println("DEBUG: waiting for server response");
	        String s = readLine();
	        System.out.println("DEBUG: server response: "+s);
	        
		}
    	
    }
    

public void close()
{
    socket.close();	
}

}
