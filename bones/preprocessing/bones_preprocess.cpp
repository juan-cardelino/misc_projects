
#include <iostream>
#include <fstream>
#include <string>
// Include CImg library file and use its main namespace
#define cimg_verbosity 0
#include "CImg.h"
#include <time.h>
using namespace cimg_library;
using namespace std;

#define XML_IN fprintf(stdout,(char*)"<%s>\n",  __FUNCTION__)
#define XML_OUT fprintf(stdout,(char*)"</%s>\n", __FUNCTION__)


void bones_preprocess(const char* file_i, const char* file_o, int option)
{
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);

	timeinfo=localtime(&rawtime);
	char time_c_str[80];
	strftime (time_c_str,80,"%Y.%m.%d_%H.%M.%S",timeinfo);
	string time_str=time_c_str;
	string logFileName="c:/vips/logs/preprocess_"+time_str+".log";
	ofstream logFile(logFileName.c_str());
	//cout<<logFileName<<endl;
	string svn_version="$Id: bones_preprocess.cpp 278 2011-06-10 19:10:55Z juanc $";
	if(logFile)
		logFile<<"<bones_preprocess>"<<std::endl;
	if(logFile)
	{
		logFile<<"svn version: "<< svn_version<<std::endl;
		logFile<<"input filename: "<< file_i<<std::endl;
		logFile<<"output filename: "<< file_o<<std::endl;
		logFile<<"option: "<< option<<std::endl;
		logFile<<"cimg_OS: "<< cimg_OS<<std::endl;
	}
	CImg<unsigned char> image(file_i);

	switch (option){
	case 0:
	{
		logFile<<"executing case 0 "<<std::endl;
		const CImg<unsigned char> image_out= image.normalize(0,255).mirror('x');
		image_out.save(file_o);

		break;
	}
	case 1:
		logFile<<"executing case 1"<<std::endl;
		image.mirror('x');
		image.save(file_o);
		break;
	case 2:
		logFile<<"executing case 2 "<<std::endl;
		image.save(file_o);
		break;
	case 3:
	{
		logFile<<"executing case 3 "<<std::endl;
		const CImg<unsigned char> image_out= image.equalize(256,0,255).mirror('x');
		image_out.save(file_o);

		break;
	}
	case 4:
	{
		logFile<<"executing case 4 "<<std::endl;
		const CImg<unsigned char> image_out= image.sharpen(255,true,1,0,1.5).mirror('x');
		image_out.save(file_o);

		break;
	}
	default:
		break;
	}
	logFile<<"</bones_preprocess>"<<std::endl;
}

void bones_prueba_contraste(const char* file_i, const char* file_o, int option)
{
	XML_IN;

	const CImg<unsigned char> image(file_i);
	const CImg<float> histo=image.get_histogram(256);
	image.display();
	histo.display_graph(0,3);
	float high_th=180;
	const CImg<unsigned char> mask_high=image.get_threshold(high_th);
	float low_th=20;
	const CImg<unsigned char> mask_low=1-image.get_threshold(low_th);
	mask_high.display();
	mask_low.display();

	const CImg<unsigned char> im_out=image.get_mul(1-mask_high)+image.get_mul(1-mask_low)+mask_high*high_th+mask_low*low_th;
	im_out.display();

	const CImg<unsigned char> im_cut=image.get_cut(low_th,high_th);
	im_cut.display();

	const CImg<unsigned char> im_norm_cut=im_cut.get_normalize(0,255);
	im_norm_cut.display();

	XML_OUT;
}


void bones_prueba_sharpen(const char* file_i, const char* file_o, int option)
{
	XML_IN;

	//original image
	const CImg<float> im(file_i);
	im.display();

	const CImg<float> im_grey=im.get_norm()/sqrt(3.0);
	im_grey.display();

	//blurred image
	float sigma=5;
	const CImg<float> im_blur=im_grey.get_blur(sigma);
	im_blur.display();

	float weight=2.5;
	const CImg<float> im_sharp=im_grey-weight*im_blur;
	im_sharp.display();

	const CImg<float> im_sharp_norm=im_sharp.get_normalize(0,255);
	im_sharp_norm.display();


	XML_OUT;
}
