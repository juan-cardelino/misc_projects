
// Include CImg library file and use its main namespace


#include "bones_preprocess.h"
#include <cstdlib>

// Main procedure
//----------------
int main(int argc,char **argv)
{
	const char* file_i;
	const char* file_o;
	int option=3;
	if(argc<3)
	{
		file_i =  "examples/2.bmp";
		file_o = "examples/2_out.bmp";
	}else
	{
		file_i=argv[1];
		file_o=argv[2];

	}
	if(argc>=4)
		option=atoi(argv[3]);

	//bones_preprocess(file_i, file_o, option);
	//bones_prueba_contraste(file_i, file_o, option);
	bones_prueba_sharpen(file_i, file_o, option);
	return 0;
}
