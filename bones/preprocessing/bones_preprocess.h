#ifndef BONES_PREPROCESS_H
#define BONES_PREPROCESS_H
void bones_preprocess(const char* file_i, const char* file_o, int option);
void bones_prueba_contraste(const char* file_i, const char* file_o, int option);
void bones_prueba_sharpen(const char* file_i, const char* file_o, int option);
#endif //BONES_PREPROCESS_H
