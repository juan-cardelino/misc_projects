import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;

public class EchoServerNavi {


	public static String readLine(BufferedReader in) throws IOException
	{
		String s="";
		  int c;    // guarda cada byte en una variable tipo int
			while ((c = in.read()) != 0) {   // lee un byte en c
			                                     // c = -1: no hay más bytes
			      s+=((char)c);                 // escribe el byte
			  }
		           return s;
	}
	
	public static void main(String[] args) throws Exception {

    	
	    int port=55556;
        ServerSocket serverSocket = new ServerSocket(port);
        System.err.println("Started server on port " + port);

        // repeatedly wait for connections, and process
        while (true) {

            // a "blocking" call which waits until a connection is requested
            Socket clientSocket = serverSocket.accept();
            System.err.println("Accepted connection from client");
                    
            PrintWriter out = null;
            BufferedReader in = null;
            // open up IO streams
            OutputStream os=clientSocket.getOutputStream();
            InputStream is=clientSocket.getInputStream();
            InputStreamReader isr=new InputStreamReader(is);
            out = new PrintWriter(os, true);
            in = new BufferedReader(isr);

            //mensaje de bienvenida
            //out.println("Bienvenido al servidor");
            // waits for data and reads it in until connection dies
            // readLine() blocks until the server receives a new line from client
            String s;
            try {
	    		
        	 
            s = readLine(in);
            while (true) {
            	System.out.println("CLIENT: "+s);
                //out.println("SERVER: "+s);
            	s = readLine(in);
            }
            

        	} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}

            // close IO streams, then socket
            System.err.println("Closing connection with client");
            out.close();
            in.close();
            clientSocket.close();
        }
    }
}