import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;

public class EchoServer {

    public static void main(String[] args) throws Exception {

    	
	    int port=55556;
        ServerSocket serverSocket = new ServerSocket(port);
        System.err.println("Started server on port " + port);

        // repeatedly wait for connections, and process
        while (true) {

            // a "blocking" call which waits until a connection is requested
            Socket clientSocket = serverSocket.accept();
            System.err.println("Accepted connection from client");
                    
            PrintWriter out = null;
            BufferedReader in = null;
            // open up IO streams
            OutputStream os=clientSocket.getOutputStream();
            InputStream is=clientSocket.getInputStream();
            InputStreamReader isr=new InputStreamReader(is);
            out = new PrintWriter(os, true);
            in = new BufferedReader(isr);

            //mensaje de bienvenida
            //out.println("Bienvenido al servidor");
            // waits for data and reads it in until connection dies
            // readLine() blocks until the server receives a new line from client
            String s;
            s = in.readLine();
            while (s != null) {
            	System.out.println("CLIENT: "+s);
                //out.println("SERVER: "+s);
                s = in.readLine();
            }

            // close IO streams, then socket
            System.err.println("Closing connection with client");
            out.close();
            in.close();
            clientSocket.close();
        }
    }
}