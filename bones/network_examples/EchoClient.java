import java.io.*;
import java.net.*;

public class EchoClient {
	public static void main(String[] args) throws IOException {

		String hostname = "10.0.1.137";
		int port = 55555;
		// String hostname="10.0.1.15";
		// int port=55555;
		Socket echoSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;

		System.out.println("DEBUG: opening sockets");
		try {
			echoSocket = new Socket(hostname, port);
			OutputStream os = echoSocket.getOutputStream();
			out = new PrintWriter(os, true);

			InputStream is = echoSocket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			in = new BufferedReader(isr);
		} catch (UnknownHostException e) {
			System.err.println("Computador desconocido: " + hostname);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for " + "the connection to: "
					+ hostname);
			System.exit(1);
		}

		System.out.println("DEBUG: open keyboard");
		// conecto con el teclado
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.println("DEBUG: handshake");
		String userInput = "NAV_BRAINLAB HANDSHAKE_2D 10.0.1.15 55556 BRAINLAB\0";
		out.println(userInput);

		System.out.print("mensaje a transmitir: " + userInput + "\n");

		userInput = "NAV_BRAINLAB PING\0";
		out.println(userInput);

		int i = 0;
		while (i < 3) {
			System.out.print("i:"+i+" mensaje a transmitir: ");
			userInput = stdIn.readLine();
			while (userInput != null && !userInput.equals("q")) {
				// System.out.println("respuesta del server: " + in.readLine());
				System.out.print("i:"+i+" mensaje a transmitir: ");
				userInput = stdIn.readLine();
			}

			userInput = "NAV_BRAINLAB REQUEST ImageID1\0";
			out.println(userInput);
			i++;
		}

		out.close();
		in.close();
		stdIn.close();
		echoSocket.close();
	}
}