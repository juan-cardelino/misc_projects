import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class AtenderCliente implements Runnable {

	public String readLine(BufferedReader in)  throws Exception
	{
		String s="";
		
		  int c;    // guarda cada byte en una variable tipo int
			while ((c = in.read()) != 0) {   // lee un byte en c
			                                     // c = -1: no hay más bytes
			      s+=((char)c);                 // escribe el byte
			  }
	
		           return s;
	}
	
	private Socket clientSocket;
	private static int lastId=0;
	private int clientId;
	public AtenderCliente(Socket s)
	{
		clientId=lastId++;
		this.clientSocket=s;
		System.out.println("Creando hilo para atender al cliente: "+clientId);
	}
	
	@Override
	public void run() {
		
		System.out.println("Corriendo hilo para atender al cliente: "+clientId);
		PrintWriter out = null;
        BufferedReader in = null;
        try{
        // open up IO streams
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }catch(Exception e)
        {
        	System.out.println(e);
        }
        
        try{
        //mensaje de bienvenida
        //out.println("Bienvenido al servidor");
        // waits for data and reads it in until connection dies
        // readLine() blocks until the server receives a new line from client
        String s = readLine(in);
        while (true) {
        	System.out.println("CLIENT: "+s);
            //out.println("SERVER: "+s);
        	s = readLine(in);
        }
        }catch(Exception e)
        {
        	System.out.println(e);
        }
        
        try{
        // close IO streams, then socket
        System.err.println("Cerrando conexion con cliente: "+clientId);
        out.close();
        in.close();
        clientSocket.close();

        }catch(Exception e)
        {
        	System.out.println(e);
        }
      
        
        
		
		
	}

}
