import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;

public class EchoMultiServer {

    public static void main(String[] args) throws Exception {

        // create socket
    	int port=55556;
        ServerSocket serverSocket = new ServerSocket(port);
        System.err.println("Started server on port " + port);
        
        // repeatedly wait for connections, and process
        while (true) 
        {
            // a "blocking" call which waits until a connection is requested
            Socket clientSocket = serverSocket.accept();
            System.err.println("Accepted connection from client");
            AtenderCliente ac=new AtenderCliente(clientSocket);
            //AtenderClienteChat ac=new AtenderClienteChat(clientSocket);
            Thread t=new Thread(ac);
            t.start();
        }
    }

	
}