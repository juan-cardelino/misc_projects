

public class MultiHilo
{

    public static void main(String[] args) throws Exception
    {

        //cantidad de hilos
        int numeroHilos = 3;
  
        //comenzar a ejecutar las tareas
        for(int i=0;i<numeroHilos;i++) 
        {
            Tarea tarea=new Tarea(i);
            Thread t=new Thread(tarea);
            t.start();
        }
        
        System.out.println("Termino ejecucion del main");
    }
	
}