


public class Tarea implements Runnable {


	private static int lastId=0;
	private int clientId;
	private int factor=1;
	
	public Tarea()
	{
		clientId=lastId++;
		System.out.println("Creando hilo: "+clientId);
	}
	
	public Tarea(int factor)
	{
		this.factor=factor+1;
		clientId=lastId++;
		System.out.println("Creando hilo: "+clientId);
	}
	
	@Override
	public void run() {
		
		
		try {
			
		for(int i=0; i<10 ;i++)
		{
				Thread.sleep(factor*1000);
				System.out.println("Hilo: "+clientId+" contador: "+i);
		}
        
		} catch (Exception e) {
			
			System.out.println(e);
		}
		System.out.println("Termino ejecucion hilo: "+clientId);
	}

}
